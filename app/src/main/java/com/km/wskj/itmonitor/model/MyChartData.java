package com.km.wskj.itmonitor.model;

/**
 * @author by Xianling.Zhou on 2017/3/6.
 */

public class MyChartData {
    public String xAxisValue;
    public float yValue;
    public float xValue;

    public MyChartData(float xValue, float yValue, String xAxisValue) {
        this.xAxisValue = xAxisValue;
        this.yValue = yValue;
        this.xValue = xValue;
    }
}
