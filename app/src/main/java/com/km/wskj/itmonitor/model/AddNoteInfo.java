package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by wuhan on 2017/3/15.
 */

public class AddNoteInfo extends BaseBean {

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "userId")
    public String userId;

}
