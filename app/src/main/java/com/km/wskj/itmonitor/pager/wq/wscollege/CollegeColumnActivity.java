package com.km.wskj.itmonitor.pager.wq.wscollege;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.FuncItem;
import com.km.wskj.itmonitor.model.KnowledgeLibrary;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.MyGridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/24.
 */
//栏目分类
public class CollegeColumnActivity extends LActivity {
    private static final String TAG = "CollegeColumnActivity";

    //ui
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.tool_bar_right_text)
    TextView toolBarRightText;
    @BindView(R.id.container_gridview)
    LinearLayout mContainer;

    //data
    private MyGridView gridView;
    List<FuncItem> funcItems = new ArrayList<>();
    private List<KnowledgeLibrary> knowledgeLibrarys;
    private String pid = null;

    private Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_college_column);
        ButterKnife.bind(this);

        toolBarBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText("全部分类");
        initFuncItems();
        initData();


        mContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                createGridView();
            }
        });
    }

    private void initData() {
        pdc.knowledgeLib(HTTP_TASK_TAG, pid, new ZxlGenericsCallback<List<KnowledgeLibrary>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<KnowledgeLibrary> knowledgeLibraries, int i) {
                knowledgeLibrarys = knowledgeLibraries;
            }
        });
    }

    private void initFuncItems() {
        String s[] = getResources().getStringArray(R.array.ws_college_column_func_title);
        int r[] = {R.mipmap.monitor_internet
                , R.mipmap.monitor_database
                , R.mipmap.monitor_middle_ware
                , R.mipmap.monitor_internet
                , R.mipmap.monitor_server
                , R.mipmap.monitor_memory
                , R.mipmap.yjs
                , R.mipmap.xxaq
                , R.mipmap.monitor_round
                , R.mipmap.xxaq
                , R.mipmap.monitor_internet
        };

        funcItems.clear();


        for (int i = 0; i < s.length && i < r.length; i++) {
            FuncItem fi = new FuncItem();
            fi.name = s[i];
            fi.resId = r[i];
            funcItems.add(fi);
        }
    }


    private void createGridView() {

        gridView = new MyGridView(context, new MyGridView.MyGridViewAdapter() {
            @Override
            public int rowspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int leftpadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int toppadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colCount(MyGridView myGridView) {
                return 4;
            }

            @Override
            public int rowHeight(MyGridView myGridView) {
                return mContainer.getHeight() / 8;
            }

            @Override
            public int viewWidth(MyGridView myGridView) {
                return mContainer.getWidth();
            }

            @Override
            public int count(MyGridView myGridView) {
                return funcItems.size();
            }

            @Override
            public View cell(int i, MyGridView myGridView) {
                View view = getLayoutInflater().inflate(R.layout.ws_college_item_func, null);
                ImageView iv_pic = (ImageView) view.findViewById(R.id.iv_pic);
                TextView tv_name = (TextView) view.findViewById(R.id.tv_name);

                FuncItem fi = funcItems.get(i);
                iv_pic.setImageResource(fi.resId);
                tv_name.setText(fi.name);
                return view;
            }

            @Override
            public View title(int i, MyGridView myGridView) {
                return null;
            }

            @Override
            public int titleOffsetY(MyGridView myGridView) {
                return 0;
            }

            @Override
            public boolean needGridLine(MyGridView myGridView) {
                return false;
            }
        });


        gridView.setListener(new MyGridView.MyGridViewListener() {
            @Override
            public void onCellTouchDown(int i) {

            }

            @Override
            public void onCellTouchUp(int i) {

            }

            @Override
            public void onCellLongPress(int i) {

            }

            @Override
            public void onCellClick(int i) {

                switch (i) {
                    //操作系统
                    case 0: {
                        String id = getCurrentItem("操作系统");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                    //数据库
                    case 1: {
                        String id = getCurrentItem("数据库");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                    //中间件
                    case 2: {
                        String id = getCurrentItem("中间件");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                    //网络
                    case 3: {
                        String id = getCurrentItem("网络");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);

                    }
                    break;
                    //服务器
                    case 4: {
                        String id = getCurrentItem("服务器");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                    //存储
                    case 5: {
                        String id = getCurrentItem("存储");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                    //云计算
                    case 6: {
                        String id = getCurrentItem("云计算");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                    //信息安全
                    case 7: {
                        String id = getCurrentItem("信息安全");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        LogUtils.e("信息安全" + id);
                        startActivity(intent);
                    }
                    break;
                    //机房东环
                    case 8: {
                        String id = getCurrentItem("机房动环");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        LogUtils.e("机房东环" + id);
                        startActivity(intent);
                    }
                    break;
                    //系统集成
                    case 9: {
                        String id = getCurrentItem("系统集成");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        LogUtils.e("机房东环" + id);
                        startActivity(intent);
                    }
                    break;
                    //行业方案
                    case 10: {
                        String id = getCurrentItem("行业方案");
                        intent = new Intent(context, CollegeListActivity.class);
                        intent.putExtra("id", id);
                        startActivity(intent);
                    }
                    break;
                }
            }
        });
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mContainer.addView(gridView.getView(), p);
    }


    private String getCurrentItem(String itemName) {

        for (KnowledgeLibrary klb : knowledgeLibrarys
                ) {
            if (klb.columnName.equals(itemName))
                return klb.id;
        }
        return null;
    }


    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
