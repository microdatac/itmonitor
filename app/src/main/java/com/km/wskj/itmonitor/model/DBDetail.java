package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/14.
 */

public class DBDetail {

    //mysql
   /* threads_running true number 活跃线程数
    max_connections true number 设置连接数
    innodb_lock_waits true number 查询锁等待
    max_used_connections true number 当前连接数
    usedRate true number 连接占用比*/

    //mysql
    @JSONField(name = "threads_running")
    public int threads_running;
    @JSONField(name = "max_connections")
    public int max_connections;
    @JSONField(name = "innodb_lock_waits")
    public int innodb_lock_waits;
    @JSONField(name = "max_used_connections")
    public int max_used_connections;
    @JSONField(name = "usedRate")
    public float usedRate;

    //sqlserver
    /* "sessions": 37,    //会话数
             "instance": "MSSQLSERVER",   //数据库实例名
             "memery_sj": 50,    //实际使用内存 MB
             "lockMessage": "现在没有阻塞和死锁信息",   //数据库死锁
             "version": "Microsoft SQL Server 2008 R2 (RTM) - 10.50.1600.1 (X64) ",   //版本
             "connections": 11   //连接数*/

    @JSONField(name = "sessions")
    public int sessions;
    @JSONField(name = "instance")
    public String instance;
    @JSONField(name = "memery_sj")
    public int memery_sj;
    @JSONField(name = "lockMessage")
    public String lockMessage;
    @JSONField(name = "version")
    public String version;
    @JSONField(name = "connections")
    public int connections;

    //oracle
    /* "CURSOR_USED": 74,    //游标使用数
             "STARTUP_TIME": 107208,  //启动时间
             "INSTANCE_NAME": "orcl",   //实例名
             "SESSIONS": 33,    //线程数
             "LOCK": false,    //是否死锁
             "DATABASE_STATUS": "ACTIVE",   //数据库状态
             "PROCESSES": "150",   //  进程数
             "PGA": 37.46,       // PGA
             "CURSOR_TOTAL": "300",    //总共游标数
             "SGA": 1640.63   //SGA*/

    @JSONField(name = "CURSOR_USED")
    public String CURSOR_USED;
    @JSONField(name = "STARTUP_TIME")
    public String STARTUP_TIME;
    @JSONField(name = "INSTANCE_NAME")
    public String INSTANCE_NAME;
    @JSONField(name = "SESSIONS")
    public int SESSIONS;
    @JSONField(name = "LOCK")
    public String LOCK;
    @JSONField(name = "DATABASE_STATUS")
    public String DATABASE_STATUS;
    @JSONField(name = "PROCESSES")
    public String PROCESSES;
    @JSONField(name = "PGA")
    public int PGA;
    @JSONField(name = "CURSOR_TOTAL")
    public String CURSOR_TOTAL;
    @JSONField(name = "SGA")
    public int SGA;


}
