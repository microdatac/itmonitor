package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/27.
 */
//商品列表
public class PartsItem extends BaseBean {

    @JSONField(name = "applicableModels")
    public String applicableModels;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "pnNumber")
    public String pnNumber;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "type")
    public String type;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "bond")
    public String bond;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "path")
    public String path;
    @JSONField(name = "number")
    public int number;
    @JSONField(name = "columnNames")
    public String columnNames;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "price")
    public double price;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "imgs")
    public List<Imgs> imgs;

}
