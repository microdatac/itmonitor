package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/15.
 */

public class NewsItem extends BaseBean {
    /*  "path": "b6599905d1fd47fe9c732c847ede7ccc",
              "columnNames": "安全漏洞",
              "createTime": "2017-03-08 21:39:11",
              "columnId": "b6599905d1fd47fe9c732c847ede7ccc",
              "author": "绿盟",
              "id": "f0ada0f37bc24735b398eecafa161002",
              "source": "绿盟",
              "title": "Apache Struts2  远程代码执行漏洞（S2-045） 技术分析与防护方案",
              "isDel": 0,
              "content": "<h2><span style=\"font-family:宋体\">综述</span></h2><p>    Apache Structs2的Jakarta Multipart parser插件存在远程代码执行漏洞，漏洞编号为CNNVD-201703-152。攻击者可以在使用该插件上传文件时，修改HTTP请求头中的Content-Type值来触发该漏洞，导致远程执行代码。</p><p>    相关链接如下：</p><p><a href=\"https://cwiki.apache.org/confluence/display/WW/S2-045?from=timeline&isappinstalled=0\"><strong><span style=\"color:#70AD47\">https://cwiki.apache.org/confluence/display/WW/S2-045?from=timeline&isappinstalled=0</span></strong></a></p><h2><span style=\"font-family:宋体\">影响的版本</span></h2><p class=\"MsoListParagraph\" style=\"margin-left:56px\"><span style=\"font-family:Wingdings\">l<span style=\"font-variant-numeric: normal;font-stretch: normal;font-size: 9px;line-height: normal;font-family: 'Times New Roman'\">  </span></span>Struts 2.3.5 - Struts 2.3.31</p><p class=\"MsoListParagraph\" style=\"margin-left:56px\"><span style=\"font-family:Wingdings\">l<span style=\"font-variant-numeric: normal;font-stretch: normal;font-size: 9px;line-height: normal;font-family: 'Times New Roman'\">  </span></span>Struts 2.5 - Struts 2.5.10</p><h2><span style=\"font-family:宋体\">不受影响的版本</span></h2><p class=\"MsoListParagraph\" style=\"margin-left:56px\"><span style=\"font-family:Wingdings\">l<span style=\"font-variant-numeric: normal;font-stretch: normal;font-size: 9px;line-height: normal;font-family: 'Times New Roman'\">  </span></span>Struts 2.3.32</p><p class=\"MsoListParagraph\" style=\"margin-left:56px\"><span style=\"font-family:Wingdings\">l<span style=\"font-variant-numeric: normal;font-stretch: normal;font-size: 9px;line-height: normal;font-family: 'Times New Roman'\">  </span></span>Struts 2.5.10.1</p><p><br/></p>",
              "desc": "Apache Struts2  远程代码执行漏洞（S2-045）技术分析与防护方案"*/

    @JSONField(name = "path")
    public String path;
    @JSONField(name = "imgs")
    public List<Imgs> imgs;
    @JSONField(name = "columnNames")
    public String columnNames;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "author")
    public String author;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "source")
    public String source;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "desc")
    public String desc;


}
