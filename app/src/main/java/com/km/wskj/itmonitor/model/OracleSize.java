package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/21.
 */

public class OracleSize extends BaseBean {
   /* TABLESPACE_NAME true string 表空间名字
    TOTAL_SIZE true string 总空间MB
    USED_SIZE true string 已用空间MB
    FREE_SIZE true string 空余空间MB*/

    @JSONField(name = "TABLESPACE_NAME")
    public String name;
    @JSONField(name = "TOTAL_SIZE")
    public String total;
    @JSONField(name = "FREE_SIZE")
    public String free;
    @JSONField(name = "USED_SIZE")
    public String used;
}
