package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/14.
 */

public class Files extends BaseBean {

    /* "file_path": "/upload/2017/03/08/",
             "new_file_name": "201703082124370396e18.docx",
             "create_time": "2017-03-08 21:37:03",
             "createTime": "2017-03-08 21:39:11",
             "attach_id": "7bd7379009104493a1a7875455807ec5",
             "articleId": "f0ada0f37bc24735b398eecafa161002",
             "host": "http://wsiops.iok.la:8080/iOPS",
             "original_file_name": "Apache Struts2 远程代码执行漏洞（S2-045）漏洞技术分析与防护方案.docx",
             "attachId": "7bd7379009104493a1a7875455807ec5",
             "type": 2,
             "suffix": ".docx"*/

    @JSONField(name = "file_path")
    public String file_path;
    @JSONField(name = "new_file_name")
    public String new_file_name;
    @JSONField(name = "create_time")
    public String create_time;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "attach_id")
    public String attach_id;
    @JSONField(name = "articleId")
    public String articleId;
    @JSONField(name = "host")
    public String host;
    @JSONField(name = "original_file_name")
    public String original_file_name;
    @JSONField(name = "attachId")
    public String attachId;
    @JSONField(name = "type")
    public int type;
    @JSONField(name = "suffix")
    public String suffix;

}
