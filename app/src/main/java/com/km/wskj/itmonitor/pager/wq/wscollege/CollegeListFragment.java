package com.km.wskj.itmonitor.pager.wq.wscollege;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.fingdo.statelayout.StateLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.CollegeItem;
import com.km.wskj.itmonitor.model.Imgs;
import com.km.wskj.itmonitor.model.KnowledgeLibrary;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.wq.forum.ForumDetailActivity;
import com.km.wskj.itmonitor.pager.wq.forum.model.ForumCard;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.global.Constant;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import zrc.widget.SimpleFooter;
import zrc.widget.SimpleHeader;
import zrc.widget.ZrcListView;

//
public class CollegeListFragment extends LFragment {
    private static final String TAG = "CollegeListFragment";
    //ui
    @BindView(R.id.knowledge_list_zrcListView)
    ZrcListView mZrcListView;
    @BindView(R.id.state_layout)
    StateLayout stateLayout;


    //data
    KnowledgeLibrary mKnowledgeLibrary;
    int lastUpdateNum;
    int pageIndex;
    MyAdapter mAdapter;
    List<CollegeItem> mCollegeLists;


    public static CollegeListFragment getInstance(KnowledgeLibrary kowledgeLibrary) {
        CollegeListFragment sf = new CollegeListFragment();
        sf.mKnowledgeLibrary = kowledgeLibrary;
        return sf;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_knowledge_list, null);
        ButterKnife.bind(this, view);
        initData();
        return view;
    }

    private void initData() {
        if (mKnowledgeLibrary == null)
            return;

        pdc.collegeList(HTTP_TASK_TAG, mKnowledgeLibrary.id, 1, Constant.PageListSize, new ZxlGenericsCallback<List<CollegeItem>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                stateLayout.showErrorView();
            }

            @Override
            public void onResponse(List<CollegeItem> collegeLists, int i) {
                if (collegeLists.size() == 0)
                    stateLayout.showEmptyView();

                mCollegeLists = collegeLists;
                lastUpdateNum = collegeLists.size();
                pageIndex = 1;
                //control
                mAdapter.notifyDataSetChanged();
                mZrcListView.setRefreshSuccess();

                if (haveMore())
                    mZrcListView.startLoadMore();
                else
                    mZrcListView.stopLoadMore();
            }
        });

        initPullToRefresh();
    }

    private void initPullToRefresh() {
        //配置listview
        ColorDrawable line = new ColorDrawable(Color.parseColor("#eeeeee"));
        mZrcListView.setDivider(line);
        mZrcListView.setDividerHeight(1);

        //设置下拉刷新的样式（可选，但如果没有Header则无法下拉刷新）
        SimpleHeader header = new SimpleHeader(getActivity());
        header.setTextColor(ContextCompat.getColor(getContext(), R.color.text_gray));
        header.setCircleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mZrcListView.setHeadable(header);

        //设置加载更多的样式（可选）
        SimpleFooter footer = new SimpleFooter(getContext());
        footer.setCircleColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mZrcListView.setFootable(footer);

        // 设置列表项出现动画（可选）
        //zListView.setItemAnimForTopIn(R.anim.topitem_in);
        mZrcListView.setItemAnimForBottomIn(R.anim.bottomitem_in);

        // 下拉刷新事件回调（可选）
        mZrcListView.setOnRefreshStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onRefresh();
            }
        });

        // 加载更多事件回调（可选）
        mZrcListView.setOnLoadMoreStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onLoadMore();
            }
        });

        // 项目选中
        mZrcListView.setOnItemClickListener(new ZrcListView.OnItemClickListener() {
            @Override
            public void onItemClick(ZrcListView parent, View view, int position, long id) {
                CollegeItem item = (CollegeItem) parent.getItemAtPosition(position);
                Intent t = new Intent(getActivity(), CollegeDetailActivity.class);
                t.putExtra("data", item);
                startActivity(t);
            }
        });

        mAdapter = new MyAdapter();
        mZrcListView.setAdapter(mAdapter);
        stateLayout.showContentView();
    }

    private void onRefresh() {
        pdc.collegeList(HTTP_TASK_TAG, mKnowledgeLibrary.id, 1, Constant.PageListSize
                , new ZxlGenericsCallback<List<CollegeItem>>() {
                    @Override
                    public void onError(Call call, Exception e, int i) {
                        LogUtils.e(TAG, e);
                        stateLayout.showErrorView();
                    }

                    @Override
                    public void onResponse(List<CollegeItem> collegeLists, int i) {
                        if (collegeLists.size() == 0)
                            stateLayout.showEmptyView();
                        mCollegeLists = collegeLists;
                        lastUpdateNum = collegeLists.size();
                        pageIndex = 1;
                        //control
                        mAdapter.notifyDataSetChanged();
                        mZrcListView.setRefreshSuccess();

                        if (haveMore())
                            mZrcListView.startLoadMore();
                        else
                            mZrcListView.stopLoadMore();
                    }
                });
    }

    private void onLoadMore() {
        pdc.collegeList(HTTP_TASK_TAG, mKnowledgeLibrary.id, pageIndex + 1, Constant.PageListSize
                , new ZxlGenericsCallback<List<CollegeItem>>() {
                    @Override
                    public void onError(Call call, Exception e, int i) {
                        LogUtils.e(TAG, e);
                        stateLayout.showErrorView();
                    }

                    @Override
                    public void onResponse(List<CollegeItem> collegeLists, int i) {
                        lastUpdateNum = collegeLists.size();
                        if (collegeLists.size() > 0) {
                            mCollegeLists.addAll(collegeLists);
                            pageIndex++;
                        }
                        //control
                        mAdapter.notifyDataSetChanged();
                        mZrcListView.setLoadMoreSuccess();
                        if (haveMore())
                            mZrcListView.startLoadMore();
                        else
                            mZrcListView.stopLoadMore();

                    }
                });
    }

    private boolean haveMore() {
        return lastUpdateNum > 0;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * 适配器
     */
    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (mCollegeLists == null) {
                return 0;
            }
            return mCollegeLists.size();
        }

        @Override
        public Object getItem(int position) {
            return mCollegeLists.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final BaseAdapterHelper helper = BaseAdapterHelper.get(getContext(),
                    convertView, parent, R.layout.item_college_list);
            CollegeItem item = mCollegeLists.get(position);
            helper.setText(R.id.tv_title, item.title);
            helper.setText(R.id.tv_des, item.desc);
            helper.setText(R.id.tv_create_time, item.createTime);

            //图片url
            CollegeItem.CoverImgBean coverImg = item.coverImg;
            if (coverImg != null) {
                String url = coverImg.host + coverImg.filePath + coverImg.newFileName;
                Glide.with(getActivity())
                        .load(url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                helper.setImageBitmap(R.id.iv_preview, resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                LogUtils.e("LLpp:加载失败");
                            }
                        }); //方法中设置asBitmap可以设置回调类型
            }
            return helper.getView();
        }
    }
}