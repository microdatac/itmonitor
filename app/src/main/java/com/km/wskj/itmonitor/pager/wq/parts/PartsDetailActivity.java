package com.km.wskj.itmonitor.pager.wq.parts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.wq.adapter.MyPagerAdapter;
import com.km.wskj.itmonitor.utils.StatusBarCompat;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author by Xianling.Zhou on 2017/3/27.
 */

public class PartsDetailActivity extends LActivity {
    private static final String TAG = "PartsDetailActivity";
    @BindView(R.id.ib_back)
    ImageButton mIbBack;
    @BindView(R.id.tab_layout)
    SlidingTabLayout mTabLayout;
    @BindView(R.id.ib_share)
    ImageButton mIbShare;
    @BindView(R.id.viewpager)
    ViewPager mViewpager;

    private String id;


    //data
    private ArrayList<LFragment> mFragments;
    private ArrayList<String> mTitles;
    private MyPagerAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parts_detail);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("id"))
            id = getIntent().getStringExtra("id");
        initView();
        initData();
    }

    private void initData() {
        mFragments = new ArrayList<>();
        mTitles = new ArrayList<>();

        mTitles.add("商品");
        mTitles.add("详情");

        mFragments.add(GoodsFragment.getInstance(id));
        mFragments.add(GoodsDetailFragment.getInstance(id));

        mAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
        mViewpager.setAdapter(mAdapter);
        mTabLayout.setViewPager(mViewpager);
    }

    private void initView() {

    }

    @OnClick({R.id.ib_back, R.id.ib_share})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ib_back:
                finish();
                break;
            case R.id.ib_share:
                Toast.makeText(context, "分享", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
