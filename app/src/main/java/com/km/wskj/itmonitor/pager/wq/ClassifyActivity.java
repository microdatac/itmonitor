package com.km.wskj.itmonitor.pager.wq;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.global.ClassifyParams;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.KnowledgeLibrary;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.wq.adapter.MenuAdapter;
import com.km.wskj.itmonitor.pager.wq.forum.ForumListActivity;
import com.km.wskj.itmonitor.pager.wq.forum.ForumSendActivity;
import com.km.wskj.itmonitor.pager.wq.wscollege.CollegeListActivity;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/31.
 */
//分类
public class ClassifyActivity extends LActivity {
    private static final String TAG = "ClassifyActivity";
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.lv_left)
    ListView lvLeft;
    @BindView(R.id.lv_right)
    ListView lvRight;

    private String mId;
    private MenuAdapter mAdapter;
    private List<KnowledgeLibrary> mData;
    private int index;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_classify);
        ButterKnife.bind(this);

        toolBarTitle.setText("选择分类");
        toolBarBack.setVisibility(View.VISIBLE);

        if (getIntent().hasExtra("index"))
            index = getIntent().getIntExtra("index", 0);
        initData();
    }

    private void initData() {
        //要时ListView右侧无滚动条需要禁用普通的滚动条及快速滚动条
        lvLeft.setVerticalScrollBarEnabled(false);
        lvLeft.setFastScrollEnabled(false);
        lvLeft.setDividerHeight(0);

        //要时ListView右侧无滚动条需要禁用普通的滚动条及快速滚动条
        lvRight.setVerticalScrollBarEnabled(false);
        lvRight.setFastScrollEnabled(false);

        ColorDrawable line = new ColorDrawable(Color.parseColor("#eeeeee"));
        lvRight.setDivider(line);
        lvRight.setDividerHeight(1);

        pdc.knowledgeLib(HTTP_TASK_TAG, mId, new ZxlGenericsCallback<List<KnowledgeLibrary>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<KnowledgeLibrary> knowledgeLibraries, int i) {

                mData = knowledgeLibraries;
                mAdapter = new MenuAdapter(context, mData);
                mAdapter.setSelectedBackgroundResource(R.mipmap.select_white);//选中时
                mAdapter.setHasDivider(false);
                mAdapter.setNormalBackgroundResource(R.color.menu_bg_gray);//未选中

                lvLeft.setAdapter(mAdapter);
                lvLeft.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (mAdapter != null)
                            mAdapter.setSelectedPos(position);
                        KnowledgeLibrary kl = (KnowledgeLibrary) parent.getItemAtPosition(position);
                        initRightMenu(kl.id);
                    }
                });

                //默认选中第一项
                lvLeft.post(new Runnable() {
                    @Override
                    public void run() {
                        lvLeft.getChildAt(0).setBackgroundResource(R.mipmap.select_white);
                        KnowledgeLibrary kl = (KnowledgeLibrary) lvLeft.getItemAtPosition(0);
                        initRightMenu(kl.id);
                    }
                });
            }
        });

        lvRight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                KnowledgeLibrary item = (KnowledgeLibrary) adapterView.getItemAtPosition(i);
                //发帖
                if (index == ClassifyParams.SEND_FORUM_CARD) {
                    Intent t = new Intent(context, ForumSendActivity.class);
                    t.putExtra("id", item.id);
                    startActivity(t);
                    finish();
                    //技术论坛全部
                } else if (index == ClassifyParams.FORUM_ALL) {
                    Intent t = new Intent(context, ForumListActivity.class);
                    t.putExtra("data", item);
                    startActivity(t);
                    finish();
                    //微数学院全部
                } else if (index == ClassifyParams.COLLEGE_ALL) {
                    Intent t = new Intent(context, CollegeListActivity.class);
                    t.putExtra("data", item);
                    startActivity(t);
                    finish();
                }
            }
        });
    }

    private void initRightMenu(String id) {
        pdc.knowledgeLib(HTTP_TASK_TAG, id, new ZxlGenericsCallback<List<KnowledgeLibrary>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<KnowledgeLibrary> knowledgeLibraries, int i) {
                lvRight.setAdapter(new MyAdapter(knowledgeLibraries));
            }
        });
    }

    class MyAdapter extends BaseAdapter {
        private List<KnowledgeLibrary> datas;

        public MyAdapter(List<KnowledgeLibrary> data) {
            this.datas = data;
        }

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public Object getItem(int i) {
            return datas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            BaseAdapterHelper helper = BaseAdapterHelper.get(context, view, viewGroup, R.layout.item_layout_right);
            KnowledgeLibrary library = datas.get(i);
            helper.setText(R.id.tv_right_menu, library.columnName + "");
            return helper.getView();
        }
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
