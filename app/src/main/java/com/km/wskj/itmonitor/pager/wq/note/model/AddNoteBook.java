package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/3/28.
 */

public class AddNoteBook extends BaseBean {
    /**
     * typeName : 我的英语笔记本
     * id : 599a784c7e6a4b5789c4da8a2ff344d9
     * userId : 41ba6d3de08e42059d25e4fa14be7e98
     */

    @JSONField(name = "typeName")
    public String typeName;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "userId")
    public String userId;
}
