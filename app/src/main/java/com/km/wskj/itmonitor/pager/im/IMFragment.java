package com.km.wskj.itmonitor.pager.im;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.im.ui.ConversationListFragment;
import com.km.wskj.itmonitor.pager.login.LoginActivity;
import com.km.wskj.itmonitor.pager.login.RegActivity;
import com.km.wskj.itmonitor.utils.DialogPlusUtils;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author by Xianling.Zhou on 2017/2/9.
 */
//交流fragment
public class IMFragment extends LFragment {
    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_right)
    ImageView mMenu;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_im, container, false);
        ButterKnife.bind(this, view);
        //沉浸shi
        StatusBarCompat.compat(getActivity(), getResources().getColor(R.color.colorWs_blue));
        initData();

        ConversationListFragment fragment = new ConversationListFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.fl_im_container, fragment);
        transaction.show(fragment);
        transaction.commit();
        return view;
    }

    private void initData() {
        mTitle.setText("交流");
        mMenu.setVisibility(View.VISIBLE);
        mMenu.setImageResource(R.mipmap.im_people);
    }

    @OnClick(R.id.tool_bar_right)
    public void menu() {
        //判断当前用户是否登陆
        if (pdc.mCurrentUser == null) {
            normalToasty("请先登录");
            startActivity(new Intent(getContext(), LoginActivity.class));
            return;
        }
        startActivity(new Intent(getActivity(), ContactListActivity.class));
    }
}
