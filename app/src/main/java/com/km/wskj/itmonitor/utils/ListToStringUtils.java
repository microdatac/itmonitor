package com.km.wskj.itmonitor.utils;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/31.
 */

public class ListToStringUtils {

    public static String listToString(List list, String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
            if (i < list.size() - 1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }
}
