package com.km.wskj.itmonitor.pager.mine;

import android.app.Application;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.ClearEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * @author by Xianling.Zhou on 2017/3/23.
 */
//设置服务器监控地址
public class SetHostUrlActivity extends LActivity {

    private static final String TAG = "SetHostUrlActivity";
    //ui
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.et_ip_url)
    ClearEditText mEtIpUrl;
    @BindView(R.id.btn_submit)
    FancyButton mSubmit;

    //data
    private LApplication lApplication;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_hosturl);
        ButterKnife.bind(this);
        initData();
        lApplication = (LApplication) getApplication();
    }

    private void initData() {
        mBack.setVisibility(View.VISIBLE);
        mTitle.setText("设置服务器地址");

    }

    @OnClick({R.id.tool_bar_back, R.id.btn_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tool_bar_back:
                finish();
                break;
            case R.id.btn_submit:
                String ip = mEtIpUrl.getText().toString();

                if (ip.equals("")) {
                    normalToasty("请输入配置地址");
                    return;
                }
                LogUtils.e(lApplication.getBaseUrl() + "默认地址");
                lApplication.setBaseUrl(ip);
                LogUtils.e(lApplication.getBaseUrl() + "更改后的地址");
                normalToasty("配置成功");
                finish();
                break;
        }
    }
}
