package com.km.wskj.itmonitor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.km.wskj.itmonitor.pager.base.LActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by Xianling.Zhou on 2017/4/10.
 */

public class PhotoActivity extends LActivity {
    @BindView(R.id.image_view)
    ImageView imageView;
    private String url;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("image_url"))
            url = getIntent().getStringExtra("image_url");


        initData();
    }

    private void initData() {
        Glide.with(context).load(url).into(imageView);
    }
}
