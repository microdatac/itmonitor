package com.km.wskj.itmonitor.pager.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.view.StarView;

public class testActivity extends AppCompatActivity {
   private StarView starView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        InitData();

    }

    private void InitData() {
        starView = (StarView) findViewById(R.id.star_view);
        starView.setStarViewClickListener(new StarView.StarViewClickListener() {
            @Override
            public void onStarViewClick(int id) {
            Log.i("onclick",id+"");

            }
        });
    }

}
