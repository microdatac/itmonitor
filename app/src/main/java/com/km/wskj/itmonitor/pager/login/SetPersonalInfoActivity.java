package com.km.wskj.itmonitor.pager.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.MainActivity;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.ClearEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/22.
 */


//注册设置个人信息页面
public class SetPersonalInfoActivity extends LActivity {
    private static final String TAG = "SetPersonalInfoActivity";
    //ui
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.et_nickname)
    ClearEditText etNickname;
    @BindView(R.id.et_pass)
    ClearEditText etPass;
    @BindView(R.id.btn_submit)
    FancyButton btnSubmit;
    //data
    private String userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_personal_info);
        ButterKnife.bind(this);

        userId = getIntent().getStringExtra("id");
        toolBarBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText("设置个人信息");
    }

    @OnClick(R.id.btn_submit)
    public void onClick() {

        String nickName = etNickname.getText().toString();
        String pass = etPass.getText().toString();

        if (nickName.equals("")) {
            normalToasty("请输入用户名");
            return;
        }

        if (pass.equals("")) {
            normalToasty("请输入密码");
            return;
        }

        pdc.updateInfo(HTTP_TASK_TAG, userId, nickName, pass, null, pdc.new LoginCallback() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(Object o, int i) {
                normalToasty("设置信息成功");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent t = new Intent(context, MainActivity.class);
                        t.putExtra("index", "1");//用来标记跳转到我的
                        startActivity(t);
                        finish();
                        overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
                    }

                }, 500);
            }
        });


    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }
}
