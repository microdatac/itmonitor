package com.km.wskj.itmonitor.global;

/**
 * @author by Xianling.Zhou on 2017/4/28.
 */
//定义全局分类参数
public class ClassifyParams {

    //发帖子选择分类
    public final static int SEND_FORUM_CARD = 1;
    //技术论坛查看全部
    public final static int FORUM_ALL = 2;
    //微学院查看全部
    public final static int COLLEGE_ALL = 3;
}
