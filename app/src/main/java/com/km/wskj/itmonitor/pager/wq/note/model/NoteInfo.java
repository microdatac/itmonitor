package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/3/28.
 */

public class NoteInfo extends BaseBean {

    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "author")
    public Object author;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "source")
    public Object source;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "desc")
    public String desc;


}
