package com.km.wskj.itmonitor.pager.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlCallBack;
import com.km.wskj.itmonitor.pager.MainActivity;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.control.LoginControl;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.ClearEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.toolsfinal.StringUtils;
import okhttp3.Call;
import okhttp3.Response;

//已废弃 更换为手机好验证码注册登陆
public class RegActivity extends LActivity {
    private static final String TAG = "RegActivity";

    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.et_reg_username)
    ClearEditText mNickName;
    @BindView(R.id.et_reg_phone)
    ClearEditText mPhone;
    @BindView(R.id.et_reg_pass)
    ClearEditText mPass;
    @BindView(R.id.et_reg_repass)
    ClearEditText mRePass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        ButterKnife.bind(this);

        mTitle.setText("注册");
        mBack.setVisibility(View.VISIBLE);
        initView();
        initData();
    }

    private void initView() {

    }

    private void initData() {

    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }

    @OnClick(R.id.btn_reg)
    public void reg(View v) {
        String nickName = mNickName.getText().toString();
        final String phoneNum = mPhone.getText().toString();
        final String pass = mPass.getText().toString();
        String rePass = mRePass.getText().toString();

        if (StringUtils.isEmpty(nickName)) {
            Toast.makeText(context, "请输入用户名", Toast.LENGTH_SHORT).show();
            return;
        }

        if (StringUtils.isEmpty(phoneNum)) {
            Toast.makeText(context, "请输入手机号", Toast.LENGTH_SHORT).show();
            return;
        }

        if (StringUtils.isEmpty(pass)) {
            Toast.makeText(context, "请输入密码", Toast.LENGTH_SHORT).show();
            return;
        }

        if (StringUtils.isEmpty(rePass)) {
            Toast.makeText(context, "请输入确认密码", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!pass.equals(rePass)) {
            Toast.makeText(context, "两次密码不一致，重新输入", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, "请稍后", "注册中...", true);
        pdc.reg(HTTP_TASK_TAG
                , phoneNum
                , pass
                , nickName
                , new ZxlCallBack() {
                    @Override
                    public Object parseNetworkResponse(Response response, int i) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int i) {
                        progressDialog.dismiss();
                        showErrorTip("注册失败:" + e.getLocalizedMessage());
                        LogUtils.e(TAG, e);
                    }

                    @Override
                    public void onResponse(Object o, int i) {
                        progressDialog.dismiss();
                        final ProgressDialog progressDialog = ProgressDialog.show(context, "请稍后", "注册成功，登陆中...", true);
                        pdc.login(HTTP_TASK_TAG, phoneNum, pass, pdc.new LoginCallback() {
                            @Override
                            public void onError(Call call, Exception e, int i) {
                                progressDialog.dismiss();
                                LogUtils.e(TAG, e);
                            }

                            @Override
                            public void onResponse(Object o, int i) {
                                progressDialog.dismiss();
                                showSuccTip("登录成功");
                                LoginControl.saveLoginName(context, phoneNum);
                                IMLogin(phoneNum, pass);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        startActivity(new Intent(context, MainActivity.class));
                                        finish();
                                        overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
                                    }

                                }, 500);
                            }
                        });

                        overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);

                    }
                });
    }


    @OnClick(R.id.tool_bar_back)
    public void back(View v) {
        finish();
    }

    //-------------------------环信--------------------------

    private void IMRegist(final String username, final String password) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    EMClient.getInstance().createAccount(username, password);
                    IMLogin(username, password);
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    LogUtils.e("注册失败" + e.toString());
                }
            }
        }).start();
    }

    private void IMLogin(final String username, final String password) {
        EMClient.getInstance().login(username, password, new EMCallBack() {
            @Override
            public void onSuccess() {
                LogUtils.e("环信登陆成功！！！！");
                // ** manually load all local groups and conversation
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
            }

            @Override
            public void onError(int i, String s) {
                LogUtils.e(i + "," + s);
                IMRegist(username, password);
            }

            @Override
            public void onProgress(int i, String s) {
                LogUtils.e(i + "," + s);
            }
        });
    }
}
