package com.km.wskj.itmonitor.utils;

import android.graphics.Color;
import android.graphics.Typeface;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/2/14.
 */
//动态折线图
public class DynamicChartUtils {
    public static void initChart(LineChart chart, int color) {
        LineData data = new LineData();
        //不要文本描述
        chart.getDescription().setEnabled(false);
        // mChart.setDrawHorizontalGrid(false);
        //
        //不要网格背景
        chart.setDrawGridBackground(false);
//        chart.getRenderer().getGridPaint().setGridColor(Color.WHITE & 0x70FFFFFF);
        // 手势触摸
        chart.setTouchEnabled(true);
        //缩放比例和拖动
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        //如果禁用,扩展可以在x轴和y轴分别完成
        chart.setPinchZoom(false);
        chart.setBackgroundColor(color);
        //设置自定义图表补偿(自动补偿计算在此禁用)
        chart.setViewPortOffsets(10, 0, 10, 0);
        //设置边框
        chart.setDrawBorders(false);
        chart.setBorderWidth(0.5f);
        chart.setBorderColor(Color.WHITE);

        // 先增加一个空的数据，随后往里面动态添加
        chart.setData(data);

        // 图表的注解(只有当数据集存在时候才生效)
        Legend l = chart.getLegend();
        l.setEnabled(false);

        chart.getAxisLeft().setEnabled(false);
        chart.getAxisLeft().setSpaceTop(40f);
        chart.getAxisLeft().setSpaceBottom(40f);
        chart.getAxisRight().setEnabled(false);

        //x轴
        chart.getXAxis().setEnabled(false);
        chart.getXAxis().setAxisMaximum(100);
        chart.getXAxis().setSpaceMin(5);

        // animate calls invalidate()...
//        /动画调用无效()……
        chart.animateX(2500);
    }

    //动态添加坐标点
    public static void addEntry(LineChart chart, float f) {
        LineData data = chart.getData();
        // 每一个LineDataSet代表一条线，每张统计图表可以同时存在若干个统计折线，这些折线像数组一样从0开始下标。
        // 本例只有一个，那么就是第0条折线
        LineDataSet set1 = (LineDataSet) data.getDataSetByIndex(0);

        // 如果该统计折线图还没有数据集，则创建一条出来，如果有则跳过此处代码。
        if (set1 == null) {
            set1 = createLineDataSet();
            data.addDataSet(set1);
        }

        // 先添加一个x坐标轴的值
        // 因为是从0开始，data.getXValCount()每次返回的总是全部x坐标轴上总数量，所以不必多此一举的加1
        data.addDataSet(set1);


        // set.getEntryCount()获得的是所有统计图表上的数据点总量，
        // 如从0开始一样的数组下标，那么不必多次一举的加1
        Entry entry = new Entry(set1.getEntryCount(), f);

        // 往linedata里面添加点。注意：addentry的第二个参数即代表折线的下标索引。
        // 因为本例只有一个统计折线，那么就是第一个，其下标为0.
        // 如果同一张统计图表中存在若干条统计折线，那么必须分清是针对哪一条（依据下标索引）统计折线添加。
        data.addEntry(entry, 0);

        // 像ListView那样的通知数据更新
        chart.notifyDataSetChanged();

        // 当前统计图表中最多在x轴坐标线上显示的总量
        chart.setVisibleXRangeMaximum(20f);

        // y坐标轴线最大值
        // mChart.setVisibleYRange(30, AxisDependency.LEFT);

        // 将坐标移动到最新
        // 此代码将刷新图表的绘图
        chart.moveViewToX(data.getXMax() - 20f);
        // mChart.moveViewTo(data.getXValCount()-7, 55f,
        // AxisDependency.LEFT);

    }

    // 初始化数据集，添加一条统计折线，可以简单的理解是初始化y坐标轴线上点的表征
    private static LineDataSet createLineDataSet() {
        LineDataSet set1 = new LineDataSet(null, "动态添加数据");
        // set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        set1.setLineWidth(1.25f);
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);// 设置平滑曲线
        set1.setCircleRadius(5f);
        set1.setCircleHoleRadius(2.5f);
        set1.setColor(Color.parseColor("#1496EC"));// 设置曲线颜色
        set1.setCircleColor(Color.WHITE);
        set1.setHighLightColor(Color.WHITE);
        set1.setHighlightEnabled(true);//不显示定位线
        set1.setDrawValues(false);// 不显示坐标点的数据
        set1.setDrawCircles(false);//// 不显示坐标点的小圆点

        return set1;
    }
}
