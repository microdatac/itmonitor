package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/3/28.
 */

public class NoteImgs extends BaseBean {
    @JSONField(name = "file_path")
    public String filePath;
    @JSONField(name = "new_file_name")
    public String newFileName;
    @JSONField(name = "create_time")
    public String create_time;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "attach_id")
    public String attach_id;
    @JSONField(name = "host")
    public String host;
    @JSONField(name = "noteId")
    public String noteId;
    @JSONField(name = "original_file_name")
    public String originalFileName;
    @JSONField(name = "attachId")
    public String attachId;
    @JSONField(name = "type")
    public int type;
    @JSONField(name = "suffix")
    public String suffix;
}
