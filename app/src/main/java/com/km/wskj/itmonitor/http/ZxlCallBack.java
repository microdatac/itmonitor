package com.km.wskj.itmonitor.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zxl.zxlapplibrary.http.callback.Callback;
import com.zxl.zxlapplibrary.util.LogUtils;

import okhttp3.Response;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */

public abstract class ZxlCallBack<T> extends Callback<T> {
    @Override
    public String validateReponse(Response response, int id) throws Exception {
        JSONObject json = JSON.parseObject(response.body().string());
        if (json == null) {
            return ("Response is not json data!");
        } else {
            LogUtils.d("response:" + json.toJSONString());
            String data = json.getString("DATA");
            int ecode = json.getIntValue("ECODE");//该值的约定可变
            String msg = json.getString("MSG");
            if (ecode == 10000) {
                validateData = data;
                return null;
            } else {
               /* switch (ecode) {
                    case 11004:
                        msg = "密码错误";
                        break;
                    case 11003:
                        msg = "用户不存在";
                        break;
                }
                return msg + ",错误代码:" + ecode;*/
                return data;
            }
        }
    }
}
