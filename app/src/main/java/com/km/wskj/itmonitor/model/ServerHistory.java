package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;


/**
 * @author by Xianling.Zhou on 2017/2/23.
 */

public class ServerHistory {
   /* memoryTimes true array 内存时间
    cpuTimes true array CPU时间
    cpuTotals true array CPU使用率
    memoryVirtual true array 虚拟内存使用率
    memoryPhysics true array 物理内存使用率*/

    @JSONField(name = "memoryTimes")
    public String memoryTime;
    @JSONField(name = "cpuTimes")
    public String cpuTime;
    @JSONField(name = "cpuTotals")
    public String cpuTotal;
    @JSONField(name = "memoryVirtual")
    public String memoryVirtual;
    @JSONField(name = "memoryPhysic")
    public String memoryPhysic;
}
