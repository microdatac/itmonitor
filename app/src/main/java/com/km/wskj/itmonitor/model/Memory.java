package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/10.
 */

public class Memory extends BaseBean {
    /*usedPercent true string 已占用百分比
    physicsMem true string 物理内存
    freePhysicsMem true string 可用物理内存
    usedPhysicsMem true string 已用物理内存
    virtualMem true string 虚拟内存
    freeVirtualMem true string 可用虚拟内存
    usedVirtualMem true string 已用虚拟内存*/

    @JSONField(name = "usedPercent")
    public String usedPercent;

    @JSONField(name = "physicsMem")
    public String physicsMem;

    @JSONField(name = "freePhysicsMem")
    public String freePhysicsMem;

    @JSONField(name = "usedPhysicsMem")
    public String usedPhysicsMem;

    @JSONField(name = "virtualMem")
    public String virtualMem;

    @JSONField(name = "freeVirtualMem")
    public String freeVirtualMem;

    @JSONField(name = "usedVirtualMem")
    public String usedVirtualMem;
}
