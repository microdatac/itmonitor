package com.km.wskj.itmonitor.pager.monitor.database.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/4/12.
 */

public class DbMsg extends BaseBean {

    /**
     * DATA : 2017-04-02 20:11:29
     * ECODE : 10000
     */
    @JSONField(name = "DATA")
    public String DATA;
    @JSONField(name = "ECODE")
    public int ECODE;
}
