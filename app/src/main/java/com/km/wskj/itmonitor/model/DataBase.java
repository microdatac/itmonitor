package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/14.
 */

public class DataBase extends BaseBean {
    /*"databaseName": "test",
            "connectType": "jdbc",
            "ip": "10.211.55.3",
            "monitorEndTime": null,
            "serverName": "本地虚拟机W",
            "serverId": "d51c9b7ba03c4b6d8c5d0b05585e6f64",
            "databaseVersion": "Microsoft SQL Server 2008 R2 (RTM) - 10.50.1600.1 (X64) ",
            "databaseType": "3e2f6f6fa511434d85d5b969fb1682a8",
            "password": "chuyun.123456",
            "isMonitor": 0,
            "port": 1433,
            "monitorBegTime": null,
            "createTime": "2017-01-15 22:16:11",
            "monitorCycle": 0,
            "serverType": "2",
            "name": "test",
            "codeName": "SQLServer",
            "id": "7730184b83a045a3857d5c47f87acf1a",
            "isDel": 0,
            "username": "sa"
            status true number 状态 1正常 2故障
            */

    @JSONField(name = "databaseName")
    public String databaseName;//实例名

    @JSONField(name = "connectType")
    public String connectType;

    @JSONField(name = "ip")
    public String ip;

    @JSONField(name = "monitorEndTime")
    public String monitorEndTime;

    @JSONField(name = "serverName")
    public String serverName;

    @JSONField(name = "serverId")
    public String serverId;

    @JSONField(name = "databaseVersion")
    public String databaseVersion;

    @JSONField(name = "databaseType")
    public String databaseType;

    @JSONField(name = "password")
    public String password;

    @JSONField(name = "isMonitor")
    public int isMonitor;

    @JSONField(name = "port")
    public int port;

    @JSONField(name = "monitorBegTime")
    public String monitorBegTime;

    @JSONField(name = "createTime")
    public String createTime;

    @JSONField(name = "monitorCycle")
    public int monitorCycle;

    @JSONField(name = "serverType")
    public String serverType;

    @JSONField(name = "name")
    public String name;

    @JSONField(name = "codeName")
    public String codeName;

    @JSONField(name = "id")
    public String id;

    @JSONField(name = "isDel")
    public int isDel;

    @JSONField(name = "username")
    public String username;

    @JSONField(name = "status")
    public int status;

}
