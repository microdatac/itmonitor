package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by wuhan on 2017/3/16.
 */

public class NoteImage extends BaseBean {
//     "file_path": "/upload/2017/03/08/",
//             "new_file_name": "201703082124345192062.png",
//             "create_time": "2017-03-08 21:34:51",
//             "createTime": "2017-03-16 12:08:46",
//             "attach_id": "5726569ca5894d4b81ef80bb2ea1113f",
//             "articleId": "f0ada0f37bc24735b398eecafa161002",
//             "host": "http://wsiops.iok.la:8080/iOPS",
//             "original_file_name": "QQ图片20170308212941.png",
//             "attachId": "5726569ca5894d4b81ef80bb2ea1113f",
//             "type": 1,
//             "suffix": ".png"
    @JSONField(name = "file_path")
    public String file_path;
    @JSONField(name = "new_file_name")
    public String new_file_name;
    @JSONField(name = "create_time")
    public String create_time;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "attach_id")
    public String attach_id;
    @JSONField(name = "articleId")
    public String articleId;
    @JSONField(name = "host")
    public String host;
    @JSONField(name = "original_file_name")
    public String original_file_name;
    @JSONField(name = "attachId")
    public String attachId;
    @JSONField(name = "type")
    public int type;
    @JSONField(name = "suffix")
    public String suffix;

}
