package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/9.
 */

public class Type extends BaseBean {
    /* id true string ID
     codeName true string 名称
     pid true string 上级ID
     pOrder true number 排序
     createTime true string 创建时间
     remark true string 备注
     isDel true number 是否删除
 */
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "codeName")
    public String codeName;
    @JSONField(name = "pid")
    public String pId;
    @JSONField(name = "pOrder")
    public int pOrder;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "remark")
    public String remark;
    @JSONField(name = "isDel")
    public int isDel;


    public String toString() {
        return codeName;
    }

}
