package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/2.
 */

public class SqlServerIO extends BaseBean {

    /**
     * IO_STALL : 1123
     * Total_IO : 3244032
     * DatabaseFile_Type : DATA_FILE
     * DatabaseName : master
     * IO_Read : 3227648
     * IO_Write : 16384
     */

    @JSONField(name = "IO_STALL")
    public int IOSTALL;
    @JSONField(name = "Total_IO")
    public int TotalIO;
    @JSONField(name = "DatabaseFile_Type")
    public String DatabaseFileType;
    @JSONField(name = "DatabaseName")
    public String DatabaseName;
    @JSONField(name = "IO_Read")
    public int IORead;
    @JSONField(name = "IO_Write")
    public int IOWrite;
}
