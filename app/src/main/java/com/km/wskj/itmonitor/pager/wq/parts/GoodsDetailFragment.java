package com.km.wskj.itmonitor.pager.wq.parts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.GoodsDetail;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.zxl.zxlapplibrary.http.callback.StringCallback;
import com.zxl.zxlapplibrary.util.LogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/27.
 */

public class GoodsDetailFragment extends LFragment {
    private static final String TAG = "GoodsDetailFragment";
    @BindView(R.id.parts_webview)
    WebView mWebView;

    private String id;

    public static GoodsDetailFragment getInstance(String id) {
        GoodsDetailFragment gf = new GoodsDetailFragment();
        gf.id = id;
        return gf;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater
            , @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_goods_detail, null);
        ButterKnife.bind(this, view);
        initData();
        return view;
    }

    private void initData() {
       /* pdc.goodsDetail(HTTP_TASK_TAG, id, new ZxlGenericsCallback<GoodsDetail>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(GoodsDetail gd, int i) {
                WebSettings webSettings = mWebView.getSettings();
                webSettings.setDefaultTextEncodingName("UTF-8");
                mWebView.loadData(gd.goodsInfo.content + "", "text/html; charset=UTF-8", null);
            }
        });*/

        pdc.goodsHtml(HTTP_TASK_TAG, id, new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(String s, int i) {
                WebSettings webSettings = mWebView.getSettings();
                webSettings.setDefaultTextEncodingName("UTF-8");
                mWebView.loadData(s + "", "text/html; charset=UTF-8", null);
            }
        });
    }


}
