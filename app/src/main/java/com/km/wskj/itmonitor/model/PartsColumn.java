package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/28.
 */

public class PartsColumn extends BaseBean {

    /**
     * pOrder : 5
     * createTime : 2017-03-28 11:22:13
     * pid : null
     * remark : null
     * id : c802fb8c3dee4e478c9e27327541feb1
     * isDel : 0
     * columnName : 存储备件
     */

    @JSONField(name = "pOrder")
    public int pOrder;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "pid")
    public Object pid;
    @JSONField(name = "remark")
    public Object remark;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "columnName")
    public String columnName;
}
