package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/21.
 */

public class SqlServerSizeList extends BaseBean {
    /*"total": "5",
            "value": "1"*/
    @JSONField(name = "total")
    public String total;
    @JSONField(name = "value")
    public String value;
}
