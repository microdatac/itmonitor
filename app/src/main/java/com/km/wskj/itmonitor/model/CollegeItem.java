package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/24.
 */
//培训列表
public class CollegeItem extends BaseBean {


    /**
     * videoLength : 600
     * coverImg : {"file_path":"/upload/2017/03/23/","new_file_name":"201703231424315387ced.jpg","create_time":"2017-03-23 14:31:53","attach_id":"9ffdd93bcd1a4e1685ef8d5b55aced04","host":"http://iops.tpddns.cn:8088/iOPS","original_file_name":"1.jpg","suffix":".jpg"}
     * columnId : df2db874f4a8405db39889c3d8ff28e7
     * videoId : 9ffdd93bcd1a4e1685ef8d5b55aced04
     * video_no_img : {"file_path":"/upload/2017/03/23/","new_file_name":"201703231424315387ced.jpg","create_time":"2017-03-23 14:31:53","attach_id":"9ffdd93bcd1a4e1685ef8d5b55aced04","host":"http://iops.tpddns.cn:8088/iOPS","original_file_name":"1.jpg","suffix":".jpg"}
     * title : java培训
     * userId : 222344c76435432da3937a14ca5d71c3
     * content : jdbc数据库连接jdbc数据库连接jdbc数据库连接jdbc数据库连接jdbc数据库连接jdbc数据库连接jdbc数据库连接jdbc数据库连接jdbc数据库连接
     * path : 7180ba1c18f44b7fb82af792f7a802f7,df2db874f4a8405db39889c3d8ff28e7
     * coursewareId : 9ffdd93bcd1a4e1685ef8d5b55aced04
     * coverImgId : 9ffdd93bcd1a4e1685ef8d5b55aced04
     * columnNames : 云计算-服务器虚拟化
     * createTime : 2017-03-23 14:52:02
     * id : 6260c169e1454deab77112c4d67d4f06
     * isDel : 0
     * courseware : {"file_path":"/upload/2017/03/23/","new_file_name":"201703231424315387ced.jpg","create_time":"2017-03-23 14:31:53","attach_id":"9ffdd93bcd1a4e1685ef8d5b55aced04","host":"http://iops.tpddns.cn:8088/iOPS","original_file_name":"1.jpg","suffix":".jpg"}
     * desc : jdbc数据库连接
     */

    @JSONField(name = "videoLength")
    public int videoLength;
    @JSONField(name = "coverImg")
    public CoverImgBean coverImg;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "videoId")
    public String videoId;
    @JSONField(name = "video_no_img")
    public VideoBean video;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "path")
    public String path;
    @JSONField(name = "coursewareId")
    public String coursewareId;
    @JSONField(name = "coverImgId")
    public String coverImgId;
    @JSONField(name = "columnNames")
    public String columnNames;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "courseware")
    public CoursewareBean courseware;
    @JSONField(name = "desc")
    public String desc;


    public static class CoverImgBean extends BaseBean {
        /**
         * file_path : /upload/2017/03/23/
         * new_file_name : 201703231424315387ced.jpg
         * create_time : 2017-03-23 14:31:53
         * attach_id : 9ffdd93bcd1a4e1685ef8d5b55aced04
         * host : http://iops.tpddns.cn:8088/iOPS
         * original_file_name : 1.jpg
         * suffix : .jpg
         */

        @JSONField(name = "file_path")
        public String filePath;
        @JSONField(name = "new_file_name")
        public String newFileName;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "attach_id")
        public String attachId;
        @JSONField(name = "host")
        public String host;
        @JSONField(name = "original_file_name")
        public String originalFileName;
        @JSONField(name = "suffix")
        public String suffix;
    }

    public static class VideoBean extends BaseBean {
        /**
         * file_path : /upload/2017/03/23/
         * new_file_name : 201703231424315387ced.jpg
         * create_time : 2017-03-23 14:31:53
         * attach_id : 9ffdd93bcd1a4e1685ef8d5b55aced04
         * host : http://iops.tpddns.cn:8088/iOPS
         * original_file_name : 1.jpg
         * suffix : .jpg
         */

        @JSONField(name = "file_path")
        public String filePath;
        @JSONField(name = "new_file_name")
        public String newFileName;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "attach_id")
        public String attachId;
        @JSONField(name = "host")
        public String host;
        @JSONField(name = "original_file_name")
        public String originalFileName;
        @JSONField(name = "suffix")
        public String suffix;
    }

    public static class CoursewareBean extends BaseBean {
        /**
         * file_path : /upload/2017/03/23/
         * new_file_name : 201703231424315387ced.jpg
         * create_time : 2017-03-23 14:31:53
         * attach_id : 9ffdd93bcd1a4e1685ef8d5b55aced04
         * host : http://iops.tpddns.cn:8088/iOPS
         * original_file_name : 1.jpg
         * suffix : .jpg
         */

        @JSONField(name = "file_path")
        public String filePath;
        @JSONField(name = "new_file_name")
        public String newFileName;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "attach_id")
        public String attachId;
        @JSONField(name = "host")
        public String host;
        @JSONField(name = "original_file_name")
        public String originalFileName;
        @JSONField(name = "suffix")
        public String suffix;
    }
}
