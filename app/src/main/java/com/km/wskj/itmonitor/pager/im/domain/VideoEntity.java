package com.km.wskj.itmonitor.pager.im.domain;

public class VideoEntity {
	public int ID;
	public String title;
	public String filePath;
	public int size;
	public int duration;
}
