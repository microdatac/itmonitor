package com.km.wskj.itmonitor.pager.monitor.dh;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.Server;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.monitor.dh.domain.DhItem;
import com.km.wskj.itmonitor.pager.monitor.server.ServerDetailActivity;
import com.km.wskj.itmonitor.pager.monitor.server.ServerListActivity;
import com.km.wskj.itmonitor.pager.wq.forum.ForumActivity;
import com.km.wskj.itmonitor.utils.TimeUtils;
import com.km.wskj.itmonitor.view.DashboardView;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.global.Constant;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import zrc.widget.SimpleFooter;
import zrc.widget.SimpleHeader;
import zrc.widget.ZrcListView;

/**
 * @author by Xianling.Zhou on 2017/4/21.
 */
//动环主界面
public class DhActivity extends LActivity {
    private static final String TAG = "DhActivity";

    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)

    TextView toolBarTitle;
    @BindView(R.id.zrcListView)
    ZrcListView mZrcListView;

    MyAdapter mAdapter;
    List<DhItem> mDatas;
    DhItem di;
    Intent mIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dh);
        ButterKnife.bind(this);
        toolBarTitle.setText("动环监控");
        toolBarBack.setVisibility(View.VISIBLE);

        //模拟数据
        mDatas = new ArrayList<>();
        di = new DhItem();
        di.name = "动力配电柜";
        di.type = "1";
        di.url = R.mipmap.icon_dh_pdg70;
        mDatas.add(di);

        di = new DhItem();
        di.name = "USP电池组";
        di.type = "2";
        di.url = R.mipmap.icon_dh_dianchi70;
        mDatas.add(di);

        di = new DhItem();
        di.name = "USP主机";
        di.type = "3";
        di.url = R.mipmap.icon_dh_usp70;
        mDatas.add(di);

        di = new DhItem();
        di.name = "精密空调";
        di.url = R.mipmap.icon_dh_kongtiao70;
        di.type = "4";
        mDatas.add(di);

        di = new DhItem();
        di.name = "漏水检测";
        di.type = "5";
        di.url = R.mipmap.icon_dh_water70;
        mDatas.add(di);

        di = new DhItem();
        di.name = "湿温度";
        di.type = "6";
        di.url = R.mipmap.icon_dh_wd70;
        mDatas.add(di);

        di = new DhItem();
        di.name = "新风机";
        di.url = R.mipmap.icon_dh_fj70;
        di.type = "7";
        mDatas.add(di);


        initPullToRefresh();
    }

    private void initPullToRefresh() {
        //添加头布局
        View view = getLayoutInflater().inflate(R.layout.dh_header, null);
        DashboardView dashboardView = (DashboardView) view.findViewById(R.id.dashboardView);
        dashboardView.setRealTimeValue(2);

        if (mZrcListView.getHeaderViewsCount() == 0)
            mZrcListView.addHeaderView(view, null, true);//注意：第三个参数必须为true，否则无效

        //配置listview
        ColorDrawable line = new ColorDrawable(Color.parseColor("#f1f1f1"));
        mZrcListView.setDivider(line);
        mZrcListView.setDividerHeight(1);

        //设置下拉刷新的样式（可选，但如果没有Header则无法下拉刷新）
        SimpleHeader header = new SimpleHeader(context);
        header.setTextColor(ContextCompat.getColor(context, R.color.text_gray));
        header.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setHeadable(header);

        //设置加载更多的样式（可选）
        SimpleFooter footer = new SimpleFooter(context);
        footer.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setFootable(footer);

        // 设置列表项出现动画（可选）
        //zListView.setItemAnimForTopIn(R.anim.topitem_in);
        mZrcListView.setItemAnimForBottomIn(R.anim.bottomitem_in);

        // 下拉刷新事件回调（可选）
        mZrcListView.setOnRefreshStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onRefresh();
            }
        });

        // 加载更多事件回调（可选）
        mZrcListView.setOnLoadMoreStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onLoadMore();
            }
        });

        // 项目选中
        mZrcListView.setOnItemClickListener(new ZrcListView.OnItemClickListener() {
            @Override
            public void onItemClick(ZrcListView parent, View view, int position, long id) {
                DhItem dhItem = (DhItem) parent.getItemAtPosition(position);
                switch (dhItem.type) {
                    //动力配电柜
                    case "1":
                        mIntent = new Intent(context, PDCActivity.class);
                        startActivity(mIntent);
                        break;
                    // USP电池组
                    case "2":
                        mIntent = new Intent(context, UpsBatteryActivity.class);
                        startActivity(mIntent);
                        break;
                    //USP主机
                    case "3":
                        mIntent = new Intent(context, UpsHostActivity.class);
                        startActivity(mIntent);
                        break;
                    //精密空调
                    case "4":
                        mIntent = new Intent(context, AirCActivity.class);
                        startActivity(mIntent);
                        break;
                    //漏水检测
                    case "5":
                        mIntent = new Intent(context, WaterTestActivity.class);
                        startActivity(mIntent);
                        break;
                    //湿温度
                    case "6":
                        mIntent = new Intent(context, WetTemperatureActivity.class);
                        startActivity(mIntent);
                        break;
                    //新风机
                    case "7":
                        mIntent = new Intent(context, FreshAirMachineActivity.class);
                        startActivity(mIntent);
                        break;
                }
            }
        });

        mAdapter = new MyAdapter();
        mZrcListView.setAdapter(mAdapter);
    }

    private void onRefresh() {
        mAdapter.notifyDataSetChanged();
        mZrcListView.setRefreshSuccess();
    }

    private void onLoadMore() {
        mAdapter.notifyDataSetChanged();
        mZrcListView.setLoadMoreSuccess();
    }

    /**
     * 适配器
     */
    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (mDatas == null) {
                return 0;
            }
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            DhItem item = mDatas.get(position);
            return item;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final BaseAdapterHelper helper = BaseAdapterHelper.get(context, convertView, parent, R.layout.item_dh);
            DhItem item = mDatas.get(position);
            helper.setImageResource(R.id.iv_img, item.url);
            helper.setText(R.id.tv_item_name, item.name);

            return helper.getView();
        }
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
