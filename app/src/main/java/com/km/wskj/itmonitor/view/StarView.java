package com.km.wskj.itmonitor.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Administrator on 2017/1/5.
 */
public class StarView extends View {
    /**
     * 画笔对象的引用
     */
    private Paint paint;
    /**
     * 当前进度
     */
    private int[] progress  ;
    /**
     * 是否显示中间的进度
     */
    private float Reference = 2036;
    private List<int[] > coordinate = new ArrayList<int[] >();
    private int  clickid = 100  ;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    public StarView(Context context) {
        this(context, null);
    }

    public StarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        paint = new Paint();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (progress == null){
            paint.setColor(Color.parseColor("#CCCCCC"));
            paint.setAntiAlias(true);  //消除锯齿
            paint.setStrokeWidth((float)2);
            canvas.drawLine(0,getHeight(),getWidth(),getHeight(), paint);
            canvas.drawLine(0,getHeight()/5*4,getWidth(),getHeight()/5*4, paint);
            canvas.drawLine(0,getHeight()/5*3,getWidth(),getHeight()/5*3, paint);
            canvas.drawLine(0,getHeight()/5*2,getWidth(),getHeight()/5*2, paint);
            canvas.drawLine(0,getHeight()/5*1,getWidth(),getHeight()/5*1, paint);
            paint.setColor(Color.parseColor("#00A0E8"));
            canvas.drawRect(getWidth()/20*1,getHeight()/5*4,getWidth()/20*3,getHeight(), paint);// 长方形
            coordinate.add(new int[]{getWidth()/20*1,getWidth()/20*3,getHeight()/5*4,getHeight()});

            canvas.drawRect(getWidth()/20*5,getHeight()/5*3,getWidth()/20*7,getHeight(), paint);// 长方形
            coordinate.add(new int[]{getWidth()/20*5,getWidth()/20*7,getHeight()/5*3,getHeight()});

            canvas.drawRect(getWidth()/20*9,getHeight()/5*2,getWidth()/20*11,getHeight(), paint);// 长方形
            coordinate.add(new int[]{getWidth()/20*9,getWidth()/20*11,getHeight()/5*2,getHeight()});

            canvas.drawRect(getWidth()/20*13,getHeight()/10*3,getWidth()/20*15,getHeight(), paint);// 长方形
            coordinate.add(new int[]{getWidth()/20*13,getWidth()/20*15,getHeight()/10*3,getHeight()});

            canvas.drawRect(getWidth()/20*17,getHeight()/5*1,getWidth()/20*19,getHeight(), paint);// 长方形
            coordinate.add(new int[]{getWidth()/20*17,getWidth()/20*19,getHeight()/5*1,getHeight()});
            return;
        }
        paint.setColor(Color.parseColor("#6385B1"));
        paint.setAntiAlias(true);  //消除锯齿
        paint.setStrokeWidth((float) 10);
        //设置线宽
        canvas.drawLine(0,getHeight(),getWidth(),getHeight(), paint);
        canvas.drawLine(0,getHeight()/5*4,getWidth(),getHeight()/5*4, paint);
        canvas.drawLine(0,getHeight()/5*3,getWidth(),getHeight()/5*3, paint);
        canvas.drawLine(0,getHeight()/5*2,getWidth(),getHeight()/5*2, paint);
        canvas.drawLine(0,getHeight()/5*1,getWidth(),getHeight()/5*1, paint);
        paint.setColor(Color.parseColor("#00A0E8"));
        canvas.drawRect(getWidth()/20*1,getHeight()/5*4,getWidth()/20*3,getHeight(), paint);// 长方形
    }

    /**
     * 获取进度.需要同步
     * @return
     */
    public synchronized int[] getProgress() {
        return progress;
    }

    /**
     * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步
     * 刷新界面调用postInvalidate()能在非UI线程刷新
     * @param progress
     */
    public synchronized void setProgress(int[] progress) {
        this.progress = progress;
        postInvalidate();
    }


    //定义一个接口对象listerner
    private StarViewClickListener listener;
    //获得接口对象的方法。
    public void setStarViewClickListener(StarViewClickListener listener) {
        this.listener = listener;
    }
    //定义一个接口
    public interface  StarViewClickListener{
        public void onStarViewClick(int id);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                float x=event.getX();
                float y = event.getY();
                Log.i("coordinate.size()",coordinate.size()+"");
                if (coordinate.size()!=0) {

                    for (int i = 0;i<coordinate.size();i++){
                        if (coordinate.get(i)[0]<(int)x && (int)x<coordinate.get(i)[1] && coordinate.get(i)[2]<(int)y && (int)y<coordinate.get(i)[3] ){
                            clickid = i;
                            break;
                        }else {
                            clickid = 100;
                        }
                    }

                }
                if(true){

                    if(listener!=null){
                        Log.i("clickid=",clickid+"");
                        listener.onStarViewClick(clickid);
                    }
                    return true;
                }
                break;

            case MotionEvent.ACTION_UP:
                return true;
        }
        return super.onTouchEvent(event);
    }

}
