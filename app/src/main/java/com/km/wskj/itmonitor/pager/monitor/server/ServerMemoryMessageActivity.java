package com.km.wskj.itmonitor.pager.monitor.server;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingdo.statelayout.StateLayout;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.ServerHdMemory;
import com.km.wskj.itmonitor.model.ServerHdMemoryItem;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.utils.FloatUtils;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import zrc.widget.ZrcListView;

//存储信息
public class ServerMemoryMessageActivity extends LActivity implements StateLayout.OnViewRefreshListener {
    private static final String TAG = "ServerMemoryMessageActi";

    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.zrcListView)
    ZrcListView zrcListView;
    @BindView(R.id.state_layout)
    StateLayout stateLayout;
    @BindView(R.id.donutProgress)
    DonutProgress donutProgress;
    @BindView(R.id.tv_used)
    TextView tvUsed;
    @BindView(R.id.tv_free)
    TextView tvFree;


    //data
    private String ip;

    private List<ServerHdMemoryItem> hdMemoryItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_memory_message);
        ButterKnife.bind(this);
        stateLayout.setRefreshListener(this);
        if (getIntent().hasExtra("ip")) {
            ip = getIntent().getStringExtra("ip");
            mTitle.setText(ip);
        }
        mBack.setVisibility(View.VISIBLE);

        initData();
    }

    private void initData() {
        stateLayout.showLoadingView();
        pdc.serverHdMemory(HTTP_TASK_TAG, ip, new ZxlGenericsCallback<ServerHdMemory>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                stateLayout.showErrorView();
            }

            @Override
            public void onResponse(ServerHdMemory shm, int i) {
                hdMemoryItems = shm.hardwareItems;
                //设置适配器

                float v = FloatUtils.doubleFloat(Float.parseFloat(shm.used) / Float.parseFloat(shm.total)) * 100;
                donutProgress.setText(v + "%");
                donutProgress.setProgress(v);
                donutProgress.setInnerBottomText("已使用容量");
                tvFree.setText(shm.free + "GB");
                tvUsed.setText(shm.used + "GB");
                ColorDrawable line = new ColorDrawable(Color.parseColor("#f1f1f1"));
                zrcListView.setDivider(line);
                zrcListView.setDividerHeight(1);

                zrcListView.setAdapter(new MyAdapter());

                stateLayout.showContentView();
                stateLayout.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void refreshClick() {
        initData();
    }

    @Override
    public void loginClick() {

    }

    /**
     * 适配器
     */
    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (hdMemoryItems == null) {
                return 0;
            }
            return hdMemoryItems.size();
        }

        @Override
        public Object getItem(int position) {
            ServerHdMemoryItem item = hdMemoryItems.get(position);
            return item;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BaseAdapterHelper helper = BaseAdapterHelper.get(context, convertView, parent, R.layout.server_hdm_item);
            ServerHdMemoryItem item = hdMemoryItems.get(position);
            helper.setText(R.id.server_hdm_diskName, item.diskName);
            helper.setText(R.id.server_hdm_diskTotal, "总:" + item.totalVolume + "GB");
            helper.setText(R.id.server_hdm_freeVolume, "可用:" + item.freeVolume + "GB");
            return helper.getView();
        }
    }


    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }

}
