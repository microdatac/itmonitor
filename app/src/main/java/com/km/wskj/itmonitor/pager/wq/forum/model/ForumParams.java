package com.km.wskj.itmonitor.pager.wq.forum.model;

import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/3/31.
 */
//评论上传帖子参数类
public class ForumParams extends BaseBean {

    public String id;//false string 帖子ID 编辑时才需要
    public String userId;//userId true string 发帖人ID
    public String columnId;// columnId true string 所属栏目ID
    public String title;
    public String content;
    public String imgIds;
}
