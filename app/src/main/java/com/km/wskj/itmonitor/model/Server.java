package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/10.
 */
//服务器
public class Server {
    @JSONField(name = "memory")
    public String memory;//内存

    @JSONField(name = "serverName")
    public String serverName;//服务器名称

    @JSONField(name = "osSN")
    public String osSN;//操作系统版本号

    @JSONField(name = "baseStartTime")
    public String baseStartTime;//系统启动时间

    @JSONField(name = "osVersion")
    public String osVersion;//    osVersion true string 操作系统版本

    @JSONField(name = "borderCount")
    public int borderCount;

    @JSONField(name = "baseHostName")
    public String baseHostName;

    @JSONField(name = "id")
    public String id;

    @JSONField(name = "serverTypeName")
    public String serverTypeName;//服务器类型

    @JSONField(name = "cpuCount")
    public int cpuCount;//cpu数量

    @JSONField(name = "netCount")
    public int netCount;

    @JSONField(name = "cpuSN")
    public String cpuSN;//CPU序列号

    @JSONField(name = "osStartTime")
    public String osStartTime;//系统启动时间

    @JSONField(name = "osInstallDate")
    public String osInstallDate;//系统安装时间

    @JSONField(name = "ip")
    public String ip;//ip地址

    @JSONField(name = "baseSN")
    public String baseSN;

    @JSONField(name = "baseCaregory")
    public String baseCaregory;

    @JSONField(name = "osName")
    public String osName;//操作系统名字

    @JSONField(name = "cpuName")
    public String cpuName;//cpu名称


    @JSONField(name = "createTime")
    public String createTime;//创建时间

    @JSONField(name = "serverType")
    public String serverType;//服务器类型

    @JSONField(name = "baseCompany")
    public String baseCompany;

    @JSONField(name = "isDel")
    public int isDel;//是否删除

    @JSONField(name = "cpuInterval")
    public int cpuInterval;//CPU主频

    @JSONField(name = "osCurrentTime")
    public String osCurrentTime;//系统当前时间


    @JSONField(name = "status")
    public int status;//1正常 2告警 3故障

}
