package com.km.wskj.itmonitor.pager.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetServerIpActivity extends AppCompatActivity {
    @BindView(R.id.setserver_address)
    EditText setserverAddress;
    @BindView(R.id.setserver_port)
    EditText setserverPort;
    private String ip;
    private String port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_server_ip);
        ButterKnife.bind(this);
        ip = LApplication.getInstance().sp.getString("ip", "183.224.178.99");
        port = LApplication.getInstance().sp.getString("port", "8088");
        setserverAddress.setText(ip);
        setserverPort.setText(port);

    }

    @OnClick(R.id.set_server_back)
    public void onClick() {
        finish();
    }
}
