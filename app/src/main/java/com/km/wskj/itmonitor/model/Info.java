package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/14.
 */

public class Info extends BaseBean {
    /*id true string 主键ID
    columnId true string 栏目ID
    createTime true string 创建时间
    author true string 作者
    source true string 来源
    title true string 标题
    content true string 内容
    desc true string 描述
    isDel true number 是否删除*/

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "author")
    public String author;
    @JSONField(name = "source")
    public String source;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "isDel")
    public int isDel;

}
