package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/21.
 */
//oracle缓冲命中率
public class OracleCacheHit extends BaseBean {

    /**
     * cache_Hit_Percent : 96.21
     * memory_disk : 0
     * sgaHit : 100
     * librarycacheHit : 99
     * buffer_cache : 100
     * tablespaces : {"PBR":20,"PBW":11,"PYR":20,"PYW":11}
     */

    @JSONField(name = "cache_Hit_Percent")
    public double cacheHitPercent;
    @JSONField(name = "memory_disk")
    public int memoryDisk;
    @JSONField(name = "sgaHit")
    public int sgaHit;
    @JSONField(name = "librarycacheHit")
    public int librarycacheHit;
    @JSONField(name = "buffer_cache")
    public int bufferCache;
    @JSONField(name = "tablespaces")
    public TablespacesBean tablespaces;

    public static class TablespacesBean {
        /**
         * PBR : 20
         * PBW : 11
         * PYR : 20
         * PYW : 11
         */

        @JSONField(name = "PBR")
        public int PBR;
        @JSONField(name = "PBW")
        public int PBW;
        @JSONField(name = "PYR")
        public int PYR;
        @JSONField(name = "PYW")
        public int PYW;
    }
}
