package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;
import com.zxl.zxlapplibrary.model.ImageData;
import com.zxl.zxlapplibrary.util.AbDateUtil;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class NoteDetail extends BaseBean {
    public String id;
    public String title;
    public String content;

    @JSONField(name = "update_time", format = "yyyy-MM-dd HH:mm:ss")
    public Date date;
    @JSONField(serialize = false, deserialize = false)
    public List<ImageData> imgList = new ArrayList<>();


    @JSONField(name = "info")
    public NoteInfo info;
    @JSONField(name = "imgs")
    public List<NoteImgs> imgs;
    @JSONField(name = "files")
    public List<?> files;

    //control
    public boolean ischecked;

    public void copyFrom(NoteDetail ni) {
        this.id = ni.info.id;
        this.title = ni.info.title;
        this.content = ni.info.content;
        this.imgList = ni.imgList;
        LogUtils.e("img list===========" + imgList.size());
    }

    public String genDateStr() {
        return AbDateUtil.getStringByFormat(date, AbDateUtil.dateFormatYMD);
    }

    /**
     * 生成图片ids
     * 注意只能是新的图片id
     *
     * @return
     */
    public String genImgIds() {
        String ids = "";
        for (int i = 0; i < imgList.size(); i++) {
            ImageData img = imgList.get(i);
            if (img.fileCompressPath != null) {
                ids += imgList.get(i).imgId + ",";
            }
        }
        if (ids.length() > 0) {
            ids = ids.substring(0, ids.length() - 1);
        }
        return ids;
    }
}
