package com.km.wskj.itmonitor.pager.wq.note;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.wq.forum.model.ForumImg;
import com.km.wskj.itmonitor.pager.wq.note.model.NoteDetail;
import com.km.wskj.itmonitor.pager.wq.note.model.NoteImgs;
import com.km.wskj.itmonitor.pager.wq.note.model.NoteList;
import com.zxl.zxlapplibrary.activity.image.ImageViewPagerActivity;
import com.zxl.zxlapplibrary.model.ImageData;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class NoteDetailActivity extends LActivity {
    private static final String TAG = "NoteDetailActivity";

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_content)
    TextView tv_content;
    @BindView(R.id.layout_pics)
    ViewGroup layout_pics;
    @BindView(R.id.container_pics)
    LinearLayout container_pics;

    //data
    NoteList nl;
    NoteDetail nd;
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;


    private List<String> mImgUrlList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        ButterKnife.bind(this);
        toolBarBack.setVisibility(View.VISIBLE);

        if (getIntent().hasExtra("data")) {
            nl = (NoteList) getIntent().getSerializableExtra("data");
            toolBarTitle.setText(nl.title);
            loadNoteDetail();
        }
    }

    private void loadNoteDetail() {
        pdc.noteDetail_(HTTP_TASK_TAG, nl.id, new ZxlGenericsCallback<NoteDetail>() {

            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(NoteDetail noteDetail, int i) {
                nd = noteDetail;

                refreshUI();
            }
        });


    }

    private void refreshUI() {
        tv_title.setText(nd.info.title);
        tvTime.setText(nd.info.createTime);
        tv_content.setText(nd.info.content);

        List<NoteImgs> imgs = nd.imgs;

        if (imgs.size() > 0) {
            for (int i = 0; i < imgs.size(); i++) {
                ImageData img = new ImageData();
                img.imgId = imgs.get(i).attachId;

                img.imgUrl = imgs.get(i).host
                        + imgs.get(i).filePath
                        + imgs.get(i).newFileName;

                nd.imgList.add(img);
            }
        }

        if (nd.imgList.size() == 0) {
            layout_pics.setVisibility(View.GONE);
        } else {
            layout_pics.setVisibility(View.VISIBLE);
            container_pics.removeAllViews();
            mImgUrlList.clear();
            for (int i = 0; i < nd.imgs.size(); i++) {
                NoteImgs forumImg = imgs.get(i);
                String imgUrl = forumImg.host + forumImg.filePath + forumImg.newFileName;
                ImageView iv = new ImageView(context);
                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                iv.setAdjustViewBounds(true);
                Glide.with(context).load(imgUrl).into(iv);

                mImgUrlList.add(imgUrl);
                iv.setTag(R.id.pictag, Integer.valueOf(i));

                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                p.setMargins(0, 20, 0, 0);

                container_pics.setOnClickListener(piclistener);
                container_pics.addView(iv, p);
            }
        }
    }

    private View.OnClickListener piclistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
