package com.km.wskj.itmonitor.pager.wq.knowledgelib;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingdo.statelayout.StateLayout;
import com.flyco.tablayout.SlidingTabLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.KnowledgeLibrary;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.wq.adapter.MyPagerAdapter;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/14.
 */
//知识列表activity
public class KnowledgeListActivity extends LActivity implements StateLayout.OnViewRefreshListener {
    private static final String TAG = "KnowledgeListActivity";

    //ui
    @BindView(R.id.top_tab)
    SlidingTabLayout mSlidingTab;
    @BindView(R.id.vp_knowledge)
    ViewPager mViewPager;
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.tool_bar_right)
    ImageView toolBarRight;
    @BindView(R.id.state_layout)
    StateLayout stateLayout;

    //data
    private String pid;
    private ArrayList<LFragment> mFragments;
    private ArrayList<String> mTitles;
    private MyPagerAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledge_lib_list);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("id"))
            pid = getIntent().getStringExtra("id");

        if (getIntent().hasExtra("itemName"))
            toolBarTitle.setText(getIntent().getStringExtra("itemName"));

        toolBarBack.setVisibility(View.VISIBLE);
        toolBarRight.setVisibility(View.VISIBLE);
        toolBarRight.setImageResource(R.mipmap.search_white);
        stateLayout.setRefreshListener(this);
        initData();
    }

    private void initData() {
        stateLayout.showLoadingView();
        pdc.knowledgeLib(HTTP_TASK_TAG, pid, new ZxlGenericsCallback<List<KnowledgeLibrary>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                stateLayout.showErrorView();
            }

            @Override
            public void onResponse(List<KnowledgeLibrary> knowledgeLibraries, int i) {
                mFragments = new ArrayList<>();
                mTitles = new ArrayList<>();

                for (KnowledgeLibrary kl : knowledgeLibraries
                        ) {
                    mTitles.add(kl.columnName);
                    mFragments.add(KnowledgeListFragment.getInstance(kl.id));
                }

                mAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
                mViewPager.setAdapter(mAdapter);
                mSlidingTab.setViewPager(mViewPager);

                stateLayout.showContentView();
            }
        });

    }

    @OnClick({R.id.tool_bar_back, R.id.tool_bar_right})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tool_bar_back:
                finish();
                break;
            case R.id.tool_bar_right:
                openIntent(KnowledgeSearchListActivity.class, true);
                break;
        }
    }

    @Override
    public void refreshClick() {
        initData();
    }

    @Override
    public void loginClick() {

    }
}
