package com.km.wskj.itmonitor.pager.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.CodeMsg;
import com.km.wskj.itmonitor.model.RegInfo;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.utils.CountDownTimerUtils;
import com.zxl.zxlapplibrary.http.callback.StringCallback;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.ClearEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/22.
 */
//获取验证码页面
public class RegSendCodeActivity extends LActivity {


    private static final String TAG = "RegSendCodeActivity";
    //ui
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.et_phoneNum)
    ClearEditText etPhoneNum;
    @BindView(R.id.et_code)
    ClearEditText etCode;
    @BindView(R.id.btn_code)
    Button btnCode;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    //data
    private String telNum;
    private String vCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_send_code);
        ButterKnife.bind(this);
        mBack.setVisibility(View.VISIBLE);
        mTitle.setText("手机号注册");
        initData();
    }

    private void initData() {


    }


    @OnClick({R.id.tool_bar_back, R.id.btn_code, R.id.btn_submit})
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.tool_bar_back:
                finish();
                break;
            case R.id.btn_code:
                telNum = etPhoneNum.getText().toString();
                vCode = etCode.getText().toString();
                if (telNum.equals("")) {
                    normalToasty("请输入手机号");
                    return;
                }
                pdc.getCode(HTTP_TASK_TAG, telNum, new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int i) {
                        LogUtils.e(TAG, e);
                    }

                    @Override
                    public void onResponse(String s, int i) {
                        //解析下json
                        CodeMsg cMsg = JSON.parseObject(s, CodeMsg.class);
                        normalToasty(cMsg.DATA);
                        if (cMsg.ECODE == 10000) {
                            CountDownTimerUtils mCountDownTimerUtils = new CountDownTimerUtils(btnCode, 60000, 1000);
                            mCountDownTimerUtils.start();
                        }
                        normalToasty(cMsg.DATA);

                    }
                });

                break;

            case R.id.btn_submit:
                telNum = etPhoneNum.getText().toString();
                vCode = etCode.getText().toString();

                if (telNum.equals("")) {
                    normalToasty("请输入手机号");
                    return;
                }
                if (vCode.equals("")) {
                    normalToasty("请填写验证码");
                    return;
                }
                LogUtils.e(telNum + "--------------" + vCode);

                pdc.regUser(HTTP_TASK_TAG, telNum, vCode, new ZxlGenericsCallback<RegInfo>() {
                    @Override
                    public void onError(Call call, Exception e, int i) {
                        LogUtils.e(TAG, e);
                    }

                    @Override
                    public void onResponse(RegInfo regInfo, int i) {
                        Intent t = new Intent(context, SetPersonalInfoActivity.class);
                        t.putExtra("id", regInfo.userId);
                        startActivity(t);
                    }
                });

                break;
        }
    }
}
