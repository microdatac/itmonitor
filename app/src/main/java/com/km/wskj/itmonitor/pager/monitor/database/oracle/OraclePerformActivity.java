package com.km.wskj.itmonitor.pager.monitor.database.oracle;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.MyChartData;
import com.km.wskj.itmonitor.model.OracleCacheHit;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/21.
 */
//oracle性能页面
public class OraclePerformActivity extends LActivity {

    private static final String TAG = "OraclePerformActivity";
    //ui
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.oracle_sga_hit)
    LineChart oracleSgaHit;
    @BindView(R.id.oracle_lib_cache)
    LineChart oracleLibCache;
    @BindView(R.id.oracle_dd_cache)
    LineChart oracleDdCache;
    @BindView(R.id.oracle_buffer_cache)
    LineChart oracleBufferCache;
    @BindView(R.id.oracle_memory_disk)
    LineChart oracleMemoryDisk;


    //data
    private String id;
    //4个图表对应的数据
    ArrayList<MyChartData> datas1 = new ArrayList<>();
    ArrayList<MyChartData> datas2 = new ArrayList<>();
    ArrayList<MyChartData> datas3 = new ArrayList<>();
    ArrayList<MyChartData> datas4 = new ArrayList<>();
    ArrayList<MyChartData> datas5 = new ArrayList<>();

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // 在此处添加执行的代码
            initData();
            handler.postDelayed(this, 2000);// 5000ms后执行this，即runable
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oracle_perform);
        ButterKnife.bind(this);
        id = getIntent().getStringExtra("id");
        toolBarTitle.setText("性能");
        toolBarBack.setVisibility(View.VISIBLE);
        handler.postDelayed(runnable, 2000);// 打开定时器，2000ms后执行runnable操作
        initView();
        initData();
    }

    private void initData() {
        pdc.oracleCacheHit(HTTP_TASK_TAG, id, new ZxlGenericsCallback<OracleCacheHit>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(OracleCacheHit och, int i) {
                //获取当前系统时分秒
                Calendar now = Calendar.getInstance();
                String str = now.get(Calendar.HOUR_OF_DAY) +
                        ":" + now.get(Calendar.MINUTE) +
                        ":" + now.get(Calendar.SECOND);

                datas1.add(new MyChartData(0f, Float.parseFloat(och.sgaHit + ""), str));
                datas2.add(new MyChartData(0f, Float.parseFloat(och.librarycacheHit + ""), str));
                datas3.add(new MyChartData(0f, Float.parseFloat(och.cacheHitPercent + ""), str));
                datas4.add(new MyChartData(0f, Float.parseFloat(och.bufferCache + ""), str));
                datas5.add(new MyChartData(0f, Float.parseFloat(och.memoryDisk + ""), str));

                initChart(oracleSgaHit, datas1);
                initChart(oracleLibCache, datas2);
                initChart(oracleDdCache, datas3);
                initChart(oracleBufferCache, datas4);
                initChart(oracleMemoryDisk, datas5);
            }
        });
    }

    private void initView() {

    }


    private void initChart(LineChart mChart, final ArrayList<MyChartData> datas) {

        mChart.setDrawGridBackground(false);
        mChart.getDescription().setEnabled(false);
        // add an empty data object
        mChart.setData(new LineData());
        //获取此图表的x轴
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);//设置轴启用或禁用 如果禁用以下的设置全部不生效
        xAxis.setDrawAxisLine(false);//是否绘制轴线
        xAxis.setDrawGridLines(false);//设置x轴上每个点对应的线
        xAxis.setDrawLabels(true);//绘制标签  指x轴上的对应数值
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//设置x轴的显示位置
        xAxis.setTextSize(10f);//设置字体
        xAxis.setTextColor(Color.BLACK);//设置字体颜色
        //设置竖线的显示样式为虚线
        //lineLength控制虚线段的长度
        //spaceLength控制线之间的空间
        xAxis.enableGridDashedLine(10f, 10f, 0f);
//        xAxis.setAxisMinimum(0f);//设置x轴的最小值
//        xAxis.setAxisMaximum(10f);//设置最大值
        xAxis.setAvoidFirstLastClipping(true);//图表将避免第一个和最后一个标签条目被减掉在图表或屏幕的边缘
        xAxis.setLabelRotationAngle(0f);//设置x轴标签的旋转角度
        // 设置x轴显示标签数量  还有一个重载方法第二个参数为布尔值强制设置数量 如果启用会导致绘制点出现偏差
        xAxis.setLabelCount(5);
        xAxis.setGranularity(1f);
        xAxis.setTextColor(Color.BLACK);//设置轴标签的颜色
        xAxis.setTextSize(10f);//设置轴标签的大小
        xAxis.setDrawGridLines(false);//设置不绘制竖线
        xAxis.setGridLineWidth(1f);//设置竖线大小
        xAxis.setGridColor(Color.RED);//设置竖线颜色
        xAxis.setAxisLineColor(Color.BLACK);//设置x轴线颜色
        xAxis.setAxisLineWidth(1f);//设置x轴线宽度

        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return datas.get(Math.min(Math.max((int) value, 0), datas.size() - 1)).xAxisValue;
            }
        });//格式化x轴标签显示字符

        //动态添加数据
        for (int i = 0; i < datas.size(); i++) {
            addEntry(datas.get(i).yValue, mChart);
        }

        //Y轴默认显示左右两个轴线
        YAxis rightAxis = mChart.getAxisRight();  //获取右边的轴线
        rightAxis.setEnabled(false); //设置图表右边的y轴禁用

        YAxis leftAxis = mChart.getAxisLeft();   //获取左边的轴线
        leftAxis.enableGridDashedLine(10f, 10f, 0f); //设置网格线为虚线效果
        leftAxis.setDrawZeroLine(false); //是否绘制0所在的网格线
//        leftAxis.setAxisMaximum(100f);
        leftAxis.setAxisMinimum(0f);
/*
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return (int) value + "%";
            }
        });*/
        leftAxis.setLabelCount(6);
        leftAxis.setTextSize(12f);
        leftAxis.setTextColor(Color.BLACK);

        //设置与图表交互
        mChart.setTouchEnabled(true); // 设置是否可以触摸
        mChart.setDragEnabled(true);// 是否可以拖拽
        mChart.setScaleEnabled(false);// 是否可以缩放 x和y轴, 默认是true
        mChart.setScaleXEnabled(true); //是否可以缩放 仅x轴
        mChart.setScaleYEnabled(true); //是否可以缩放 仅y轴
        mChart.setPinchZoom(true);  //设置x轴和y轴能否同时缩放。默认是否
        mChart.setDoubleTapToZoomEnabled(true);//设置是否可以通过双击屏幕放大图表。默认是true
        mChart.setHighlightPerDragEnabled(true);//能否拖拽高亮线(数据点与坐标的提示线)，默认是true
        mChart.setDragDecelerationEnabled(true);//拖拽滚动时，手放开是否会持续滚动，默认是true（false是拖到哪是哪，true拖拽之后还会有缓冲）
        mChart.setDragDecelerationFrictionCoef(0.99f);//与上面那个属性配合，持续滚动时的速度快慢，[0,1) 0代表立即停止。


        // 设置图例
        Legend l = mChart.getLegend();//图例
        l.setEnabled(false);
        l.setTextSize(10f);//设置文字大小
        l.setForm(Legend.LegendForm.CIRCLE);//正方形，圆形或线
        l.setFormSize(10f); // 设置Form的大小
        l.setWordWrapEnabled(true);//是否支持自动换行 目前只支持BelowChartLeft, BelowChartRight, BelowChartCenter
        l.setFormLineWidth(10f);//设置Form的宽度

        mChart.invalidate();//重绘图
    }

    private void addEntry(float yValue, LineChart mChart) {

        LineData data = mChart.getData();
        //数据展示百分比
//        data.setValueFormatter(new PercentFormatter());
        ILineDataSet set = data.getDataSetByIndex(0);

        // set.addEntry(...); // can be called as well
        if (set == null) {
            set = createSet();
            data.addDataSet(set);
        }
        // choose a random dataSet
        int randomDataSetIndex = (int) (Math.random() * data.getDataSetCount());

        data.addEntry(new Entry(data.getDataSetByIndex(randomDataSetIndex).getEntryCount(), yValue), randomDataSetIndex);

        data.notifyDataChanged();

        // let the chart know it's data has changed
        mChart.notifyDataSetChanged();

        mChart.setVisibleXRangeMaximum(4);
        //mChart.setVisibleYRangeMaximum(15, AxisDependency.LEFT);

        // this automatically refreshes the chart (calls invalidate())
        //这会自动刷新图表
        mChart.moveViewTo(data.getEntryCount() - 7, 50f, YAxis.AxisDependency.LEFT);
    }

    private LineDataSet createSet() {
        //设置数据1  参数1：数据源 参数2：图例名称
        LineDataSet set = new LineDataSet(null, "");
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setColor(getResources().getColor(R.color.colorWs_blue));
        set.setValueTextColor(getResources().getColor(R.color.colorWs_blue));
        set.setCircleColor(getResources().getColor(R.color.colorWs_blue));
        set.setLineWidth(1f);//设置线宽
        set.setCircleRadius(2f);//设置焦点圆心的大小
        set.enableDashedHighlightLine(10f, 5f, 0f);//点击后的高亮线的显示样式
        set.setHighlightLineWidth(1f);//设置点击交点后显示高亮线宽
        set.setHighlightEnabled(true);//是否禁用点击高亮线
        set.setHighLightColor(getResources().getColor(R.color.colorWs_blue));//设置点击交点后显示交高亮线的颜色
        set.setValueTextSize(12f);//设置显示值的文字大小
        set.setDrawFilled(false);//设置禁用范围背景填充

        return set;
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
