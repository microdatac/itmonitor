package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/21.
 */

public class SqlServerSize extends BaseBean {

    @JSONField(name = "free")
    public List<SqlServerSizeList> serverSizeLists;
}
