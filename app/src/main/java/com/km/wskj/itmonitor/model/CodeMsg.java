package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/22.
 */
//验证码信息
public class CodeMsg extends BaseBean {


    /**
     * DATA : 该号码已经注册
     * ECODE : 10001
     */

    @JSONField(name = "DATA")
    public String DATA;
    @JSONField(name = "ECODE")
    public int ECODE;
}
