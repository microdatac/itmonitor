package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/2.
 */
//sqlserver高速缓冲区命中率的值
public class SqlServerB {


    /**
     * b : 100
     */

    @JSONField(name = "b")
    public int b;
}
