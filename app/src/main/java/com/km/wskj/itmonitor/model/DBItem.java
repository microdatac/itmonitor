package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/14.
 */

public class DBItem extends BaseBean {
   /* "pOrder": 1,
            "createTime": "2017-01-09 16:21:31",
            "codeName": "MySql",
            "pid": "7",
            "remark": null,
            "id": "8",
            "isDel": 0*/

    @JSONField(name = "pOrder")
    public int pOrder;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "codeName")
    public String codeName;
    @JSONField(name = "pid")
    public String pid;
    @JSONField(name = "remark")
    public String remark;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;

    public String toString() {
        return codeName;
    }

}
