package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/14.
 */

public class KnowledgeLibrary extends BaseBean {
    /*id true string 主键ID
    pid true 上级ID
    columnName true string 栏目名称
    pOrder true number 上级ID
    createTime true string 创建时间
    isDel true number 是否删除
    remark true string 备注*/

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "pid")
    public String pid;
    @JSONField(name = "columnName")
    public String columnName;
    @JSONField(name = "pOrder")
    public String pOrder;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "remark")
    public String remark;

    public String toString() {
        return columnName;
    }

}
