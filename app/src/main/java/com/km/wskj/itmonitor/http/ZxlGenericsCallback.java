package com.km.wskj.itmonitor.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zxl.zxlapplibrary.http.callback.GenericsCallback;
import com.zxl.zxlapplibrary.http.callback.JsonGenericsSerializator;
import com.zxl.zxlapplibrary.util.LogUtils;

import okhttp3.Response;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */

public abstract class ZxlGenericsCallback<T> extends GenericsCallback<T> {
    /**
     * 默认使用Json序列化数据
     */
    public ZxlGenericsCallback() {
        super(new JsonGenericsSerializator());
    }


    @Override
    public String validateReponse(Response response, int id) throws Exception {
        JSONObject json = JSON.parseObject(response.body().string());
        if (json == null) {
            return ("Response is not json data!");
        } else {
            LogUtils.d("response:" + json.toJSONString());
            String data = json.getString("DATA");
            int ecode = json.getIntValue("ECODE");//该值的约定可变
            String msg = json.getString("msg");

            if (ecode == 10000) {
                validateData = data;
                return null;
            } else {
                return data;
//                return msg + ",错误代码:" + ecode;
            }
        }
    }
}
