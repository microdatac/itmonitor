package com.km.wskj.itmonitor.pager.monitor.dh;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;

/**
 * @author by Xianling.Zhou on 2017/4/24.
 */
//ups主机
public class UpsHostActivity extends LActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upshost);
    }
}
