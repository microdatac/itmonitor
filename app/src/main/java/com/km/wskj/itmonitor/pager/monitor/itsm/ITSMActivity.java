package com.km.wskj.itmonitor.pager.monitor.itsm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.model.FuncItem;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.view.MyGridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author by Xianling.Zhou on 2017/4/18.
 */

//服务器管理主页
public class ITSMActivity extends LActivity {
    //ui
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.container_gridview)
    LinearLayout mContainer;
    @BindView(R.id.bx_donutProgress)
    DonutProgress bxDonutProgress;
    @BindView(R.id.tv_bx_count)
    TextView tvBxCount;
    @BindView(R.id.tv_bx_percent)
    TextView tvBxPercent;
    @BindView(R.id.wt_donutProgress)
    DonutProgress wtDonutProgress;
    @BindView(R.id.tv_wt_count)
    TextView tvWtCount;
    @BindView(R.id.tv_wt_percent)
    TextView tvWtPercent;
    @BindView(R.id.sj_donutProgress)
    DonutProgress sjDonutProgress;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.tv_sj_count)
    TextView tvSjCount;
    @BindView(R.id.tv_sj_percent)
    TextView tvSjPercent;
    @BindView(R.id.bg_donutProgress)
    DonutProgress bgDonutProgress;
    @BindView(R.id.tv_bg_count)
    TextView tvBgCount;
    @BindView(R.id.tv_bg_percent)
    TextView tvBgPercent;

    //data
    private MyGridView gridView;
    List<FuncItem> funcItems = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itsm);
        ButterKnife.bind(this);

        toolBarBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText("服务管理");

        mContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                createGridView();
            }
        });
        initFuncItems();
    }

    //点击事件
    @OnClick({R.id.tool_bar_back, R.id.ll_ljbx, R.id.ll_sjsb, R.id.ll_cjgd, R.id.ll_bytx})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tool_bar_back:
                finish();
                break;
            //立即报修
            case R.id.ll_ljbx:
                normalToasty("尚未开放");
                break;
            //事件上报
            case R.id.ll_sjsb:
                normalToasty("尚未开放");
                break;
            //创建工单
            case R.id.ll_cjgd:
                normalToasty("尚未开放");
                break;
            //保养提醒
            case R.id.ll_bytx:
                normalToasty("尚未开放");
                break;
        }
    }

    private void initFuncItems() {
        String s[] = getResources().getStringArray(R.array.monitor_sm_func_title);
        int r[] = {R.mipmap.icon_sm_bx_manager
                , R.mipmap.icon_sm_gd_manager
                , R.mipmap.icon_sm_by_manager
                , R.mipmap.icon_sm_sj_manager
                , R.mipmap.icon_sm_wt_manager
                , R.mipmap.icon_sm_bg_manager
                , R.mipmap.icon_sm_fb_manager
                , R.mipmap.icon_sm_pz_manager
        };
        funcItems.clear();
        for (int i = 0; i < s.length && i < r.length; i++) {
            FuncItem fi = new FuncItem();
            fi.name = s[i];
            fi.resId = r[i];
            funcItems.add(fi);
        }
    }


    private void createGridView() {

        gridView = new MyGridView(context, new MyGridView.MyGridViewAdapter() {
            @Override
            public int rowspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int leftpadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int toppadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colCount(MyGridView myGridView) {
                return 4;
            }

            @Override
            public int rowHeight(MyGridView myGridView) {
                return mContainer.getHeight() / 2;
            }

            @Override
            public int viewWidth(MyGridView myGridView) {
                return mContainer.getWidth();
            }

            @Override
            public int count(MyGridView myGridView) {
                return funcItems.size();
            }

            @Override
            public View cell(int i, MyGridView myGridView) {
                View view = getLayoutInflater().inflate(R.layout.monitor_item_func, null);
                ImageView iv_pic = (ImageView) view.findViewById(R.id.iv_pic);
                TextView tv_name = (TextView) view.findViewById(R.id.tv_name);

                FuncItem fi = funcItems.get(i);
                iv_pic.setImageResource(fi.resId);
                tv_name.setText(fi.name);
                return view;
            }

            @Override
            public View title(int i, MyGridView myGridView) {
                return null;
            }

            @Override
            public int titleOffsetY(MyGridView myGridView) {
                return 0;
            }

            @Override
            public boolean needGridLine(MyGridView myGridView) {
                return false;
            }
        });


        gridView.setListener(new MyGridView.MyGridViewListener() {
            @Override
            public void onCellTouchDown(int i) {

            }

            @Override
            public void onCellTouchUp(int i) {

            }

            @Override
            public void onCellLongPress(int i) {

            }

            @Override
            public void onCellClick(int i) {
                switch (i) {
                    //报修管理
                    case 0: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //工单管理
                    case 1: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //保养管理
                    case 2: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //事件管理
                    case 3: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //问题管理
                    case 4: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //变更管理
                    case 5: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //发布管理
                    case 6: {
                        normalToasty("尚未开放");
                    }
                    break;
                    //配置管理
                    case 7: {
                        normalToasty("尚未开放");
                    }
                    break;

                }
            }
        });
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mContainer.addView(gridView.getView(), p);
    }
}
