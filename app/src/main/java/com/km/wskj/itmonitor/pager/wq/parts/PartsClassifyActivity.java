package com.km.wskj.itmonitor.pager.wq.parts;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.Internet;
import com.km.wskj.itmonitor.model.KnowledgeLibrary;
import com.km.wskj.itmonitor.model.PartsColumn;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.wq.adapter.MenuAdapter;
import com.km.wskj.itmonitor.pager.wq.adapter.PartsMenuAdapter;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/31.
 */
//配件分类
public class PartsClassifyActivity extends LActivity {
    private static final String TAG = "PartsClassifyActivity";

    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.lv_left)
    ListView lvLeft;
    @BindView(R.id.lv_right)
    ListView lvRight;

    private String mId;
    private PartsMenuAdapter mAdapter;
    private List<PartsColumn> mData;
    private int index;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_classify);
        ButterKnife.bind(this);

        toolBarTitle.setText("选择分类");
        toolBarBack.setVisibility(View.VISIBLE);

        initData();
    }

    private void initData() {
        //要时ListView右侧无滚动条需要禁用普通的滚动条及快速滚动条
        lvLeft.setVerticalScrollBarEnabled(false);
        lvLeft.setFastScrollEnabled(false);
        lvLeft.setDividerHeight(0);

        //要时ListView右侧无滚动条需要禁用普通的滚动条及快速滚动条
        lvRight.setVerticalScrollBarEnabled(false);
        lvRight.setFastScrollEnabled(false);

        ColorDrawable line = new ColorDrawable(Color.parseColor("#eeeeee"));
        lvRight.setDivider(line);
        lvRight.setDividerHeight(1);

        pdc.partsColumn(HTTP_TASK_TAG, mId, new ZxlGenericsCallback<List<PartsColumn>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<PartsColumn> PartsColumns, int i) {
                mData = PartsColumns;
                mAdapter = new PartsMenuAdapter(context, mData);
                mAdapter.setSelectedBackgroundResource(R.mipmap.select_white);//选中时
                mAdapter.setHasDivider(false);
                mAdapter.setNormalBackgroundResource(R.color.menu_bg_gray);//未选中

                lvLeft.setAdapter(mAdapter);
                lvLeft.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (mAdapter != null)
                            mAdapter.setSelectedPos(position);
                        PartsColumn item = (PartsColumn) parent.getItemAtPosition(position);
                        initRightMenu(item.id);
                    }
                });

                //默认选中第一项
                lvLeft.post(new Runnable() {
                    @Override
                    public void run() {
                        lvLeft.getChildAt(0).setBackgroundResource(R.mipmap.select_white);
                        PartsColumn item = (PartsColumn) lvLeft.getItemAtPosition(0);
                        initRightMenu(item.id);
                    }
                });
            }
        });

        //右边选定跳转
        lvRight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PartsColumn item = (PartsColumn) adapterView.getItemAtPosition(i);
                Intent t = new Intent(context, PartsListActivity.class);
                t.putExtra("data", item);
                startActivity(t);
            }
        });
    }

    private void initRightMenu(String id) {
        pdc.partsColumn(HTTP_TASK_TAG, id, new ZxlGenericsCallback<List<PartsColumn>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<PartsColumn> partsColumnS, int i) {
                lvRight.setAdapter(new MyAdapter(partsColumnS));
            }
        });
    }

    class MyAdapter extends BaseAdapter {
        private List<PartsColumn> datas;

        public MyAdapter(List<PartsColumn> data) {
            this.datas = data;
        }

        @Override
        public int getCount() {
            return datas.size();
        }

        @Override
        public Object getItem(int i) {
            return datas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            BaseAdapterHelper helper = BaseAdapterHelper.get(context, view, viewGroup, R.layout.item_layout_right);
            PartsColumn library = datas.get(i);
            helper.setText(R.id.tv_right_menu, library.columnName + "");
            return helper.getView();
        }
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
