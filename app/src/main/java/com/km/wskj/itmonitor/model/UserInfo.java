package com.km.wskj.itmonitor.model;


import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */
//用户信息
public class UserInfo extends BaseBean {

    /**
     * is_locked : 0
     * create_time : 2017-03-22 11:02:50
     * subscribe : null
     * head_img : null
     * sex : 1
     * user_from : iss
     * remark : null
     * area_id : null
     * password_extra : null
     * is_org_admin : 0
     * user_cn_name : null
     * user_role_id : dd3b26f2714c4376a3fb097afd1cb91f
     * password : null
     * user_type : 1
     * user_id : d9600aef565e457c8e3c827f1940f1ed
     * province_id : null
     * tel_no : 18388139726
     * org_id : null
     * nickname : 邹老板
     * is_del : 0
     * post_type : null
     * vercode : null
     * username : 18388139726
     * city_id : null
     */

    @JSONField(name = "is_locked")
    public int isLocked;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "subscribe")
    public Object subscribe;
    @JSONField(name = "head_img")
    public Object headImg;
    @JSONField(name = "sex")
    public int sex;
    @JSONField(name = "user_from")
    public String userFrom;
    @JSONField(name = "remark")
    public Object remark;
    @JSONField(name = "area_id")
    public Object areaId;
    @JSONField(name = "password_extra")
    public Object passwordExtra;
    @JSONField(name = "is_org_admin")
    public int isOrgAdmin;
    @JSONField(name = "user_cn_name")
    public Object userCnName;
    @JSONField(name = "user_role_id")
    public String userRoleId;
    @JSONField(name = "password")
    public Object password;
    @JSONField(name = "user_type")
    public int userType;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "province_id")
    public Object provinceId;
    @JSONField(name = "tel_no")
    public String telNo;
    @JSONField(name = "org_id")
    public Object orgId;
    @JSONField(name = "nickname")
    public String nickname;
    @JSONField(name = "is_del")
    public int isDel;
    @JSONField(name = "post_type")
    public Object postType;
    @JSONField(name = "vercode")
    public Object vercode;
    @JSONField(name = "username")
    public String username;
    @JSONField(name = "city_id")
    public Object cityId;
    @JSONField(name = "host")
    public String host;
}
