package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;


/**
 * @author by Xianling.Zhou on 2017/2/28.
 */
//报警列表
public class AlarmList extends BaseBean {
    /*deviceName true string 报警对象
    deviceId true string 设备ID
    level1Name true string 类型
    level2Name true string 类别
    level3Name true string 分类
    message true string 报警信息
    deviceIp true string 设备IP
    handleTime true number 处理时间
    handleMessage true string 处理内容
    times true number 报警次数
    createTime true string 创建时间
    id true string 主键ID
    level1 true string 类型ID
    level2 true string 类别ID
    level3 true string 分类ID
    status true number 状态 0 未处理 1 已处理*/

    @JSONField(name = "deviceName")
    public String deviceName;
    @JSONField(name = "deviceId")
    public String deviceId;
    @JSONField(name = "level1Name")
    public String level1Name;
    @JSONField(name = "level2Name")
    public String level2Name;
    @JSONField(name = "level3Name")
    public String level3Name;
    @JSONField(name = "message")
    public String message;
    @JSONField(name = "deviceIp")
    public String deviceIp;
    @JSONField(name = "handleTime")
    public String handleTime;
    @JSONField(name = "handleMessage")
    public String handleMessage;
    @JSONField(name = "times")
    public String times;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "level1")
    public String level1;
    @JSONField(name = "level2")
    public String level2;
    @JSONField(name = "level3")
    public String level3;
    @JSONField(name = "status")
    public int status;

}
