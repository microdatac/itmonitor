package com.km.wskj.itmonitor.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.km.wskj.itmonitor.pager.base.LFragment;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/17.
 */

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    List<LFragment> mFragments;

    public MyViewPagerAdapter(FragmentManager fm, List<LFragment> fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }
}
