package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/29.
 */
//上传笔记成功返回的
public class AddCallback {

    /**
     * msg : 保存成功！
     * id : b56813456df5440ba37f04ac4631f574
     * status : y
     * info : {"id":"b56813456df5440ba37f04ac4631f574","title":"测试标题","userId":"e73139460d654221b9d1d607cc9530f9","content":"测试内容"}
     */

    @JSONField(name = "msg")
    public String msg;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "status")
    public String status;
    @JSONField(name = "info")
    public Info info;

    public static class Info {
        /**
         * id : b56813456df5440ba37f04ac4631f574
         * title : 测试标题
         * userId : e73139460d654221b9d1d607cc9530f9
         * content : 测试内容
         */

        @JSONField(name = "id")
        public String id;
        @JSONField(name = "title")
        public String title;
        @JSONField(name = "userId")
        public String userId;
        @JSONField(name = "content")
        public String content;
    }
}
