package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/2/10.
 */

public class ServerHdMemory extends BaseBean {
   /* Total true string 总共空间
    Used true string 已用空间
    Free true string 剩余空间
    items true array[object] 磁盘列表
    status true string 状态*/

    @JSONField(name = "Total")
    public String total;
    @JSONField(name = "Used")
    public String used;
    @JSONField(name = "Free")
    public String free;
    @JSONField(name = "items")
    public List<ServerHdMemoryItem> hardwareItems;

    @JSONField(name = "status")
    public String status;
}
