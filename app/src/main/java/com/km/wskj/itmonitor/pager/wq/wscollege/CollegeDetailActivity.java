package com.km.wskj.itmonitor.pager.wq.wscollege;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.CollegeDetail;
import com.km.wskj.itmonitor.model.CollegeItem;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.util.WpsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/24.
 */

//微学院详情页
public class CollegeDetailActivity extends LActivity {

    private static final String TAG = "CollegeDetailActivity";
    //ui
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.videoplayer)
    JCVideoPlayerStandard mJCVideoPlayer;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.webview_content)
    WebView mWebView;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_courseware)
    TextView tvCourseware;

    //data
    private CollegeItem mCollegeList;
    private CollegeDetail mCollegeDetail;
    private String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_college_detail);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("data"))
            mCollegeList = (CollegeItem) getIntent().getSerializableExtra("data");

        toolBarBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText(mCollegeList != null ? mCollegeList.title : "微详情");

        initData();

    }

    String url_img;
    String url_mp4;

    private void initData() {
        pdc.collegeDetail(HTTP_TASK_TAG, mCollegeList.id, new ZxlGenericsCallback<CollegeDetail>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(CollegeDetail cd, int i) {
                mCollegeDetail = cd;
                //图片url
                if (cd.coverImg != null)
                    url_img = cd.coverImg.host + cd.coverImg.filePath + cd.coverImg.newFileName;
                if (cd.video != null) {
                    url_mp4 = cd.video.host + cd.video.filePath + cd.video.newFileName;
                    mJCVideoPlayer.setUp(url_mp4
                            , JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, cd.train.title);
                }

                tvTitle.setText(cd.train.title + "");
                tvTime.setText(cd.train.createTime + "");

                //加载富文本
                WebSettings webSettings = mWebView.getSettings();
                webSettings.setDefaultTextEncodingName("UTF-8");
                mWebView.loadData(cd.train.content + "", "text/html; charset=UTF-8", null);

                if (cd.courseware != null) {
                    tvCourseware.setVisibility(View.VISIBLE);
                    tvCourseware.setText(cd.courseware.originalFileName + "（点击查看）");
                    url = cd.courseware.host + cd.courseware.filePath + cd.courseware.newFileName;
                }

                Glide.with(mActivity)
                        .load(url_img == null ? R.mipmap.no_img : url_img)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                mJCVideoPlayer.thumbImageView.setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                LogUtils.e("LLpp:加载失败");
                            }
                        }); //方法中设置asBitmap可以设置回调类型
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (mJCVideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mJCVideoPlayer.releaseAllVideos();
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }

    @OnClick(R.id.tv_courseware)
    public void courseware() {
        if (mCollegeDetail.courseware == null)
            return;

        String suffix = mCollegeDetail.courseware.suffix;
        if (suffix.equals(".docx")
                || suffix.equals(".doc")
                || suffix.equals(".pdf")
                || suffix.equals(".pptx")
                || suffix.equals(".ppt")
                || suffix.equals(".xls")
                || suffix.equals(".xlsx "))
            WpsUtil.openDocInWps(context, url, mCollegeDetail.courseware.originalFileName);
    }
}
