package com.km.wskj.itmonitor.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


public class CpuView extends View {
    /**
     * 画笔对象的引用
     */
    private Paint paint;

    /**
     * 圆环的颜色
     */
    private String LineColor = "#0672BA";

    public String getLineColor() {
        return LineColor;
    }

    public void setLineColor(String lineColor) {
        LineColor = lineColor;
    }

    private float progress = 0;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public CpuView(Context context) {
        this(context, null);
    }

    public CpuView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CpuView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        paint = new Paint();

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        paint.setAntiAlias(true);  //消除锯齿

        Path path5 = new Path();

        paint.setColor(Color.parseColor("#6385B1"));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        for (int s = 1; s < 7; s++) {
            for (int i = 0; i < 31; i = i + 2) {
                path5.moveTo(getWidth() / 31 * (i), getHeight() / 6 * s);
                path5.lineTo(getWidth() / 31 * (1 + i), getHeight() / 6 * s);
            }
        }
        path5.close();
        paint.setStrokeWidth(3);
        canvas.drawPath(path5, paint);
        if (progress != 0) {
            paint.setColor(Color.parseColor(LineColor));
            paint.setStyle(Paint.Style.FILL);
            canvas.drawLine(0, (int) ((double) getHeight() / 120.0 * (120.0 - progress)), getWidth(), (int) ((double) getHeight() / 120.0 * (120.0 - progress)), paint);
        }
    }


    /**
     * 获取进度.需要同步
     *
     * @return
     */
    public synchronized double getProgress() {
        return progress;
    }

    /**
     * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步
     * 刷新界面调用postInvalidate()能在非UI线程刷新
     *
     * @param progress
     */
    public synchronized void setProgress(float progress) {
        this.progress = progress;

        postInvalidate();


    }
}
