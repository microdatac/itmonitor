package com.km.wskj.itmonitor.pager.wq.parts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.GlideImageLoader;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.GoodsDetail;
import com.km.wskj.itmonitor.model.Imgs;
import com.km.wskj.itmonitor.model.NewsColumns;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.news.NewsListFragment;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerClickListener;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/27.
 */

public class GoodsFragment extends LFragment {

    private static final String TAG = "GoodsFragment";
    @BindView(R.id.banner)
    Banner mBanner;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_des)
    TextView mTvDes;
    @BindView(R.id.tv_price)
    TextView mTvPrice;

    private String id;
    private List<String> imgUrls = new ArrayList<>();
    private List<String> titles = new ArrayList<>();

    public static GoodsFragment getInstance(String id) {
        GoodsFragment gf = new GoodsFragment();
        gf.id = id;
        return gf;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater
            , @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_goods, null);
        ButterKnife.bind(this, view);
        initData();
        initView();
        return view;
    }

    private void initView() {

    }

    private void initData() {
        pdc.goodsDetail(HTTP_TASK_TAG, id, new ZxlGenericsCallback<GoodsDetail>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(GoodsDetail gd, int i) {
                List<Imgs> imgses = gd.imgses;
                if (imgses.size() != 0) {
                    for (int j = 0; j < imgses.size(); j++) {
                        Imgs imgs = imgses.get(j);
                        imgUrls.add(imgs.host + imgs.file_path + imgs.new_file_name);
                    }

                }

                //设置图片加载器
                mBanner.setImageLoader(new GlideImageLoader());
                //设置图片集合
                mBanner.setImages(imgUrls);
                mBanner.setBannerTitles(titles);
                //设置轮播器样式
                mBanner.setBannerStyle(BannerConfig.NUM_INDICATOR);
                //设置动画
                mBanner.setBannerAnimation(Transformer.DepthPage);
                //设置自动轮播，默认为true
                mBanner.isAutoPlay(true);
                //设置轮播时间
                mBanner.setDelayTime(3000);
                //设置指示器位置（当banner模式中有指示器时）
                mBanner.setIndicatorGravity(BannerConfig.CENTER);
                //banner设置方法全部调用完毕时最后调用
                mBanner.start();

                //轮播器点击事件
                mBanner.setOnBannerClickListener(new OnBannerClickListener() {
                    @Override
                    public void OnBannerClick(int position) {

                    }
                });

                mTvTitle.setText(gd.goodsInfo.title + "");
                mTvPrice.setText("￥" + gd.goodsInfo.price);
            }
        });
    }
}
