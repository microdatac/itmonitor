package com.km.wskj.itmonitor.pager.monitor.server.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/4/19.
 */

public class Rats extends BaseBean {
    /**
     * physicsMem : 0.96
     * freePhysicsMem : 0.07
     * freeVirtualMem : 1.85
     * virtualMem : 2
     */
    //内存
    @JSONField(name = "physicsMem")
    public float physicsMem;
    @JSONField(name = "freePhysicsMem")
    public float freePhysicsMem;
    @JSONField(name = "freeVirtualMem")
    public float freeVirtualMem;
    @JSONField(name = "virtualMem")
    public int virtualMem;

    /**
     * User : 0
     * Total : 0
     * Sys : 0
     */
    //cpu
    @JSONField(name = "User")
    public int User;
    @JSONField(name = "Total")
    public int Total;
    @JSONField(name = "Sys")
    public int Sys;



}
