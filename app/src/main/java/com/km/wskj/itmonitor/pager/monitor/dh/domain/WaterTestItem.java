package com.km.wskj.itmonitor.pager.monitor.dh.domain;

import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/4/24.
 */
//模拟数据模型
public class WaterTestItem extends BaseBean {

    public String area;
    public String state;
}
