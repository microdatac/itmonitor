package com.km.wskj.itmonitor.pager.monitor.alarm;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.AlarmList;
import com.km.wskj.itmonitor.model.AlarmListType;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.global.Constant;
import com.zxl.zxlapplibrary.util.LogUtils;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import zrc.widget.SimpleFooter;
import zrc.widget.SimpleHeader;
import zrc.widget.ZrcListView;


/**
 * @author by Xianling.Zhou on 2017/2/9.
 */
//报警列表
public class AlarmListActivity extends LActivity {
    //ui
    @BindView(R.id.sp_alarm_type)
    Spinner mSpinner;
    @BindView(R.id.zlv_alarm_list)
    ZrcListView mZrcListView;

    //data
    private int lastUpdateNum;
    private MyAdapter mAdapter;
    private int pageIndex;
    private List<AlarmList> alarmList = new ArrayList<>();
    private String deviceId;
    private String level1, level2, level3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_list);
        ButterKnife.bind(this);
        initSpinner();
    }

    private void initData() {
        pdc.alarmList(HTTP_TASK_TAG, deviceId, 1, Constant.PageListSize, -1, level1, level2, level3
                , new ZxlGenericsCallback<List<AlarmList>>() {

                    @Override
                    public void onError(Call call, Exception e, int i) {
                        mZrcListView.setRefreshFail("数据获取失败");
                        LogUtils.e("error", e);
                    }

                    @Override
                    public void onResponse(List<AlarmList> alarmLists, int i) {
                        alarmList = alarmLists;
                        lastUpdateNum = alarmLists.size();
                        pageIndex = 1;
                        //control
                        mAdapter.notifyDataSetChanged();
                        mZrcListView.setRefreshSuccess();

                        if (haveMore())
                            mZrcListView.startLoadMore();
                        else
                            mZrcListView.stopLoadMore();
                    }
                });


        initPullToRefresh();
    }

    private void initPullToRefresh() {
        //配置listview
        ColorDrawable line = new ColorDrawable(Color.parseColor("#666666"));
        mZrcListView.setDivider(line);
        mZrcListView.setDividerHeight(1);

        //设置下拉刷新的样式（可选，但如果没有Header则无法下拉刷新）
        SimpleHeader header = new SimpleHeader(context);
        header.setTextColor(ContextCompat.getColor(context, R.color.text_gray));
        header.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setHeadable(header);

        //设置加载更多的样式（可选）
        SimpleFooter footer = new SimpleFooter(context);
        footer.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setFootable(footer);

        // 设置列表项出现动画（可选）
        //zListView.setItemAnimForTopIn(R.anim.topitem_in);
        mZrcListView.setItemAnimForBottomIn(R.anim.bottomitem_in);

        // 下拉刷新事件回调（可选）
        mZrcListView.setOnRefreshStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onRefresh();
            }
        });

        // 加载更多事件回调（可选）
        mZrcListView.setOnLoadMoreStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onLoadMore();
            }
        });

        // 项目选中
        mZrcListView.setOnItemClickListener(new ZrcListView.OnItemClickListener() {
            @Override
            public void onItemClick(ZrcListView parent, View view, int position, long id) {
                AlarmList item = (AlarmList) parent.getItemAtPosition(position);
                Intent intent = new Intent(context, AlarmDetailActivity.class);
                intent.putExtra("id", item.id);
                startActivity(intent);
            }
        });

        mAdapter = new MyAdapter();
        mZrcListView.setAdapter(mAdapter);
    }

    private void onRefresh() {
        pdc.alarmList(HTTP_TASK_TAG, deviceId, 1, Constant.PageListSize, -1, level1, level2, level3
                , new ZxlGenericsCallback<List<AlarmList>>() {

                    @Override
                    public void onError(Call call, Exception e, int i) {
                        mZrcListView.setRefreshFail("数据获取失败");
                        LogUtils.e("error", e);
                    }

                    @Override
                    public void onResponse(List<AlarmList> alarmLists, int i) {
                        alarmList = alarmLists;
                        lastUpdateNum = alarmLists.size();
                        pageIndex = 1;

                        //control
                        mAdapter.notifyDataSetChanged();
                        mZrcListView.setRefreshSuccess();

                        if (haveMore())
                            mZrcListView.startLoadMore();
                        else
                            mZrcListView.stopLoadMore();
                    }
                });

    }

    private void onLoadMore() {
        pdc.alarmList(HTTP_TASK_TAG, deviceId, pageIndex + 1, Constant.PageListSize, -1, level1, level2, level3
                , new ZxlGenericsCallback<List<AlarmList>>() {

                    @Override
                    public void onError(Call call, Exception e, int i) {
                        mZrcListView.setRefreshFail("数据获取失败");
                        LogUtils.e("error", e);
                    }

                    @Override
                    public void onResponse(List<AlarmList> alarmLists, int i) {

                        lastUpdateNum = alarmLists.size();
                        if (alarmLists.size() > 0) {
                            alarmList.addAll(alarmLists);
                            pageIndex++;
                        }

                        //control
                        mAdapter.notifyDataSetChanged();
                        mZrcListView.setLoadMoreSuccess();
                        if (haveMore())
                            mZrcListView.startLoadMore();
                        else
                            mZrcListView.stopLoadMore();
                    }
                });
    }

    private boolean haveMore() {
        return lastUpdateNum > 0;
    }


    /**
     * 适配器
     */
    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (alarmList == null) {
                return 0;
            }
            return alarmList.size();
        }

        @Override
        public Object getItem(int position) {
            AlarmList item = alarmList.get(position);
            return item;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BaseAdapterHelper helper = BaseAdapterHelper.get(context, convertView, parent
                    , R.layout.alarm_list_item);
            AlarmList item = alarmList.get(position);

            helper.setText(R.id.alarm_item_deviceName, item.deviceName);
            helper.setText(R.id.alarm_item_message, item.message);
            helper.setTag(R.id.alarm_item_createTime, item.createTime);

            //运行状态//状态 0 未处理 1 已处理
            switch (item.status) {
                case 0:
                    helper.setText(R.id.alarm_item_status, "未处理");
                    helper.setBackgroundColor(R.id.alarm_item_status, Color.parseColor("#ed7d31"));
                    break;

                case 1:
                    helper.setText(R.id.alarm_item_status, "已处理");
                    helper.setBackgroundColor(R.id.alarm_item_status, Color.parseColor("#70ad47"));
                    break;

            }
            return helper.getView();
        }
    }

    /**
     * 初始化下拉菜单
     */
    private void initSpinner() {

        pdc.alramType(HTTP_TASK_TAG, new ZxlGenericsCallback<List<AlarmListType>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                showErrorTip("请求失败，请稍后重试！");
            }

            @Override
            public void onResponse(List<AlarmListType> alarmListTypes, int i) {

                AlarmListType at = new AlarmListType();
                at.val = "全部";
                at.id = "";

                List<AlarmListType> mData = new ArrayList<AlarmListType>();
                mData.add(at);
                mData.addAll(alarmListTypes);

                //改变了spinner的默认样式
                ArrayAdapter<AlarmListType> mArrayAdapter = new ArrayAdapter<>(context
                        , android.R.layout.simple_spinner_item, mData);

                //设置样式
                mArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //加载适配器
                mSpinner.setAdapter(mArrayAdapter);
                mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        AlarmListType alarmListType = (AlarmListType) adapterView.getItemAtPosition(i);
                        //*设置spinner字体样式
                        TextView tv = (TextView) view;
                        tv.setTextColor(getResources().getColor(R.color.white));    //设置颜色
                        tv.setTextSize(16.0f);    //设置大小
                        tv.setGravity(Gravity.CENTER_HORIZONTAL);   //设置居中

                        level1 = alarmListType.id;
                        initData();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }
        });


    }


    @OnClick({R.id.iv_alarm_back, R.id.tv_alarm_menu})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_alarm_back:
                finish();
                break;
            case R.id.tv_alarm_menu:
                openIntent(AlarmStatisticsActivity.class, true);
                break;
        }
    }
}
