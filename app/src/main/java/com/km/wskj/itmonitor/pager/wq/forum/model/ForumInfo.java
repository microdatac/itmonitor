package com.km.wskj.itmonitor.pager.wq.forum.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;


public class ForumInfo extends BaseBean {

    /**
     * commentNum : 0
     * path : 1c9506a66eef46559b5ffa6c7c082959,9351ab044f804e2883a0f8e0bf0c34a9
     * viewNum : 0
     * createTime : 2017-03-27 10:01:59
     * columnId : 9351ab044f804e2883a0f8e0bf0c34a9
     * id : 24c8fed574e441a6845fe4bde89321e0
     * title : 本
     * userId : e73139460d654221b9d1d607cc9530f9
     * isDel : 0
     * content : 浪漫满屋五
     */

    @JSONField(name = "commentNum")
    public int commentNum;
    @JSONField(name = "path")
    public String path;
    @JSONField(name = "viewNum")
    public int viewNum;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "content")
    public String content;
}
