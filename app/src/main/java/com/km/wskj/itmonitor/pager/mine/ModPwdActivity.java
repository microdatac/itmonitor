
package com.km.wskj.itmonitor.pager.mine;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.util.ToolsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ModPwdActivity extends LActivity {
    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.tool_bar_right_text)
    TextView mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_pwd);
        ButterKnife.bind(this);
        //ui
        mTitle.setText("修改密码");
        mBack.setVisibility(View.VISIBLE);
        mMenu.setVisibility(View.VISIBLE);
        mMenu.setText("提交");
    }

    @OnClick(R.id.tool_bar_right_text)
    public void menu() {
        submit();
    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }


    private void submit() {
        EditText et_old = (EditText) findViewById(R.id.et_oldpwd);
        EditText et_new = (EditText) findViewById(R.id.et_newpwd);
        EditText et_again = (EditText) findViewById(R.id.et_againpwd);
        String old = et_old.getText().toString();
        String newpwd = et_new.getText().toString();
        String againpwd = et_again.getText().toString();
        if (old.isEmpty() || newpwd.isEmpty() || againpwd.isEmpty()) {
            ToolsUtil.msgbox(this, "不能有项为空");
            return;
        }
        if (!newpwd.equals(againpwd)) {
            ToolsUtil.msgbox(this, "新密码两次输入不一致!");
            return;
        }
        final ProgressDialog dlg = ProgressDialog.show(this, "正在提交", "请稍后...",
                true);

       /* pdc.modPwd(newpwd, old, new WeishuCallback() {
            @Override
            public Object parseNetworkResponse(Response response, int i) throws Exception {
                return null;
            }

            @Override
            public void onError(Call call, Exception e, int i) {
                dlg.dismiss();
                Toast.makeText(ModPwdActivity.this, "" + e.getMessage(),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Object o, int i) {
                dlg.dismiss();
                Toast.makeText(ModPwdActivity.this, "密码修改成功", Toast.LENGTH_LONG)
                        .show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                        overridePendingTransition(R.anim.anim_enter,
                                R.anim.anim_exit);
                    }

                }, 1000);
            }
        });*/

    }
}
