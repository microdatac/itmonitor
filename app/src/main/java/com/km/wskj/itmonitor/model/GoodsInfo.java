package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/27.
 */

public class GoodsInfo extends BaseBean {

    /**
     * applicableModels : DELL
     * columnId : df2db874f4a8405db39889c3d8ff28e7
     * pnNumber : EX103232
     * title : 希捷固态硬盘
     * type : 128GB
     * userId : 222344c76435432da3937a14ca5d71c3
     * bond : 希捷
     * content : 最新固态硬盘，非常不错
     * path : 7180ba1c18f44b7fb82af792f7a802f7,df2db874f4a8405db39889c3d8ff28e7
     * number : 22
     * createTime : 2017-03-23 16:56:05
     * price : 22.22
     * id : d433e4bd53aa416a82f4fb08ac5bbd94
     * isDel : 0
     */

    @JSONField(name = "applicableModels")
    public String applicableModels;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "pnNumber")
    public String pnNumber;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "type")
    public String type;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "bond")
    public String bond;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "path")
    public String path;
    @JSONField(name = "number")
    public int number;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "price")
    public double price;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;
}
