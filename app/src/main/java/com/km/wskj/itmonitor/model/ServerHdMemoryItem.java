package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/10.
 */
//硬盘列表
public class ServerHdMemoryItem extends BaseBean {

    /**
     * totalVolume : 18.0
     * diskName : /
     * freeVolume : 4.9
     */
    @JSONField(name = "totalVolume")
    public String totalVolume;
    @JSONField(name = "diskName")
    public String diskName;
    @JSONField(name = "freeVolume")
    public String freeVolume;
}
