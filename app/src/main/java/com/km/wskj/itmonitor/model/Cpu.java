package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/10.
 */

public class Cpu {
    @JSONField(name = "Total")
    public String total;//占用率

    @JSONField(name = "User")
    public String User;

    @JSONField(name = "Idle")
    public String Idle;

    @JSONField(name = "Sys")
    public String Sys;

    @JSONField(name = "Wait")
    public String Wait;

    @JSONField(name = "time")
    public String time;

    @JSONField(name = "status")
    public String status;

    @JSONField(name = "cupThreshold")
    public String cupThreshold;

}
