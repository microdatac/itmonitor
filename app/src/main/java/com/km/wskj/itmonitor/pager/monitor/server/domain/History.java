package com.km.wskj.itmonitor.pager.monitor.server.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/4/19.
 */

public class History extends BaseBean {
    @JSONField(name = "times")
    public List<String> times;
    @JSONField(name = "rats")
    public List<Rats> rats;
}
