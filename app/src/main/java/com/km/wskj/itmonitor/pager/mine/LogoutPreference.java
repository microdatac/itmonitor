package com.km.wskj.itmonitor.pager.mine;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.preference.Preference;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;


public class LogoutPreference extends Preference {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LogoutPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public LogoutPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LogoutPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LogoutPreference(Context context) {
        super(context);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        final TextView titleView = (TextView) view.findViewById(android.R.id.title);
        if (titleView != null) {
            titleView.setTextColor(Color.parseColor("#D96A5B"));
        }
    }
}
