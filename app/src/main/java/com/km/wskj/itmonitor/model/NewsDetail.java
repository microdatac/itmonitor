package com.km.wskj.itmonitor.model;


import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/15.
 */

public class NewsDetail extends BaseBean {

    @JSONField(name = "imgs")
    public List<Imgs> imgses;
    @JSONField(name = "files")
    public List<Files> files;
    @JSONField(name = "info")
    public Info info;

}
