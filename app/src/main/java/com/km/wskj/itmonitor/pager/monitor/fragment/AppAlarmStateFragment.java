package com.km.wskj.itmonitor.pager.monitor.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LFragment;

import butterknife.ButterKnife;

/**
 * @author by Xianling.Zhou on 2017/2/28.
 */

public class AppAlarmStateFragment extends LFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater
            , @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_app_police, container, false);
        ButterKnife.bind(this, view);

        return view;
    }
}
