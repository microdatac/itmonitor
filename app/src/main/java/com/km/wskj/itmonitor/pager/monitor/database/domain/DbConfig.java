package com.km.wskj.itmonitor.pager.monitor.database.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/4/10.
 */

public class DbConfig extends BaseBean {

    /**
     * VALUE : null
     * KEY : parallel_instance_group
     */
    @JSONField(name = "VALUE")
    public String value;
    @JSONField(name = "KEY")
    public String key;
}
