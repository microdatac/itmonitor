package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/24.
 */
//培训详情
public class CollegeDetail extends BaseBean {

    @JSONField(name = "coverImg")
    public CoverImgBean coverImg;
    @JSONField(name = "video")
    public VideoBean video;
    @JSONField(name = "courseware")
    public CoursewareBean courseware;
    @JSONField(name = "train")
    public TrainBean train;

    public static class CoverImgBean {
        @JSONField(name = "file_path")
        public String filePath;
        @JSONField(name = "new_file_name")
        public String newFileName;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "attach_id")
        public String attachId;
        @JSONField(name = "host")
        public String host;
        @JSONField(name = "original_file_name")
        public String originalFileName;
        @JSONField(name = "suffix")
        public String suffix;
    }

    public static class VideoBean {

        @JSONField(name = "file_path")
        public String filePath;
        @JSONField(name = "new_file_name")
        public String newFileName;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "attach_id")
        public String attachId;
        @JSONField(name = "host")
        public String host;
        @JSONField(name = "original_file_name")
        public String originalFileName;
        @JSONField(name = "suffix")
        public String suffix;
    }

    public static class CoursewareBean {


        @JSONField(name = "file_path")
        public String filePath;
        @JSONField(name = "new_file_name")
        public String newFileName;
        @JSONField(name = "create_time")
        public String createTime;
        @JSONField(name = "attach_id")
        public String attachId;
        @JSONField(name = "host")
        public String host;
        @JSONField(name = "original_file_name")
        public String originalFileName;
        @JSONField(name = "suffix")
        public String suffix;
    }

    public static class TrainBean {

        @JSONField(name = "videoLength")
        public int videoLength;
        @JSONField(name = "columnId")
        public String columnId;
        @JSONField(name = "videoId")
        public String videoId;
        @JSONField(name = "title")
        public String title;
        @JSONField(name = "userId")
        public String userId;
        @JSONField(name = "content")
        public String content;
        @JSONField(name = "path")
        public String path;
        @JSONField(name = "coursewareId")
        public String coursewareId;
        @JSONField(name = "coverImgId")
        public String coverImgId;
        @JSONField(name = "createTime")
        public String createTime;
        @JSONField(name = "id")
        public String id;
        @JSONField(name = "isDel")
        public int isDel;
        @JSONField(name = "desc")
        public String desc;
    }
}
