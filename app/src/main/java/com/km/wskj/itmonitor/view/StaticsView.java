package com.km.wskj.itmonitor.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.km.wskj.itmonitor.R;

/**
 * Created by Administrator on 2016/12/22.
 */

public class StaticsView extends View {
    /**
     * 画笔对象的引用
     */
    private Paint paint;

    /**
     * 当前进度
     */
    private int[] progress  ;
    /**
     * 是否显示中间的进度
     */
    private float Reference = 2036;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    public StaticsView(Context context) {
        this(context, null);
    }

    public StaticsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StaticsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        paint = new Paint();

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (progress == null){
            paint.setColor(Color.parseColor("#6385B1"));
            paint.setAntiAlias(true);  //消除锯齿
            paint.setStrokeWidth((float) 10);
            //设置线宽
            canvas.drawLine(0,getHeight(),getWidth()/10*9,getHeight(), paint);
            canvas.drawLine(0,getHeight(),0,0, paint);
            return;
        }


        paint.setColor(Color.parseColor("#6385B1"));
        paint.setAntiAlias(true);  //消除锯齿
        paint.setStrokeWidth((float) 10);
        //设置线宽
        canvas.drawLine(0,getHeight(),getWidth()/10*9,getHeight(), paint);
        canvas.drawLine(0,getHeight(),0,0, paint);

        paint.setColor(Color.parseColor("#72CBFD"));
        paint.setStyle(Paint.Style.FILL);//设置填满
        paint.setTextSize(26);
//        canvas.drawRect(2,getHeight()/18*16-getHeight()/18/2,getWidth()/5*4,getHeight()-getHeight()/18/2, paint);// 长方形
//        canvas.drawRect(2,getHeight()/18*13-getHeight()/18/2,getWidth()/5*4,getHeight()/18*15-getHeight()/18/2, paint);// 长
//        canvas.drawRect(2,getHeight()/18*10-getHeight()/18/2,getWidth()/5*4,getHeight()/18*12-getHeight()/18/2, paint);// 长
//        canvas.drawRect(2,getHeight()/18*7-getHeight()/18/2,getWidth()/5*4,getHeight()/18*9-getHeight()/18/2, paint);// 长
//        canvas.drawRect(2,getHeight()/18*4-getHeight()/18/2,getWidth()/5*4,getHeight()/18*6-getHeight()/18/2, paint);// 长
//        canvas.drawRect(2,getHeight()/18*1-getHeight()/18/2,getWidth()/5*4,getHeight()/18*3-getHeight()/18/2, paint);// 长
        canvas.drawRect(2,getHeight()/18*16-getHeight()/18/2,getWidth()/5*4*progress[1]/Reference,getHeight()-getHeight()/18/2, paint);// 长方形
        canvas.drawRect(2,getHeight()/18*13-getHeight()/18/2,getWidth()/5*4*progress[3]/Reference,getHeight()/18*15-getHeight()/18/2, paint);// 长
        canvas.drawRect(2,getHeight()/18*10-getHeight()/18/2,getWidth()/5*4*progress[5]/Reference,getHeight()/18*12-getHeight()/18/2, paint);// 长
        canvas.drawRect(2,getHeight()/18*7-getHeight()/18/2,getWidth()/5*4*progress[7]/Reference,getHeight()/18*9-getHeight()/18/2, paint);// 长
        canvas.drawRect(2,getHeight()/18*4-getHeight()/18/2,getWidth()/5*4*progress[9]/Reference,getHeight()/18*6-getHeight()/18/2, paint);// 长
        canvas.drawRect(2,getHeight()/18*1-getHeight()/18/2,getWidth()/5*4*progress[11]/Reference,getHeight()/18*3-getHeight()/18/2, paint);// 长
        paint.setColor(Color.parseColor("#FF723D"));

        canvas.drawRect(5,getHeight()/18*16-getHeight()/18/2+getHeight()/60,getWidth()/5*4*progress[0]/Reference,getHeight()-getHeight()/18/2-getHeight()/60, paint);// 长方形
        canvas.drawRect(5,getHeight()/18*13-getHeight()/18/2+getHeight()/60,getWidth()/5*4*progress[2]/Reference,getHeight()/18*15-getHeight()/18/2-getHeight()/60, paint);// 长方形
        canvas.drawRect(5,getHeight()/18*10-getHeight()/18/2+getHeight()/60,getWidth()/5*4*progress[4]/Reference,getHeight()/18*12-getHeight()/18/2-getHeight()/60, paint);// 长方形
        canvas.drawRect(5,getHeight()/18*7-getHeight()/18/2+getHeight()/60,getWidth()/5*4*progress[6]/Reference,getHeight()/18*9-getHeight()/18/2-getHeight()/60, paint);// 长方形
        canvas.drawRect(5,getHeight()/18*4-getHeight()/18/2+getHeight()/60,getWidth()/5*4*progress[8]/Reference,getHeight()/18*6-getHeight()/18/2-getHeight()/60, paint);// 长方形
        canvas.drawRect(5,getHeight()/18*1-getHeight()/18/2+getHeight()/60,getWidth()/5*4*progress[10]/Reference,getHeight()/18*3-getHeight()/18/2-getHeight()/60, paint);// 长方形
//        canvas.drawRect(5,getHeight()/18*16-getHeight()/18/2+getHeight()/60,getWidth()/5*4,getHeight()-getHeight()/18/2-getHeight()/60, paint);// 长方形
//        canvas.drawRect(5,getHeight()/18*13-getHeight()/18/2+getHeight()/60,getWidth()/5*4,getHeight()/18*15-getHeight()/18/2-getHeight()/60, paint);// 长方形
//        canvas.drawRect(5,getHeight()/18*10-getHeight()/18/2+getHeight()/60,getWidth()/5*4,getHeight()/18*12-getHeight()/18/2-getHeight()/60, paint);// 长方形
//        canvas.drawRect(5,getHeight()/18*7-getHeight()/18/2+getHeight()/60,getWidth()/5*4,getHeight()/18*9-getHeight()/18/2-getHeight()/60, paint);// 长方形
//        canvas.drawRect(5,getHeight()/18*4-getHeight()/18/2+getHeight()/60,getWidth()/5*4,getHeight()/18*6-getHeight()/18/2-getHeight()/60, paint);// 长方形
//        canvas.drawRect(5,getHeight()/18*1-getHeight()/18/2+getHeight()/60,getWidth()/5*4,getHeight()/18*3-getHeight()/18/2-getHeight()/60, paint);// 长方形

        paint.setColor(Color.parseColor("#6385B1"));
        canvas.drawText(progress[0]+"/"+progress[1],getWidth()/5*4*progress[1]/Reference+10,getHeight()/18*16-getHeight()/18/2+getHeight()/18+13, paint);
        canvas.drawText(progress[2]+"/"+progress[3],getWidth()/5*4*progress[3]/Reference+10,getHeight()/18*13-getHeight()/18/2+getHeight()/18+13, paint);
        canvas.drawText(progress[4]+"/"+progress[5],getWidth()/5*4*progress[5]/Reference+10,getHeight()/18*10-getHeight()/18/2+getHeight()/18+13, paint);
        canvas.drawText(progress[6]+"/"+progress[7],getWidth()/5*4*progress[7]/Reference+10,getHeight()/18*7-getHeight()/18/2+getHeight()/18+13, paint);
        canvas.drawText(progress[8]+"/"+progress[9],getWidth()/5*4*progress[9]/Reference+10,getHeight()/18*4-getHeight()/18/2+getHeight()/18+13, paint);
        canvas.drawText(progress[10]+"/"+progress[11],getWidth()/5*4*progress[11]/Reference+10,getHeight()/18*1-getHeight()/18/2+getHeight()/18+13, paint);

//        canvas.drawText("122/122",getWidth()/5*4+10,getHeight()/18*16-getHeight()/18/2+getHeight()/18+15, paint);
//        canvas.drawText("122/122",getWidth()/5*4+10,getHeight()/18*13-getHeight()/18/2+getHeight()/18+15, paint);
//        canvas.drawText("122/122",getWidth()/5*4+10,getHeight()/18*10-getHeight()/18/2+getHeight()/18+15, paint);
//        canvas.drawText("122/122",getWidth()/5*4+10,getHeight()/18*7-getHeight()/18/2+getHeight()/18+15, paint);
//        canvas.drawText("122/122",getWidth()/5*4+10,getHeight()/18*4-getHeight()/18/2+getHeight()/18+15, paint);
//        canvas.drawText("122/122",getWidth()/5*4+10,getHeight()/18*1-getHeight()/18/2+getHeight()/18+15, paint);

    }

    /**
     * 获取进度.需要同步
     * @return
     */
    public synchronized int[] getProgress() {
        return progress;
    }

    /**
     * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步
     * 刷新界面调用postInvalidate()能在非UI线程刷新
     * @param progress
     */
    public synchronized void setProgress(int[] progress) {
//        if(progress < 0){
//            throw new IllegalArgumentException("progress not less than 0");
//        }
//        if(progress > max){
//            progress = max;
//        }
//        if(progress <= max){
//            this.progress = progress;
//            postInvalidate();
//        }
        this.progress = progress;
        postInvalidate();
    }







}
