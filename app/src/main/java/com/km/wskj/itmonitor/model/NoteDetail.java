package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * Created by wuhan on 2017/3/15.
 */

public class NoteDetail extends BaseBean {
//             "path": null,
//             "imgs": [ ],
//             "columnNames": null,
//             "createTime": "2017-03-15 17:59:56",
//             "columnId": null,
//             "id": "bb4c9a5633da4716b3a8368bee254009",
//             "title": "我",
//             "userId": "e73139460d654221b9d1d607cc9530f9",
//             "isDel": 0,
//             "content": null

    @JSONField(name = "path")
    public String path;
    @JSONField(name = "imgs")
    public List<NoteImage> noteImages;
    @JSONField(name = "columnNames")
    public String columnNames;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "content")
    public String content;


}
