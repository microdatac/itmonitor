package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/4/7.
 */
//类型统计
public class TypeStatistics extends BaseBean {

    /**
     * num : 1
     * serverTypeName : Linux
     */

    @JSONField(name = "num")
    public int num;
    @JSONField(name = "typeName")
    public String typeName;
}
