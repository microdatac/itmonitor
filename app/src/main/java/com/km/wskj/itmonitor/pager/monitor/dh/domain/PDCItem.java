package com.km.wskj.itmonitor.pager.monitor.dh.domain;

import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/4/25.
 */
//模拟数据
public class PDCItem extends BaseBean {

    public String itemName;
    public String itemData;
}
