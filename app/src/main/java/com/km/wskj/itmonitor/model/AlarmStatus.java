package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/4/7.
 */

public class AlarmStatus extends BaseBean {

    /**
     * num1 : 4
     * num2 : 17
     */

    @JSONField(name = "num1")
    public int num1;
    @JSONField(name = "num2")
    public int num2;
}
