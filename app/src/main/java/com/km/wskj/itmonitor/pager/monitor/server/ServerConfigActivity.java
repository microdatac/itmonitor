package com.km.wskj.itmonitor.pager.monitor.server;

import android.os.Bundle;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.ServerTypeDetail;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.zxl.zxlapplibrary.util.LogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ServerConfigActivity extends LActivity {
    //ui
    @BindView(R.id.tv_hw_baseCaregory)
    TextView mBaseCaregory;//设备型号
    @BindView(R.id.tv_hw_baseCompany)
    TextView mBaseCompany;//设备厂商
    @BindView(R.id.tv_hw_cpuCount)
    TextView mCpuCount;//cpu核数
    @BindView(R.id.tv_hw_cpuSN)
    TextView mCpuSn;//配置
    @BindView(R.id.tv_hw_diskTotal)
    TextView mTotal;//磁盘容量
    @BindView(R.id.tv_hw_osSN)
    TextView mOssn;//操作系统版本号
    @BindView(R.id.tv_hw_osVersion)
    TextView moSversion;//系统版本
    @BindView(R.id.tv_hw_memory)
    TextView mMem;//内存配置

    //data
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_config);
        ButterKnife.bind(this);
        //沉浸式
        StatusBarCompat.compat(context, getResources().getColor(R.color.colorWs_blue));
        id = getIntent().getStringExtra("id");
        initData();
    }

    private void initData() {
        pdc.serverDetail(HTTP_TASK_TAG, id, new ZxlGenericsCallback<ServerTypeDetail>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                showErrorTip("加载失败");
                LogUtils.e("error", e);
            }

            @Override
            public void onResponse(ServerTypeDetail std, int i) {
                mBaseCaregory.setText("设备型号：" + std.baseCaregory);
                mBaseCompany.setText("设备厂商：" + std.baseCompany);
                mCpuCount.setText("CPU核数：" + std.cpuCount + "核");
                mCpuSn.setText("CPU配置：" + std.cpuSN);
                mTotal.setText("磁盘容量：" + std.diskTotal);
                mOssn.setText("系统版本号：" + std.osSN);
                moSversion.setText("系统版本：" + std.osVersion);
                mMem.setText("内存配置：" + std.memory + "GB");
            }
        });
    }

    @OnClick(R.id.handware_back)
    public void onClick() {
        finish();
    }
}
