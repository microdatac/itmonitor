package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/10.
 */

public class ServerTypeDetail extends BaseBean {

    @JSONField(name = "id")
    public String id;//主键ID

    @JSONField(name = "ip")
    public String ip;//ip

    @JSONField(name = "memory")
    public int memory;//内存空间

    @JSONField(name = "diskTotal")
    public String diskTotal;//磁盘空间

    @JSONField(name = "serverName")
    public String serverName;//服务器名称

    @JSONField(name = "monitorEndTime")
    public String monitorEndTime;


    @JSONField(name = "osSN")
    public String osSN;

    @JSONField(name = "baseStartTime")
    public String baseStartTime;


    @JSONField(name = "osVersion")
    public String osVersion;


    @JSONField(name = "monitorBegTime")
    public String monitorBegTime;

    @JSONField(name = "borderCount")
    public String borderCount;


    @JSONField(name = "monitorCycle")
    public String monitorCycle;


    @JSONField(name = "baseHostName")
    public String baseHostName;

    @JSONField(name = "cpuCount")
    public String cpuCount;

    @JSONField(name = "netCount")
    public String netCount;

    @JSONField(name = "cupThreshold")
    public String cupThreshold;

    @JSONField(name = "cpuSN")
    public String cpuSN;

    @JSONField(name = "osStartTime")
    public String osStartTime;


    @JSONField(name = "osInstallDate")
    public String osInstallDate;

    @JSONField(name = "baseSN")
    public String baseSN;

    @JSONField(name = "baseCaregory")
    public String baseCaregory;

    @JSONField(name = "osName")
    public String osName;

    @JSONField(name = "cpuName")
    public String cpuName;

    @JSONField(name = "hdioThreshold")
    public int hdioThreshold;


    @JSONField(name = "isMonitor")
    public int isMonitor;

    @JSONField(name = "virtualMemoryThreshold")
    public int virtualMemoryThreshold;

    @JSONField(name = "createTime")
    public String createTime;

    @JSONField(name = "memoryThreshold")
    public int memoryThreshold;


    @JSONField(name = "hdStorageThreshold")
    public int hdStorageThreshold;

    @JSONField(name = "serverType")
    public String serverType;

    @JSONField(name = "baseCompany")
    public String baseCompany;

    @JSONField(name = "isDel")
    public int isDel;

    @JSONField(name = "cpuInterval")
    public int cpuInterval;


    @JSONField(name = "osCurrentTime")
    public String osCurrentTime;//


}
