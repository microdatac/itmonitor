package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/4/6.
 */
//极光推送json
public class JPush extends BaseBean {

    /**
     * n_builder_id : 0
     * ad_id : 6408127433
     * n_only : 1
     * m_content : {"n_extras":{"id":"123456"},"n_content":"报警信息api推送测试","n_flag":1,"ad_t":0}
     * show_type : 4
     */

    @JSONField(name = "n_builder_id")
    public int nBuilderId;
    @JSONField(name = "ad_id")
    public String adId;
    @JSONField(name = "n_only")
    public int nOnly;
    @JSONField(name = "m_content")
    public MContentBean mContent;
    @JSONField(name = "show_type")
    public int showType;

    public static class MContentBean {
        /**
         * n_extras : {"id":"123456"}
         * n_content : 报警信息api推送测试
         * n_flag : 1
         * ad_t : 0
         */

        @JSONField(name = "n_extras")
        public NExtrasBean nExtras;
        @JSONField(name = "n_content")
        public String nContent;
        @JSONField(name = "n_flag")
        public int nFlag;
        @JSONField(name = "ad_t")
        public int adT;

        public static class NExtrasBean {
            /**
             * id : 123456
             */
            @JSONField(name = "id")
            public String id;
        }
    }
}
