package com.km.wskj.itmonitor.pager.wq.parts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.KnowledgeLibrary;
import com.km.wskj.itmonitor.model.PartsColumn;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.wq.adapter.MyPagerAdapter;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/4/6.
 */

public class PartsTabListActivity extends LActivity {
    private static final String TAG = "PartsTabListActivity";
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.slidingTabLayout)
    SlidingTabLayout mTabLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewpager;
    @BindView(R.id.tool_bar_right)
    ImageView toolBarRight;

    private PartsColumn mPartsColumn;

    //data
    private ArrayList<LFragment> mFragments;
    private ArrayList<String> mTitles;
    private MyPagerAdapter mMyPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_forum_list);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("data"))
            mPartsColumn = (PartsColumn) getIntent().getSerializableExtra("data");

        toolBarTitle.setText(mPartsColumn != null ? mPartsColumn.columnName : "");
        toolBarBack.setVisibility(View.VISIBLE);
        toolBarRight.setVisibility(View.VISIBLE);
        toolBarRight.setImageResource(R.mipmap.search_white);

        initData();
    }

    private void initData() {
        mFragments = new ArrayList<>();
        mTitles = new ArrayList<>();

        pdc.partsColumn(HTTP_TASK_TAG, mPartsColumn.id, new ZxlGenericsCallback<List<PartsColumn>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<PartsColumn> partsColumns, int i) {
                for (int j = 0; j < partsColumns.size(); j++) {
                    String columnName = partsColumns.get(j).columnName;
                    mTitles.add(columnName);
                    mFragments.add(PartsListFragment.getInstance(partsColumns.get(j)));
                }
                mMyPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
                mViewpager.setAdapter(mMyPagerAdapter);
                mTabLayout.setViewPager(mViewpager);
            }
        });
    }

    @OnClick({R.id.tool_bar_back, R.id.tool_bar_right})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tool_bar_back:
                finish();
                break;
            case R.id.tool_bar_right:
                normalToasty("search");
                break;
        }
    }
}
