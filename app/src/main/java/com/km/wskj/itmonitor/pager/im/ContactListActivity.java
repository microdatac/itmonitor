package com.km.wskj.itmonitor.pager.im;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.im.ui.AddContactActivity;
import com.km.wskj.itmonitor.pager.im.ui.ContactListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * @author by Xianling.Zhou on 2017/2/27.
 */

//联系人列表页面
public class ContactListActivity extends LActivity {
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.tool_bar_right)
    ImageView mAdd;

    private FragmentTransaction transaction;
    private ContactListFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        ButterKnife.bind(this);

        fragment = new ContactListFragment();
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_contact_container, fragment);
        transaction.show(fragment);
        transaction.commit();

        mTitle.setText("联系人");
        mBack.setVisibility(View.VISIBLE);
        mAdd.setVisibility(View.VISIBLE);
        mAdd.setImageResource(R.mipmap.add);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }


    @OnClick(R.id.tool_bar_right)
    public void menu() {
        startActivity(new Intent(context, AddContactActivity.class));
    }
}
