package com.km.wskj.itmonitor.pager.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingdo.statelayout.StateLayout;
import com.flyco.tablayout.SlidingTabLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.NewsColumns;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.wq.adapter.MyPagerAdapter;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/2/9.
 */
//新闻
public class NewsFragment extends LFragment {
    private static final String TAG = "NewsFragment";
    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.vp_news)
    ViewPager mViewPager;
    @BindView(R.id.slidingtab_news)
    SlidingTabLayout mSlidingTab;
    @BindView(R.id.tool_bar_right)
    ImageView toolBarRight;
    @BindView(R.id.state_layout)
    StateLayout stateLayout;

    //data
    private ArrayList<LFragment> mFragments;
    private ArrayList<String> mTitles;
    private MyPagerAdapter mAdapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        //沉浸shi
        StatusBarCompat.compat(getActivity(), getResources().getColor(R.color.colorWs_blue));
        mTitle.setText("新闻");
        toolBarRight.setVisibility(View.VISIBLE);
        toolBarRight.setImageResource(R.mipmap.search_white);

        stateLayout.setRefreshListener(new StateLayout.OnViewRefreshListener() {
            @Override
            public void refreshClick() {
                initData();
            }

            @Override
            public void loginClick() {

            }
        });
        initData();
        return view;
    }

    private void initData() {
        stateLayout.showLoadingView();
        mFragments = new ArrayList<>();
        mTitles = new ArrayList<>();

        pdc.newsColumn(HTTP_TASK_TAG, new ZxlGenericsCallback<List<NewsColumns>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                stateLayout.showErrorView();
            }

            @Override
            public void onResponse(List<NewsColumns> newsColumnses, int i) {
                for (int j = 0; j < newsColumnses.size(); j++) {
                    String columnName = newsColumnses.get(j).columnName;
                    mTitles.add(columnName);
                    mFragments.add(NewsListFragment.getInstance(newsColumnses.get(j)));
                }

                mAdapter = new MyPagerAdapter(getFragmentManager(), mFragments, mTitles);
                mViewPager.setAdapter(mAdapter);
                mViewPager.setOffscreenPageLimit(5);
                mSlidingTab.setViewPager(mViewPager);

                stateLayout.showContentView();
            }
        });
    }

    @OnClick(R.id.tool_bar_right)
    public void onClick() {
        startActivity(new Intent(getActivity(), NewsSearchListActivity.class));
    }
}
