package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/15.
 */

public class NewsColumns extends BaseBean {
    /* "pOrder": 2,
             "createTime": "2017-03-08 17:22:08",
             "pid": null,
             "remark": "紧急发布IT系统安全漏洞",
             "id": "b6599905d1fd47fe9c732c847ede7ccc",
             "isDel": 0,
             "columnName": "安全漏洞"*/

    @JSONField(name = "pOrder")
    public int pOrder;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "pid")
    public String pid;
    @JSONField(name = "remark")
    public String remark;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "columnName")
    public String columnName;
}
