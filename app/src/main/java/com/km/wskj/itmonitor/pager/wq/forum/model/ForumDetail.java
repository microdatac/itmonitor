package com.km.wskj.itmonitor.pager.wq.forum.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

import java.util.List;

//帖子详情
public class ForumDetail extends BaseBean {

    @JSONField(name = "imgs")
    public List<ForumImg> forumImgs;
    @JSONField(name = "info")
    public ForumInfo forumInfo;
}
