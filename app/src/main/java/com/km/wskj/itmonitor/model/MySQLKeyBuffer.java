package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/14.
 */
//统计图表
public class MySQLKeyBuffer {
   /* key_buffer_write_hits true number key Buffer write 命中率
    key_buffer_read_hits true string key Buffer read 命中率
    innodb_buffer_read_hits true string InnoDB Buffer命中率
    Query_cache_hits true number Query Cache命中率
    Thread_cache_hits true string Thread Cache 命中率
    QPS true string QPS (每秒Query量)
    TPS true number TPS(每秒事务量)*/

    @JSONField(name = "key_buffer_write_hits")
    public float key_buffer_write_hits;
    @JSONField(name = "key_buffer_read_hits")
    public String key_buffer_read_hits;
    @JSONField(name = "innodb_buffer_read_hits")
    public String innodb_buffer_read_hits;
    @JSONField(name = "Thread_cache_hits")
    public String Thread_cache_hits;
    @JSONField(name = "QPS")
    public String QPS;
    @JSONField(name = "TPS")
    public int TPS;
}
