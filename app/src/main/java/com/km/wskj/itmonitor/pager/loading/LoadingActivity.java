package com.km.wskj.itmonitor.pager.loading;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;
import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.control.PublicDataControl;
import com.km.wskj.itmonitor.pager.MainActivity;
import com.zxl.zxlapplibrary.activity.loading.MyLoadingActivity;
import com.zxl.zxlapplibrary.control.LoginControl;
import com.zxl.zxlapplibrary.util.LogUtils;

import cn.finalteam.toolsfinal.StringUtils;
import es.dmoral.toasty.Toasty;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/2/8.
 */
//闪屏页
public class LoadingActivity extends MyLoadingActivity {

    private final static String TAG = "LoadingActivity";
    String loginPwd;
    String loginName;
    private String password;

    @Override
    public int loadingResId() {
        return R.mipmap.splash;
    }

    @Override
    public ImageView.ScaleType scaleType() {
        return ImageView.ScaleType.FIT_XY;
    }

    @Override
    public int bgColorId() {
        return R.color.white;
    }

    @Override
    public Intent nextIntent() {
        return null;
    }

    @Override
    public int taskCount() {
        return 3;
    }

    @Override
    public void doLoading() {
        final ProgressDialog pd = new ProgressDialog(context);
        pd.setMessage("尝试自动登陆");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
        final PublicDataControl pdc = LApplication.app.pdc;
        loginName = LoginControl.readLoginName(context);

        if (!StringUtils.isEmpty(loginName))
            loginPwd = LoginControl.readPasswordForName(context, loginName);

        if (!StringUtils.isEmpty(loginName) && !StringUtils.isEmpty(loginPwd)) {
            IMLogin(loginName);//登陆环信
            //自动登陆
            pdc.login(HTTP_TASK_TAG, loginName, loginPwd, pdc.new LoginCallback() {
                @Override
                public void onError(Call call, Exception e, int i) {
                    LogUtils.e(TAG, e);
                    pd.dismiss();
//                    Toasty.warning(context, "自动登陆失败", Toast.LENGTH_SHORT, true).show();
                    pdc.loadingFinish();
//                    startActivity(new Intent(context, MainActivity.class));
                    LoadingActivity.this.finish();
                    overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
                }

                @Override
                public void onResponse(Object o, int i) {
                    pd.dismiss();
                  /*  startActivity(new Intent(context, MainActivity.class));
                    finish();
                    overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);*/
                    pdc.loadingFinish();
                    LoadingActivity.this.finish();
                }
            });

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pd.dismiss();
                    LoadingActivity.this.finish();
                    pdc.loadingFinish();
                    overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
                }
            }, 500);
        }
    }

    @Override
    public int delayTime() {
        return 3;
    }

    @Override
    public void done() {
        LApplication.app.pdc.loadingFinish();
    }

    private void IMLogin(final String username) {
        password = "123456";//环信密码默认123456
        EMClient.getInstance().login(username, password, new EMCallBack() {
            @Override
            public void onSuccess() {
                LogUtils.e("环信登陆成功！！！！");
                // ** manually load all local groups and conversation
                EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
            }

            @Override
            public void onError(int i, String s) {
                LogUtils.e(i + "," + s);
                IMRegist(username, password);
            }

            @Override
            public void onProgress(int i, String s) {
                LogUtils.e(i + "," + s);
            }
        });
    }

    private void IMRegist(final String username, final String password) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    EMClient.getInstance().createAccount(username, password);
                    IMLogin(username);
                } catch (HyphenateException e) {
                    e.printStackTrace();
                    LogUtils.e("注册失败" + e.toString());
                }
            }
        }).start();
    }
}
