package com.km.wskj.itmonitor.pager.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;
import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.control.PublicDataControl;
import com.zxl.zxlapplibrary.control.VersionControl;
import com.zxl.zxlapplibrary.fragment.BaseFragment;

import es.dmoral.toasty.Toasty;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */

public class LFragment extends BaseFragment {

    public PublicDataControl pdc = LApplication.app.pdc;
    public VersionControl vc = LApplication.app.vc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //沉浸式
        StatusBarUtil.setColor(getActivity(), getResources().getColor(R.color.colorWs_blue));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    //各种toast
    public void successToasty(String str, boolean isicon) {
        Toasty.success(getContext(), str, Toast.LENGTH_SHORT, isicon).show();
    }

    public void errorToasty(String str, boolean isicon) {
        Toasty.error(getContext(), str, Toast.LENGTH_SHORT, isicon).show();
    }

    public void normalToasty(String str) {
        Toasty.normal(getContext(), str, Toast.LENGTH_SHORT).show();
    }
}
