package com.km.wskj.itmonitor.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.login.LoginActivity;
import com.km.wskj.itmonitor.pager.login.RegActivity;
import com.km.wskj.itmonitor.pager.login.RegSendCodeActivity;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.ViewHolder;

/**
 * @author by Xianling.Zhou on 2017/3/15.
 */

public class DialogPlusUtils {

    public static void createDialogPlus(final Activity activity) {
        DialogPlus dialog = DialogPlus.newDialog(activity)
                .setContentHolder(new ViewHolder(R.layout.dialogplus_login_layout))
//                    .setHeader(R.layout.dialogplus_header)
                .setPadding(40, 0, 40, 0)
                .setContentWidth(ViewGroup.LayoutParams.WRAP_CONTENT)  // or any custom width ie: 300
                .setContentHeight(ViewGroup.LayoutParams.MATCH_PARENT)
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(DialogPlus dialog, View v) {
                        switch (v.getId()) {
                            //登陆
                            case R.id.dailogplus_login:
                                activity.startActivity(new Intent(activity, LoginActivity.class));
                                dialog.dismiss();
                                activity.overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
                                break;
                            //注册
                            case R.id.dailogplus_reg:
                                activity.startActivity(new Intent(activity, RegSendCodeActivity.class));
                                dialog.dismiss();
                                activity.overridePendingTransition(R.anim.anim_enter, R.anim.anim_exit);
                                break;

                            case R.id.dialogplus_close:
                                dialog.dismiss();
                                break;
                        }
                    }
                })
                .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                .setGravity(Gravity.CENTER)
                .create();
        dialog.show();
    }
}
