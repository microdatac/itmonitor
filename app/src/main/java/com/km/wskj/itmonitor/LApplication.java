package com.km.wskj.itmonitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;

import com.km.wskj.itmonitor.control.PublicDataControl;
import com.km.wskj.itmonitor.pager.im.IMHelper;
import com.tencent.bugly.Bugly;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.common.QueuedWork;
import com.zxl.zxlapplibrary.application.MyApplication;
import com.zxl.zxlapplibrary.util.FileUtil;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.util.ServerConfig;
import com.zxl.zxlapplibrary.util.glide.GlideImageLoader;
import com.zxl.zxlapplibrary.util.glide.GlidePauseOnScrollListener;

import java.io.File;

import cn.finalteam.galleryfinal.CoreConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.ThemeConfig;
import cn.jpush.android.api.JPushInterface;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */
public class LApplication extends MyApplication {
    private static final String TAG = "LApplication";
    public static LApplication app;
    public PublicDataControl pdc;

    public static Context applicationContext;

    private String baseUrl;//定义一个全局变量，用来更改主机地址
    public SharedPreferences sp;
    // login user name
    public final String PREF_USERNAME = "username";
    /**
     * nickname for current user, the nickname instead of ID be shown when user receive notification from APNs
     */
    public static String currentUserNick = "";

    @Override
    public void onCreate() {
        super.onCreate();
        setBaseUrl("http://iops.gnway.cc:8088/iOPS/api/");//默然服务器监控地址
//        setBaseUrl("http://192.168.1.129:8088/iOPS/api/");//默然服务器监控地址

        //开启debug模式，方便定位错误，具体错误检查方式可以查看http://dev.umeng.com/social/android/quick-integration的报错必看，正式发布，请关闭该模式
        Config.DEBUG = true;
        QueuedWork.isUseThreadPool = false;
        UMShareAPI.get(this);

        MultiDex.install(this);
        applicationContext = this;
        //init
        app = this;
        pdc = new PublicDataControl(getApplicationContext(), this);

//        极光初始化
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);

        //init demo helper
        IMHelper.getInstance().init(applicationContext);

        //统一初始化方法
        Bugly.init(getApplicationContext(), "97648c3c6a", true);

        ServerConfig.initServerConfig(getApplicationContext());

        //gallery
        ThemeConfig themeConfig = new ThemeConfig.Builder()
                .setTitleBarBgColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                .setFabNornalColor(Color.rgb(0x00, 0xac, 0xc1))
                .setFabPressedColor(Color.rgb(0x01, 0x83, 0x93))
                .setCheckSelectedColor(Color.rgb(0x00, 0xac, 0xc1))
                .setCropControlColor(Color.rgb(0x00, 0xac, 0xc1))
                .build();

        try {
            CoreConfig coreConfig = (new CoreConfig.Builder(getApplicationContext(), new GlideImageLoader(), themeConfig))
                    .setPauseOnScrollListener(new GlidePauseOnScrollListener(false, true))
                    .setEditPhotoCacheFolder(new File(FileUtil.getAppExtCachePath()))
                    .setTakePhotoFolder(new File(FileUtil.getAppExtFilesPath())).build();
            GalleryFinal.init(coreConfig);
        } catch (Exception e) {
            LogUtils.e(TAG, e);
        }


    }

    public static LApplication getInstance() {
        return app;
    }


    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    //各个平台的配置，建议放在全局Application或者程序入口
    {
        PlatformConfig.setWeixin("wx1b3ee2e3d1a5f812", "83a61ae36e2c5ce37151bb8c8be2dd63");
        PlatformConfig.setQQZone("100424468", "c7394704798a158208a74ab60104f0ba");

    }
}
