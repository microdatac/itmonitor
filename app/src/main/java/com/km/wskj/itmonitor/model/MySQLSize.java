package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/20.
 */
//mysql表大小
public class MySQLSize extends BaseBean {
    /*  "data_size": 1.4,
              "TABLE_NAME": "td_knowledge_info",
              "index_size": 0.01*/
    @JSONField(name = "data_size")
    public String data_size;
    @JSONField(name = "TABLE_NAME")
    public String table_name;
    @JSONField(name = "index_size")
    public String index_size;
}
