package com.km.wskj.itmonitor.pager.mine;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.UpdateService;
import com.km.wskj.itmonitor.control.PublicDataControl;
import com.km.wskj.itmonitor.pager.login.LoginActivity;
import com.zxl.zxlapplibrary.control.VersionControl;
import com.zxl.zxlapplibrary.util.FileUtil;
import com.zxl.zxlapplibrary.util.InstallApkUtil;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.util.ToolsUtil;

import okhttp3.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetFragment extends PreferenceFragment {
    //data
    PublicDataControl pdc = LApplication.app.pdc;

    Preference.OnPreferenceClickListener preferenceClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference pre) {

            if (pre.getKey().equals("set_update")) {
                checkUpdate();
            }
            if (pre.getKey().equals("set_clearcache")) {
                FileUtil.clearCache();
                pre.setSummary("缓存:0M");
                Toast.makeText(getActivity(), "清除成功", Toast.LENGTH_LONG).show();
            }

            if (pre.getKey().equals("set_account_logout")) {
                EMClient.getInstance().logout(true, new EMCallBack() {

                    @Override
                    public void onSuccess() {
                        LogUtils.e("环信退出成功！！！");
                    }

                    @Override
                    public void onProgress(int progress, String status) {
                        LogUtils.e(status + "," + progress);
                    }

                    @Override
                    public void onError(int code, String message) {
                        LogUtils.e(code + "," + message);
                    }
                });
                pdc.logout();
                getActivity().finish();
            }
            //
            if (pre.getKey().equals("set_mod_pwd")) {
                Intent t = new Intent(getActivity(), ModPwdActivity.class);
                startActivity(t);
            }


            if (pre.getKey().equals("set_autoupdate_enable")) {
                checkUpdate();
            }

            return false;
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //从xml文件加载选项
        addPreferencesFromResource(R.xml.setting);

        //修改密码
        Preference setModPwd = findPreference("set_mod_pwd");
        setModPwd.setOnPreferenceClickListener(preferenceClickListener);

        //退出登录
        Preference setLogout = findPreference("set_account_logout");
        setLogout.setOnPreferenceClickListener(preferenceClickListener);

//        //检查更新
//        Preference setUpdate = findPreference("set_update");
//        setUpdate.setOnPreferenceClickListener(preferenceClickListener);

        //自动更新
        Preference set_autoUpdate = findPreference("set_autoupdate_enable");
        set_autoUpdate.setOnPreferenceClickListener(preferenceClickListener);

        // 清除
        Preference setClear = findPreference("set_clearcache");
        setClear.setOnPreferenceClickListener(preferenceClickListener);
        setClear.setSummary("缓存:" + FileUtil.appCacheSize());

      /*  // 推送
        CheckBoxPreference pre_notify = (CheckBoxPreference) findPreference("set_notify_enable");
        pre_notify.setOnPreferenceChangeListener(preferenceChangeListener);*/
    }

    private void checkUpdate() {
        final ProgressDialog dlg = ProgressDialog.show(getActivity(), "检查更新", "请稍后...", true);
        final VersionControl vc = LApplication.app.vc;
        if (vc.isFirUpdateAvailable()) {
            vc.requestFirVersion(vc.new FirVersionCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    dlg.dismiss();
                    LogUtils.e("request xtkj update server fail", e);
                }

                @Override
                public void onResponse(Object response, int id) {
                    dlg.dismiss();
                    if (vc.checkVersion() > 0) {
                        installUpdate();
                    }
                }
            });
        }
    }

    private void installUpdate() {
        final VersionControl vc = LApplication.app.vc;
        InstallApkUtil i = new InstallApkUtil(getActivity(), getActivity().getPackageName(),
                "app.apk", vc.apkUrl, vc.must == 1);//
        if (vc.must == 1) {
            i.installFromService("版本更新", "检测到新版本:" + vc.versionName + "\n" + vc.updateInfo
                    + "\n需要立即更新", UpdateService.class);
        } else {
            i.installFromService("版本更新", "检测到新版本:" + vc.versionName + "\n" + vc.updateInfo
                    + "\n是否立即更新", UpdateService.class);
        }
    }
}
