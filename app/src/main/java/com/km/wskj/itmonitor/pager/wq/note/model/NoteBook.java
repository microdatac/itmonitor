package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;


public class NoteBook extends BaseBean {
    /**
     * createTime : 2017-03-27 20:45:30
     * typeName : 手机上试试
     * id : 58d13be6e66842848748d34480167b6c
     * userId : e73139460d654221b9d1d607cc9530f9
     * isDel : 0
     */

    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "typeName")
    public String typeName;
    @JSONField(name = "id")
    public String idX;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "isDel")
    public int isDel;


    public boolean ischecked;

    public String toString() {
        return typeName;
    }


}
