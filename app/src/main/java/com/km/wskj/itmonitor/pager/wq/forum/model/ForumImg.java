package com.km.wskj.itmonitor.pager.wq.forum.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

public class ForumImg extends BaseBean {


    /**
     * file_path : /upload/2017/03/27/
     * new_file_name : 20170327102401599acc9.jpg
     * create_time : 2017-03-27 10:01:59
     * createTime : 2017-03-27 10:01:59
     * attach_id : 5b1bc134babe4f0faafd7930248c1516
     * host : http://iops.gnway.cc:8088/iOPS
     * original_file_name : 1.jpg
     * attachId : 5b1bc134babe4f0faafd7930248c1516
     * type : 1
     * suffix : .jpg
     * forumId : 24c8fed574e441a6845fe4bde89321e0
     */

    @JSONField(name = "file_path")
    public String filePath;
    @JSONField(name = "new_file_name")
    public String newFileName;
    @JSONField(name = "create_time")
    public String createTime;
    @JSONField(name = "attach_id")
    public String attachId;
    @JSONField(name = "host")
    public String host;
    @JSONField(name = "original_file_name")
    public String originalFileName;
    @JSONField(name = "type")
    public int type;
    @JSONField(name = "suffix")
    public String suffix;
    @JSONField(name = "forumId")
    public String forumId;
}
