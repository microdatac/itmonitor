package com.km.wskj.itmonitor.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.km.wskj.itmonitor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/12/21.
 */

public class FlowView extends View {
    /**
     * 画笔对象的引用
     */
    private Paint paint;

    /**
     * 圆环的颜色
     */
    private String  LineColor="#6A3916";

    public String getLineColor() {
        return LineColor;
    }

    public void setLineColor(String lineColor) {
        LineColor = lineColor;
    }

    private List<Double>progress = new ArrayList<>();

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    public FlowView(Context context) {
        this(context, null);
    }

    public FlowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FlowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        paint = new Paint();

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        paint.setAntiAlias(true);  //消除锯齿

        Path path5=new Path();
//        path5.moveTo(170, 410);
//        path5.lineTo(230,410);
//        path5.lineTo(215,350);
//        path5.lineTo(185, 350);
//        path5.close();
        paint.setColor(Color.parseColor("#6385B1"));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        for(int s = 1;s<7;s++){
            for(int i = 0;i<31;i=i+2){
                path5.moveTo(getWidth()/31*(i), getHeight()/6*s);
                path5.lineTo(getWidth()/31*(1+i),getHeight()/6*s);
            }
        }

//        for(int i = 0;i<31;i=i+2){
//            path5.moveTo(getWidth()/31*(i), getHeight()/5*4);
//            path5.lineTo(getWidth()/31*(1+i),getHeight()/5*4);
//        }
//        for(int i = 0;i<31;i=i+2){
//            path5.moveTo(getWidth()/31*(i), getHeight()/5*3);
//            path5.lineTo(getWidth()/31*(1+i),getHeight()/5*3);
//        }
//        for(int i = 0;i<31;i=i+2){
//            path5.moveTo(getWidth()/31*(i), getHeight()/5*2);
//            path5.lineTo(getWidth()/31*(1+i),getHeight()/5*2);
//        }
//        for(int i = 0;i<31;i=i+2){
//            path5.moveTo(getWidth()/31*(i), getHeight()/5*1);
//            path5.lineTo(getWidth()/31*(1+i),getHeight()/5*1);
//        }
//        path5.moveTo(0, getHeight());
//        path5.lineTo(getWidth()/31*1,getHeight());
//        path5.moveTo(getWidth()/31*2, getHeight());
//        path5.lineTo(getWidth()/31*3,getHeight());
//        path5.moveTo(getWidth()/31*4, getHeight());
//        path5.lineTo(getWidth()/31*5,getHeight());
//        path5.moveTo(getWidth()/31*6, getHeight());
//        path5.lineTo(getWidth()/31*7,getHeight());
//        path5.moveTo(getWidth()/31*8, getHeight());
//        path5.lineTo(getWidth()/31*9,getHeight());
//        path5.moveTo(getWidth()/31*10, getHeight());
//        path5.lineTo(getWidth()/31*11,getHeight());
//        path5.moveTo(getWidth()/31*12, getHeight());
//        path5.lineTo(getWidth()/31*13,getHeight());
//        path5.moveTo(getWidth()/31*14, getHeight());
//        path5.lineTo(getWidth()/31*15,getHeight());
//        path5.moveTo(getWidth()/31*16, getHeight());
//        path5.lineTo(getWidth()/31*17,getHeight());
//        path5.moveTo(getWidth()/31*18, getHeight());
//        path5.lineTo(getWidth()/31*19,getHeight());
//        path5.moveTo(getWidth()/31*20, getHeight());
//        path5.lineTo(getWidth()/31*21,getHeight());
//        path5.moveTo(getWidth()/31*22, getHeight());
//        path5.lineTo(getWidth()/31*23,getHeight());
//        path5.moveTo(getWidth()/31*24, getHeight());
//        path5.lineTo(getWidth()/31*25,getHeight());
//        path5.moveTo(getWidth()/31*26, getHeight());
//        path5.lineTo(getWidth()/31*27,getHeight());
//        path5.moveTo(getWidth()/31*28, getHeight());
//        path5.lineTo(getWidth()/31*29,getHeight());
//        path5.moveTo(getWidth()/31*30, getHeight());
//        path5.lineTo(getWidth()/31*31,getHeight());
        path5.close();
        paint.setStrokeWidth(3);
        canvas.drawPath(path5, paint);
        if(progress.size() != 0){
            paint.setColor(Color.parseColor(LineColor));
            paint.setStyle(Paint.Style.FILL);
            for (int i = 0;i<progress.size();i++){
                canvas.drawCircle(getWidth() / 48 * (i ),(float) (getHeight()/6*5*(1-progress.get(i))+getHeight()/6),6, paint);// 小圆
                if(i<progress.size()-1) {
                    canvas.drawLine(6, (float) (getHeight() / 6 * 5 * (1 - progress.get(i)) + getHeight() / 6), getWidth() / 48 * (i + 1), (float) (getHeight() / 6 * 5 * (1 - progress.get(i)) + getHeight() / 6), paint);
                }else if (i ==progress.size()-1){
                    canvas.drawCircle(getWidth() / 48 * (i+1 ),(float) (getHeight()/6*5*(1-progress.get(i))+getHeight()/6),6, paint);// 小圆
                }
            }
//            canvas.drawCircle(8, getHeight()/5*4, 8, paint);// 小圆
//            canvas.drawLine(8,getHeight()/5*4,getWidth()/20*1,getHeight()/6, paint);
//
//            canvas.drawCircle(getWidth()/20*1,getHeight()/6, 8, paint);// 小圆
//            canvas.drawLine(getWidth()/20*1,getHeight()/6,getWidth()/20*3,getHeight()/6, paint);
//
//            canvas.drawCircle(getWidth()/20*3,getHeight()/6, 8, paint);// 小圆
//            canvas.drawLine(getWidth()/20*3,getHeight()/6,getWidth()/20*5,getHeight()/6, paint);
//
//            canvas.drawCircle(getWidth()/20*5,getHeight()/6, 8, paint);// 小圆
//            canvas.drawLine(getWidth()/20*5,getHeight()/6,getWidth()/20*8,getHeight()/6, paint);
//
//            canvas.drawCircle(getWidth()/20*8,getHeight()/6, 8, paint);// 小圆
//            canvas.drawLine(getWidth()/20*8,getHeight()/6,getWidth()/20*10,getHeight()/6, paint);
//
//            canvas.drawCircle(getWidth()/20*10,getHeight()/6, 8, paint);// 小圆
//            canvas.drawLine(getWidth()/20*10,getHeight()/6,getWidth()/20*11,getHeight()/6, paint);
//
//            canvas.drawCircle(getWidth()/20*11,getHeight()/6, 8, paint);// 小圆
//            canvas.drawLine(getWidth()/20*11,getHeight()/6,getWidth()/20*13,getHeight()/6, paint);
        }




    }




    /**
     * 获取进度.需要同步
     * @return
     */
    public synchronized List<Double> getProgress() {
        return progress;
    }

    /**
     * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步
     * 刷新界面调用postInvalidate()能在非UI线程刷新
     * @param progress
     */
    public synchronized void setProgress(double progress) {
        this.progress.add(progress);
        postInvalidate();


    }

}
