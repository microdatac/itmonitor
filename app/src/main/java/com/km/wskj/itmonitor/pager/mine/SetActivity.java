package com.km.wskj.itmonitor.pager.mine;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SetActivity extends LActivity {

    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set);
        ButterKnife.bind(this);

        mBack.setVisibility(View.VISIBLE);
        //加载PrefFragment
        FragmentTransaction t = getFragmentManager().beginTransaction();
        SetFragment prefFragment = new SetFragment();
        t.add(R.id.layout_container, prefFragment);
        t.commit();
    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }


}
