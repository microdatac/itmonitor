package com.km.wskj.itmonitor.pager.monitor.alarm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.AlarmDetail;
import com.km.wskj.itmonitor.model.FuncItem;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.MyGridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/1.
 */
//报警详情页面
public class AlarmDetailActivity extends LActivity {
    private static final String TAG = "AlarmDetailActivity";

    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.et_alarm_detail_message)
    TextView mMessage;
    @BindView(R.id.et_alarm_detail_handlemessage)
    EditText mHandlerMessage;
    @BindView(R.id.btn_alarm_detail_submit)
    FancyButton mSubmit;
    @BindView(R.id.alarm_container_gridview)
    LinearLayout mContainer;
    @BindView(R.id.tv_alarm_detail_time)
    TextView mTime;
    //data
    private String id;
    //data
    List<FuncItem> funcItems = new ArrayList<>();
    private MyGridView gridView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_detail);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("id"))
            id = getIntent().getStringExtra("id");


        mContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                createGridView();
            }
        });
        initView();
        initFuncItems();
    }

    private void initView() {
        mTitle.setText("告警处理");
        mBack.setVisibility(View.VISIBLE);

        pdc.alarmDetail(HTTP_TASK_TAG, id, new ZxlGenericsCallback<AlarmDetail>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(AlarmDetail alarmDetail, int i) {
                if (alarmDetail.createTime != null)
                    mTime.setText(alarmDetail.createTime);
                if (alarmDetail.message != null)
                    mMessage.setText(alarmDetail.message);

                switch (alarmDetail.status) {
                    case 0:
                        mSubmit.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        mSubmit.setVisibility(View.GONE);
                        if (alarmDetail.handleMessage != null)
                            mHandlerMessage.setText(alarmDetail.handleMessage);

                        break;
                }
            }
        });


    }


    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }

    /**
     * 提交点击事件
     */
    @OnClick(R.id.btn_alarm_detail_submit)
    public void submit() {
        String str = mHandlerMessage.getText().toString();
        if (str.equals("")) {
            Toast.makeText(context, "请输入报警处理信息!", Toast.LENGTH_SHORT).show();
            return;
        }

        pdc.handleAlarm(HTTP_TASK_TAG, id, str, new ZxlGenericsCallback() {
            @Override
            public void onError(Call call, Exception e, int i) {
                showErrorTip("提交失败");
                LogUtils.e("error", e);
            }

            @Override
            public void onResponse(Object o, int i) {
                showSuccTip("提交成功");
                mSubmit.setVisibility(View.GONE);
            }
        });
    }

    private void initFuncItems() {
        String s[] = getResources().getStringArray(R.array.alarm_func_title_array);
        String index[] = getResources().getStringArray(R.array.alarm_func_index);

        int r[] = {R.mipmap.icon_baojing1
                , R.mipmap.icon_baojing2
                , R.mipmap.icon_baojing3
                , R.mipmap.icon_baojing4
                , R.mipmap.icon_baojing5
                , R.mipmap.icon_baojing6};

        funcItems.clear();
        for (int i = 0; i < s.length && i < r.length; i++) {
            FuncItem fi = new FuncItem();
            fi.name = s[i];
            fi.resId = r[i];
            fi.index = index[i];
            funcItems.add(fi);
        }
    }


    private void createGridView() {

        gridView = new MyGridView(context, new MyGridView.MyGridViewAdapter() {
            @Override
            public int rowspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int leftpadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int toppadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colCount(MyGridView myGridView) {
                return 3;
            }

            @Override
            public int rowHeight(MyGridView myGridView) {
                return mContainer.getHeight() / 2;
            }

            @Override
            public int viewWidth(MyGridView myGridView) {
                return mContainer.getWidth();
            }

            @Override
            public int count(MyGridView myGridView) {
                return funcItems.size();
            }

            @Override
            public View cell(int i, MyGridView myGridView) {
                View view = getLayoutInflater().inflate(R.layout.alarm_func_item, null);
                ImageView iv_pic = (ImageView) view.findViewById(R.id.iv_pic);
                TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
                LinearLayout bg = (LinearLayout) view.findViewById(R.id.ll_minitor_bg);
                FuncItem fi = funcItems.get(i);
                iv_pic.setImageResource(fi.resId);
                tv_name.setText(fi.name);
                return view;
            }

            @Override
            public View title(int i, MyGridView myGridView) {
                return null;
            }

            @Override
            public int titleOffsetY(MyGridView myGridView) {
                return 0;
            }

            @Override
            public boolean needGridLine(MyGridView myGridView) {
                return true;
            }
        });


        gridView.setListener(new MyGridView.MyGridViewListener() {
            @Override
            public void onCellTouchDown(int i) {

            }

            @Override
            public void onCellTouchUp(int i) {

            }

            @Override
            public void onCellLongPress(int i) {

            }

            @Override
            public void onCellClick(int i) {
                switch (i) {
                    //自动修复
                    case 0: {
                        normalToasty("暂未开放功能");
                    }
                    break;
                    //问题跟踪
                    case 1: {
                        normalToasty("暂未开放功能");
                    }
                    break;
                    //知识搜索
                    case 2: {
                        normalToasty("暂未开放功能");
                    }
                    break;
                    //远程支持
                    case 3: {
                        normalToasty("暂未开放功能");
                    }
                    break;
                    //保修
                    case 4: {
                        normalToasty("暂未开放功能");
                    }
                    break;
                    //关闭报警
                    case 5: {
                        normalToasty("暂未开放功能");
                    }
                }
            }
        });
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mContainer.addView(gridView.getView(), p);
    }
}
