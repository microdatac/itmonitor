package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/14.
 */

public class DBChartSize extends BaseBean {
  /*  tableNames true array[string] 数据库名称 多个
    dataSize true array[number] 数据文件大小 多个
    indexSize true array[number] 索引文件大小 多个*/

    @JSONField(name = "tableNames")
    public Object tableNames;

    @JSONField(name = "dataSize")
    public Object dataSize;

    @JSONField(name = "indexSize")
    public Object indexSize;
}
