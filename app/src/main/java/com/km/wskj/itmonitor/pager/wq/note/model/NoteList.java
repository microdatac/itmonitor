package com.km.wskj.itmonitor.pager.wq.note.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/28.
 */

public class NoteList extends BaseBean {

    @JSONField(name = "path")
    public String path;
    @JSONField(name = "columnNames")
    public Object columnNames;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "imgs")
    public List<NoteImgs> imgs;

    public boolean ischecked;

}
