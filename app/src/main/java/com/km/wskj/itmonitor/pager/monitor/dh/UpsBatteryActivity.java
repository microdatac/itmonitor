package com.km.wskj.itmonitor.pager.monitor.dh;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author by Xianling.Zhou on 2017/4/24.
 */
//ups电池组
public class UpsBatteryActivity extends LActivity {

    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ups_battery);
        ButterKnife.bind(this);

        toolBarTitle.setText("UPS电池组");
        toolBarBack.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }
}
