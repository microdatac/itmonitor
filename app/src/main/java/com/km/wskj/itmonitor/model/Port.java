package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/8.
 */

public class Port extends BaseBean {
    /* "portPhyAddr": " a4:4c:11:58:6c:c0",
             "portType": " propVirtual(53)",
             "portDesc": " Vlan1",
             "portStatus": " down",
             "portIndex": " 1",
             "portAlias": " ",
             "portRate": " 1000000000",
             "portPhyConn": " false",
             "portMTU": " 1500"*/

    @JSONField(name = "portPhyAddr")
    public String portPhyAddr;

    @JSONField(name = "portType")
    public String portType;

    @JSONField(name = "portDesc")
    public String portDesc;

    @JSONField(name = "portStatus")
    public String portStatus;

    @JSONField(name = "portIndex")
    public String portIndex;

    @JSONField(name = "portAlias")
    public String portAlias;

    @JSONField(name = "portRate")
    public String portRate;

    @JSONField(name = "portPhyConn")
    public String portPhyConn;

    @JSONField(name = "portMTU")
    public String portMTU;

}
