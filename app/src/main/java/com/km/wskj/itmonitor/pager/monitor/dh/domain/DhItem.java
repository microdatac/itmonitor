package com.km.wskj.itmonitor.pager.monitor.dh.domain;

/**
 * @author by Xianling.Zhou on 2017/4/21.
 */
//模拟数据
public class DhItem {

    /**
     * name : 配电柜
     */
    public String name;
    public String type;
    public int url;
}
