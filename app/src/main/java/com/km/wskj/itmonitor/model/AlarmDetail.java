package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/4/6.
 */

public class AlarmDetail extends BaseBean {


    /**
     * level3Name : CPU
     * deviceIp : 192.168.1.107
     * handleTime : 2017-03-15 23:47:39
     * level1Name : 服务器
     * handleMessage : 确认
     * message : CPU超过阈值：当前CPU使用率：82.14%,阈值：80%
     * deviceId : d53b92bf5d0f42bb883d307eb3ddd018
     * deviceName : 设施智能监控Linux
     * times : 1
     * alarmType : 1
     * createTime : 2017-03-14 11:40:07
     * level2Name : Linux
     * id : 1df66b9626fd4043808edf270e9e2171
     * level1 : 1
     * level3 : 10
     * level2 : 3
     * status : 1
     */

    @JSONField(name = "level3Name")
    public String level3Name;
    @JSONField(name = "deviceIp")
    public String deviceIp;
    @JSONField(name = "handleTime")
    public String handleTime;
    @JSONField(name = "level1Name")
    public String level1Name;
    @JSONField(name = "handleMessage")
    public String handleMessage;
    @JSONField(name = "message")
    public String message;
    @JSONField(name = "deviceId")
    public String deviceId;
    @JSONField(name = "deviceName")
    public String deviceName;
    @JSONField(name = "times")
    public int times;
    @JSONField(name = "alarmType")
    public int alarmType;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "level2Name")
    public String level2Name;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "level1")
    public String level1;
    @JSONField(name = "level3")
    public String level3;
    @JSONField(name = "level2")
    public String level2;
    @JSONField(name = "status")
    public int status;
}
