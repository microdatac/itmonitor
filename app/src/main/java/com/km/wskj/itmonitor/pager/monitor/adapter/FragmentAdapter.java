package com.km.wskj.itmonitor.pager.monitor.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.km.wskj.itmonitor.pager.base.LFragment;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/6.
 *         功能：主页引导栏的Fragment页面设置适配器
 */

public class FragmentAdapter extends FragmentPagerAdapter {
    private List<LFragment> fragments;


    public FragmentAdapter(FragmentManager fm, List<LFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
