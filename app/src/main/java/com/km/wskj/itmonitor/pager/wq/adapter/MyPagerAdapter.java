package com.km.wskj.itmonitor.pager.wq.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.km.wskj.itmonitor.pager.base.LFragment;

import java.util.ArrayList;

/**
 * @author by Xianling.Zhou on 2017/3/15.
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<LFragment> mFragments;
    ArrayList<String> mTitles;

    public MyPagerAdapter(FragmentManager fm, ArrayList<LFragment> lFragments, ArrayList<String> strings) {
        super(fm);
        this.mFragments = lFragments;
        this.mTitles = strings;
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.size() == 0 ? "" : mTitles.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }
}
