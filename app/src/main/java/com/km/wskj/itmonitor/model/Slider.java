package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/4/7.
 */
//轮播图
public class Slider extends BaseBean {

    @JSONField(name = "isRecommend")
    public int isRecommend;
    @JSONField(name = "file_path")
    public String filePath;
    @JSONField(name = "columnId")
    public String columnId;
    @JSONField(name = "author")
    public String author;
    @JSONField(name = "source")
    public String source;
    @JSONField(name = "title")
    public String title;
    @JSONField(name = "type")
    public int type;
    @JSONField(name = "suffix")
    public String suffix;
    @JSONField(name = "path")
    public String path;
    @JSONField(name = "new_file_name")
    public String newFileName;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "host")
    public String host;
    @JSONField(name = "original_file_name")
    public String originalFileName;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "attachId")
    public String attachId;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "desc")
    public String desc;
    @JSONField(name = "content")
    public String content;
}
