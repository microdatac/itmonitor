package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/9.
 */

public class App extends BaseBean {

    /**
     * isAlive : true
     * isMonitor : 1
     * rat : 100
     * appSortName : 项目
     * createTime : 2017-04-09 22:13:15
     * appName : 项目管理系统
     * monitorCycle : 60
     * appUrl : http://iops.gnway.cc:8888/zentao/user-login.html
     * id : 5102bf1120174589ad2c39e7fc5f972b
     * aliveThreshold : 90
     * isDel : 0
     * middleware : null
     */

    @JSONField(name = "isAlive")
    public boolean isAlive;
    @JSONField(name = "isMonitor")
    public int isMonitor;
    @JSONField(name = "rat")
    public int rat;
    @JSONField(name = "appSortName")
    public String appSortName;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "appName")
    public String appName;
    @JSONField(name = "monitorCycle")
    public int monitorCycle;
    @JSONField(name = "appUrl")
    public String appUrl;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "aliveThreshold")
    public int aliveThreshold;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "middleware")
    public String middleware;
}
