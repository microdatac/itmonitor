package com.km.wskj.itmonitor.pager.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.NewsColumns;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.base.LFragment;
import com.km.wskj.itmonitor.pager.wq.adapter.MyPagerAdapter;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/9.
 */
//已废弃
public class NewsListActivity extends LActivity {

    private static final String TAG = "NewsListActivity";

    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.tool_bar_right)
    ImageView mSearch;
    @BindView(R.id.vp_news)
    ViewPager mViewPager;
    @BindView(R.id.slidingtab_news)
    SlidingTabLayout mSlidingTab;

    //data
    private ArrayList<LFragment> mFragments;
    private ArrayList<String> mTitles;
    private MyPagerAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        //沉浸式
        StatusBarCompat.compat(context, getResources().getColor(R.color.colorWs_blue));
        initView();
        initData();
    }

    private void initView() {
        mTitle.setText("新闻公告");
        mBack.setVisibility(View.VISIBLE);
    }

    private void initData() {
        mFragments = new ArrayList<>();
        mTitles = new ArrayList<>();

        pdc.newsColumn(HTTP_TASK_TAG, new ZxlGenericsCallback<List<NewsColumns>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<NewsColumns> newsColumnses, int i) {

                for (int j = 0; j < newsColumnses.size(); j++) {
                    String columnName = newsColumnses.get(j).columnName;
                    mTitles.add(columnName);
                    mFragments.add(NewsListFragment.getInstance(newsColumnses.get(j)));
                }

                mAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
                mViewPager.setAdapter(mAdapter);
                mSlidingTab.setViewPager(mViewPager);
            }
        });
    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }
}
