package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/8.
 */

public class Internet extends BaseBean {
    /*id true string ID
    ip true string ip地址
    collectorIp true string 采集器IP
    hostname true string 设备名称
    Model true string 类型
    SN true string 序列号
    PortNumber true number 端口数量
    SoftVersion true string 软件版本
    Runtime true string 运行时间
    ppName true string 品牌名称
    hostPp true string 品牌ID
    hostLb true string 类别ID
    lbName true string 类别名称
    createTime true string 创建时间
    hostLx true
    xybz true
    status true number 设备状态 1 正常 2 告警 3 故障*/

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "ip")
    public String ip;
    @JSONField(name = "collectorIp")
    public String collectorIp;
    @JSONField(name = "hostname")
    public String hostname;
    @JSONField(name = "Model")
    public String Model;
    @JSONField(name = "SN")
    public String SN;
    @JSONField(name = "PortNumber")
    public int PortNumber;
    @JSONField(name = "SoftVersion")
    public String SoftVersion;
    @JSONField(name = "Runtime")
    public String runtime;
    @JSONField(name = "ppName")
    public String ppName;
    @JSONField(name = "hostPp")
    public String hostPp;
    @JSONField(name = "hostLb")
    public String hostLb;
    @JSONField(name = "lbName")
    public String lbName;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "hostLx")
    public String hostLx;
    @JSONField(name = "status")
    public int status;

}
