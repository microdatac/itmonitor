package com.km.wskj.itmonitor.pager.monitor.internet;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.FuncItem;
import com.km.wskj.itmonitor.model.Port;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.monitor.alarm.AlarmListActivity;
import com.km.wskj.itmonitor.pager.monitor.application.AppListActivity;
import com.km.wskj.itmonitor.pager.monitor.database.DBListActivity;
import com.km.wskj.itmonitor.pager.monitor.server.ServerListActivity;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.km.wskj.itmonitor.view.RoundProgressBar;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.view.MyGridView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/3/7.
 */

public class InternetDetailActivity extends LActivity {
    private static final String TAG = "InternetDetailActivity";

    //data
    private String id;
    private String ip;
    private MyGridView gridView;
    private List<Port> mPorts;

    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.internet_container_gridview)
    LinearLayout mContainer;
    @BindView(R.id.internet_memory)
    RoundProgressBar mProMem;
    @BindView(R.id.internet_cpu)
    RoundProgressBar mProCpu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet_detail_activity);
        ButterKnife.bind(this);
        //沉浸式
        StatusBarCompat.compat(context, getResources().getColor(R.color.colorWs_blue));

        id = getIntent().getStringExtra("id");
        ip = getIntent().getStringExtra("ip");

        mTitle.setText(ip);
        mBack.setVisibility(View.VISIBLE);
        initView();

        mProMem.setProgress(23f);
        mProCpu.setProgress(33f);


        mContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void initView() {
        pdc.portList(HTTP_TASK_TAG, id, new ZxlGenericsCallback<List<Port>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                showErrorTip("加载失败");
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(List<Port> ports, int i) {
                mPorts = ports;
                createGridView(mPorts);
            }
        });
    }


    private void createGridView(final List<Port> ports) {


        gridView = new MyGridView(context, new MyGridView.MyGridViewAdapter() {
            @Override
            public int rowspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colspacing(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int leftpadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int toppadding(MyGridView myGridView) {
                return 0;
            }

            @Override
            public int colCount(MyGridView myGridView) {
                return 8;
            }

            @Override
            public int rowHeight(MyGridView myGridView) {
                return mContainer.getHeight() / 3;
            }

            @Override
            public int viewWidth(MyGridView myGridView) {
                return mContainer.getWidth();
            }

            @Override
            public int count(MyGridView myGridView) {
                return ports.size();
            }

            @Override
            public View cell(int i, MyGridView myGridView) {
                View view = getLayoutInflater().inflate(R.layout.port_item_func, null);
                TextView title = (TextView) view.findViewById(R.id.tv_port_name);
                ImageView bg = (ImageView) view.findViewById(R.id.iv_port_state);
                Port port = ports.get(i);
                String portStatus = port.portStatus.trim();
                if (portStatus.equals("up")) {
                    bg.setBackground(getResources().getDrawable(R.drawable.port_shape_corner_green));
                } else {
                    bg.setBackground(getResources().getDrawable(R.drawable.port_shape_corner_gray));
                    bg.setImageResource(R.mipmap.cha);
                }
                title.setText((i + 1) + "");
                return view;
            }

            @Override
            public View title(int i, MyGridView myGridView) {
                return null;
            }

            @Override
            public int titleOffsetY(MyGridView myGridView) {
                return 0;
            }

            @Override
            public boolean needGridLine(MyGridView myGridView) {
                return false;
            }
        });


        gridView.setListener(new MyGridView.MyGridViewListener() {
            @Override
            public void onCellTouchDown(int i) {

            }

            @Override
            public void onCellTouchUp(int i) {

            }

            @Override
            public void onCellLongPress(int i) {

            }

            @Override
            public void onCellClick(int i) {

            }
        });
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mContainer.addView(gridView.getView(), p);
    }


    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }
}
