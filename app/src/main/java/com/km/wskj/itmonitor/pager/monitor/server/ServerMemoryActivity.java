package com.km.wskj.itmonitor.pager.monitor.server;


import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingdo.statelayout.StateLayout;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.Memory;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.utils.FloatUtils;
import com.zxl.zxlapplibrary.util.LogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * @author by Xianling.Zhou on 2017/2/9.
 */
//内存页面
public class ServerMemoryActivity extends LActivity implements StateLayout.OnViewRefreshListener {
    private static final String TAG = "ServerMemoryActivity";

    //ui
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.donutProgress)
    DonutProgress donutProgress;
    @BindView(R.id.tv_total_phy)
    TextView tvTotalPhy;
    @BindView(R.id.tv_free_phy)
    TextView tvFreePhy;
    @BindView(R.id.tv_used_phy)
    TextView tvUsedPhy;
    @BindView(R.id.tv_total_vir)
    TextView tvTotalVir;
    @BindView(R.id.tv_free_vir)
    TextView tvFreeVir;
    @BindView(R.id.tv_used_vir)
    TextView tvUsedVir;
    @BindView(R.id.state_layout)
    StateLayout stateLayout;

    //data
    private String ip;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            // 在此处添加执行的代码
            initData();
            handler.postDelayed(this, 2000);// 50ms后执行this，即runable
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_memory);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("ip"))
            ip = getIntent().getStringExtra("ip");

        toolBarTitle.setText(ip);
        toolBarBack.setVisibility(View.VISIBLE);
//        handler.postDelayed(runnable, 2000);// 打开定时器，50ms后执行runnable操作
        stateLayout.setRefreshListener(this);
        initData();
    }

    private void initData() {
        stateLayout.showLoadingView();
        pdc.memory(HTTP_TASK_TAG, ip, new ZxlGenericsCallback<Memory>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                stateLayout.showErrorView();
            }

            @Override
            public void onResponse(Memory sm, int i) {
                //set data
                float v = FloatUtils.doubleFloat(Float.parseFloat(sm.usedPhysicsMem)
                        / Float.parseFloat(sm.physicsMem)) * 100;
                donutProgress.setProgress(v);
                donutProgress.setText(v + "%");
                donutProgress.setInnerBottomText("已用物理内存");

                tvTotalPhy.setText(sm.physicsMem + "GB");
                tvUsedPhy.setText(sm.usedPhysicsMem + "GB");
                tvFreePhy.setText(sm.freePhysicsMem + "GB");
                tvTotalVir.setText(sm.virtualMem + "GB");
                tvFreeVir.setText(sm.freeVirtualMem + "GB");
                tvUsedVir.setText(sm.usedVirtualMem + "GB");

                stateLayout.showContentView();
                stateLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }

    @Override
    public void refreshClick() {
        initData();
    }

    @Override
    public void loginClick() {

    }
}
