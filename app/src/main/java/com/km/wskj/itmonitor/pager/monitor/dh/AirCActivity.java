package com.km.wskj.itmonitor.pager.monitor.dh;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fingdo.statelayout.StateLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author by Xianling.Zhou on 2017/4/24.
 */
//精密空调
public class AirCActivity extends LActivity implements StateLayout.OnViewRefreshListener {

    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.state_layout)
    StateLayout stateLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_c);
        ButterKnife.bind(this);

        toolBarTitle.setText("精密空调");
        toolBarBack.setVisibility(View.VISIBLE);

        stateLayout.showLoadingView();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stateLayout.showContentView();
            }
        }, 1000);
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }


    //statelayout
    @Override
    public void refreshClick() {

    }

    @Override
    public void loginClick() {

    }
}
