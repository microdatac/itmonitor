package com.km.wskj.itmonitor.pager.wq.forum.model;

import com.km.wskj.itmonitor.model.BaseBean;

/**
 * @author by Xianling.Zhou on 2017/4/1.
 */

public class CommentParams extends BaseBean {

    /* id false string 评论ID 编辑的时候用
     userId false string 评论人ID
     forumId false string 帖子ID
     content false string 评论内容
     imgIds false string 图片ID列表，逗号分隔*/

    public String id;
    public String userId;
    public String forumId;
    public String content;
    public String imgIds;
}
