package com.km.wskj.itmonitor.utils;

/**
 * @author by Xianling.Zhou on 2017/2/15.
 */
//保留小数点
public class FloatUtils {
    //保留两位小数
    public static float doubleFloat(float f) {
        float v = (float) (Math.round(f * 100)) / 100;//(这里的100就是2位小数点,如果要其它位,如4位,这里两个100改成10000)
        return v;
    }
}
