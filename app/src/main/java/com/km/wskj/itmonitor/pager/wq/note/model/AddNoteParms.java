package com.km.wskj.itmonitor.pager.wq.note.model;

/**
 * @author by Xianling.Zhou on 2017/3/29.
 */
//添加笔记参数
public class AddNoteParms {
    public String id;
    public String userId;
    public String columnId;
    public String title;
    public String content;
    public String fileIds;
    public String imgIds;
}
