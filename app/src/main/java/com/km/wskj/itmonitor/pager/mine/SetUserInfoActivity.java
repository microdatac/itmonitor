package com.km.wskj.itmonitor.pager.mine;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.github.johnpersano.supertoasts.SuperActivityToast;
import com.github.johnpersano.supertoasts.SuperToast;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.zhy.m.permission.MPermissions;
import com.zhy.m.permission.PermissionDenied;
import com.zhy.m.permission.PermissionGrant;
import com.zxl.zxlapplibrary.http.callback.StringCallback;
import com.zxl.zxlapplibrary.model.ImageData;
import com.zxl.zxlapplibrary.util.GalleryHelper;
import com.zxl.zxlapplibrary.util.LogUtils;
import com.zxl.zxlapplibrary.util.ToolsUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.Call;

public class SetUserInfoActivity extends LActivity {
    private static final String TAG = "SetUserInfoActivity";

    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;
    @BindView(R.id.iv_head)
    ImageView iv_head;
    @BindView(R.id.et_nickname)
    EditText nickName;
    @BindView(R.id.tool_bar_right_text)
    TextView toolBarRightText;

    private ImageData mImageData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_user_info);
        ButterKnife.bind(this);

        mBack.setVisibility(View.VISIBLE);
        //ui
        mTitle.setText("修改个人资料");
        toolBarRightText.setVisibility(View.VISIBLE);
        toolBarRightText.setText("完成");

        Glide.with(this).load(pdc.mCurrentUser.host + pdc.mCurrentUser.headImg).into(iv_head);
        nickName.setText(pdc.mCurrentUser.nickname);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshUI();
    }


    @OnClick(R.id.layout_head)
    public void onLayoutHead(View view) {
        MPermissions.requestPermissions(this, 502, Manifest.permission.CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @PermissionGrant(502)
    public void takeHeadPic() {
        new GalleryHelper().openSingleCrop(this, 200, 200, new GalleryHelper.OnPickPhotoCallback() {
            @Override
            public void onPickSucc(PhotoInfo photoInfo) {
                mImageData = new ImageData(photoInfo.getPhotoPath());
                mImageData.displayImage(context, iv_head);
                LogUtils.d("take head pic path:" + mImageData.fileOrgPath);
                //
            }

            @Override
            public void onPickFail(String s) {
                showErrorTip("获取图片失败:" + s);
            }
        });
    }

    @PermissionDenied(502)
    public void takeHeadPicFail() {
        ToolsUtil.msgbox(context, "没有拍照权限,请设置");
    }

    private void refreshUI() {

    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }

    @OnClick(R.id.tool_bar_right_text)
    public void onClick() {
        final SuperActivityToast superActivityToast = new SuperActivityToast(context, SuperToast.Type.PROGRESS);
        superActivityToast.setIndeterminate(true);
        superActivityToast.setText("请稍后...");
        superActivityToast.setProgressIndeterminate(true);
        superActivityToast.show();

        final String string = nickName.getText().toString();
        pdc.uploadPic(HTTP_TASK_TAG, mImageData, new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
            }

            @Override
            public void onResponse(String s, int i) {

                JSONObject json = JSON.parseObject(s);
                String attach_id = json.getString("attach_id");

                pdc.updateInfo(HTTP_TASK_TAG, pdc.mCurrentUser.userId, string, null, attach_id, pdc.new LoginCallback() {
                    @Override
                    public void onError(Call call, Exception e, int i) {
                        LogUtils.e(TAG, e);
                    }

                    @Override
                    public void onResponse(Object o, int i) {
                        normalToasty("更新成功");
                        superActivityToast.dismiss();
                        finish();
                    }
                });
            }
        });
    }
}
