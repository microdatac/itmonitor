package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by wuhan on 2017/3/15.
 */

public class AddNote extends BaseBean {
   /*  "msg": "保存成功！",
             "id": "3b960fd00333429db8c633dccd6bd6f4",
             "status": "y",
             "info": {
        "id": "3b960fd00333429db8c633dccd6bd6f4",
                "title": "测试标题",
                "userId": "e73139460d654221b9d1d607cc9530f9"*/

    @JSONField(name = "msg")
    public String msg;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "status")
    public String status;
    @JSONField(name = "info")
    public AddNoteInfo addNoteInfo;


}
