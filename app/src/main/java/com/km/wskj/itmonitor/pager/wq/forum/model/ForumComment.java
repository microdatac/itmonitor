package com.km.wskj.itmonitor.pager.wq.forum.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.model.BaseBean;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/4/1.
 */
//评论
public class ForumComment extends BaseBean {
    /**
     * path : null
     * imgs : []
     * quote : null
     * createTime : 2017-03-28 11:29:23
     * pId : null
     * id : fb1dd33d08a64700b1e40175ca160fa3
     * userId : e73139460d654221b9d1d607cc9530f9
     * isDel : 0
     * forumId : f27af1d4a6ff44d39bc6aae378610530
     * content : 不错
     */
    @JSONField(name = "path")
    public String path;
    @JSONField(name = "quote")
    public String quote;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "pId")
    public String pId;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "forumId")
    public String forumId;
    @JSONField(name = "content")
    public String content;
    @JSONField(name = "imgs")
    public List<ForumImg> imgs;
    @JSONField(name = "host")
    public String host;
    @JSONField(name = "head_img")
    public String head_img;
    @JSONField(name = "tel_no")
    public String tel_no;
    @JSONField(name = "nickname")
    public String nickname;

    //"host"  head_img
}
