package com.km.wskj.itmonitor.pager.monitor.application;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.model.MyChartData;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.utils.StatusBarCompat;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author by Xianling.Zhou on 2017/3/6.
 */
//应用系统名称指标
public class AppNameTargetActivity extends LActivity {
    //ui
    @BindView(R.id.tool_bar_title)
    TextView mTitle;
    @BindView(R.id.tool_bar_back)
    ImageView mBack;

    @BindView(R.id.app_name_target_ability)
    BarChart abilityChart;
    @BindView(R.id.app_name_target_time)
    BarChart timeChart;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_name_target);
        ButterKnife.bind(this);
        //沉浸式
        StatusBarCompat.compat(context, getResources().getColor(R.color.colorWs_blue));
        mTitle.setText("应用系统名称指标");
        mBack.setVisibility(View.VISIBLE);
        initView();
        initData();
    }

    private void initData() {
        initChart(abilityChart);
        initChart(timeChart);
    }

    private void initView() {

    }

    private void initChart(BarChart mBarChart) {
        /**
         * 初始化动作
         */
        mBarChart.getDescription().setEnabled(false);
        mBarChart.setMaxVisibleValueCount(60);
        mBarChart.setPinchZoom(false);
        mBarChart.setDrawBarShadow(false);
        mBarChart.setDrawGridBackground(false);

        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(10);
        xAxis.setGranularity(1f);

        /**
         * Y轴默认显示左右两个轴线
         */
        YAxis rightAxis = mBarChart.getAxisRight();  //获取右边的轴线
        rightAxis.setEnabled(true); //设置图表右边的y轴禁用
        rightAxis.enableGridDashedLine(10f, 10f, 0f);
        rightAxis.setDrawLabels(false);
        rightAxis.setDrawAxisLine(false);

        mBarChart.getAxisLeft().setDrawGridLines(false);//不要y轴线
        mBarChart.animateY(2500);
        mBarChart.getLegend().setEnabled(false);

        // 这是你要绘制的原始数据
        final List<MyChartData> data = new ArrayList<>();
        data.add(new MyChartData(0f, 10f, "15.30"));
        data.add(new MyChartData(1f, 20f, "16:30"));
        data.add(new MyChartData(2f, 30f, "17:30"));
        data.add(new MyChartData(3f, 40f, "18:30"));
        data.add(new MyChartData(4f, 50f, "19:30"));
        data.add(new MyChartData(5f, 100f, "20:30"));
        data.add(new MyChartData(6f, 50f, "21:30"));
        data.add(new MyChartData(7f, 60f, "22:30"));
        data.add(new MyChartData(8f, 90f, "23:30"));
        data.add(new MyChartData(9f, 100f, "24:30"));

        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return data.get(Math.min(Math.max((int) value, 0), data.size() - 1)).xAxisValue;
            }
        });


        setData(data, mBarChart);

    }

    private void setData(List<MyChartData> dataList, BarChart mBarChart) {

        ArrayList<BarEntry> values = new ArrayList<BarEntry>();
        List<Integer> colors = new ArrayList<Integer>();

        int green = Color.rgb(100, 203, 107);
        int red = Color.rgb(250, 89, 68);

        for (int i = 0; i < dataList.size(); i++) {

            MyChartData d = dataList.get(i);
            BarEntry entry = new BarEntry(d.xValue, d.yValue);
            values.add(entry);

            // specific colors
            if (d.yValue >= 90)
                colors.add(red);
            else
                colors.add(green);
        }

        BarDataSet set;

        if (mBarChart.getData() != null &&
                mBarChart.getData().getDataSetCount() > 0) {
            set = (BarDataSet) mBarChart.getData().getDataSetByIndex(0);
            set.setValues(values);
            mBarChart.getData().notifyDataChanged();
            mBarChart.notifyDataSetChanged();
        } else {
            set = new BarDataSet(values, "Values");
            set.setColors(colors);
            set.setValueTextColors(colors);

            BarData data = new BarData(set);
          /*  data.setValueTextSize(13f);
            data.setValueFormatter(new ValueFormatter());
            data.setBarWidth(0.8f);*/

            mBarChart.setData(data);
            mBarChart.invalidate();
        }


    }

    private class ValueFormatter implements IValueFormatter {

        private DecimalFormat mFormat;

        public ValueFormatter() {
            mFormat = new DecimalFormat("######.0");
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value);
        }

    }

    @OnClick(R.id.tool_bar_back)
    public void back() {
        finish();
    }
}
