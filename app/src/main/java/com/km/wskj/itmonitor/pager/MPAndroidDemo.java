package com.km.wskj.itmonitor.pager;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;


import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author by Xianling.Zhou on 2017/2/20.
 */

public class MPAndroidDemo extends LActivity {
    @BindView(R.id.linechart)
    LineChart mChart;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpandroid_demo);
        ButterKnife.bind(this);
        initData();
    }

    private void initData() {
        //创建描述信息
        Description description = new Description();
        description.setText("测试图表");
        description.setTextColor(Color.RED);
        description.setTextSize(12f);
        mChart.setDescription(description);//设置图表描述信息
        mChart.setNoDataText("没有数据熬");//没有数据时显示的文字
        mChart.setNoDataTextColor(Color.BLUE);//没有数据时显示文字的颜色
        mChart.setDrawGridBackground(false);//chart 绘图区后面的背景矩形将绘制
        mChart.setDrawBorders(false);//禁止绘制图表边框的线
//        mChart.setBorderColor(); //设置 chart 边框线的颜色。
//        mChart.setBorderWidth(); //设置 chart 边界线的宽度，单位 dp。
        mChart.setLogEnabled(true);//打印日志
        mChart.notifyDataSetChanged();//刷新数据
        mChart.invalidate();//重绘


        /**
         * 设置X轴线
         */
        //获取此图表的x轴
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);//设置轴启用或禁用 如果禁用以下的设置全部不生效
        xAxis.setDrawAxisLine(true);//是否绘制轴线
        xAxis.setDrawGridLines(true);//设置x轴上每个点对应的线
        xAxis.setDrawLabels(true);//绘制标签  指x轴上的对应数值
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//设置x轴的显示位置
        xAxis.setTextSize(12f);//设置字体
        xAxis.setTextColor(Color.BLACK);//设置字体颜色
        //设置竖线的显示样式为虚线
        //lineLength控制虚线段的长度
        //spaceLength控制线之间的空间
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setAxisMinimum(0f);//设置x轴的最小值
        xAxis.setAxisMaximum(10f);//设置最大值
        xAxis.setAvoidFirstLastClipping(true);//图表将避免第一个和最后一个标签条目被减掉在图表或屏幕的边缘
        xAxis.setLabelRotationAngle(0f);//设置x轴标签的旋转角度
        // 设置x轴显示标签数量  还有一个重载方法第二个参数为布尔值强制设置数量 如果启用会导致绘制点出现偏差
        xAxis.setLabelCount(6);
        xAxis.setTextColor(Color.BLUE);//设置轴标签的颜色
        xAxis.setTextSize(12f);//设置轴标签的大小
        xAxis.setGridLineWidth(1f);//设置竖线大小
        xAxis.setGridColor(Color.RED);//设置竖线颜色
        xAxis.setAxisLineColor(Color.GREEN);//设置x轴线颜色
        xAxis.setAxisLineWidth(1f);//设置x轴线宽度
//        xAxis.setValueFormatter();//格式化x轴标签显示字符


        /**
         * Y轴默认显示左右两个轴线
         */
        YAxis rightAxis = mChart.getAxisRight();  //获取右边的轴线
        rightAxis.setEnabled(false); //设置图表右边的y轴禁用
        YAxis leftAxis = mChart.getAxisLeft();   //获取左边的轴线
        leftAxis.enableGridDashedLine(10f, 10f, 0f); //设置网格线为虚线效果
        leftAxis.setDrawZeroLine(false); //是否绘制0所在的网格线
        leftAxis.setAxisMaximum(100f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setLabelCount(6);
        leftAxis.setTextSize(12f);
        leftAxis.setTextColor(Color.BLUE);


        //设置与图表交互
        mChart.setTouchEnabled(true); // 设置是否可以触摸
        mChart.setDragEnabled(true);// 是否可以拖拽
        mChart.setScaleEnabled(false);// 是否可以缩放 x和y轴, 默认是true
        mChart.setScaleXEnabled(true); //是否可以缩放 仅x轴
        mChart.setScaleYEnabled(true); //是否可以缩放 仅y轴
        mChart.setPinchZoom(true);  //设置x轴和y轴能否同时缩放。默认是否
        mChart.setDoubleTapToZoomEnabled(true);//设置是否可以通过双击屏幕放大图表。默认是true
        mChart.setHighlightPerDragEnabled(true);//能否拖拽高亮线(数据点与坐标的提示线)，默认是true
        mChart.setDragDecelerationEnabled(true);//拖拽滚动时，手放开是否会持续滚动，默认是true（false是拖到哪是哪，true拖拽之后还会有缓冲）
        mChart.setDragDecelerationFrictionCoef(0.99f);//与上面那个属性配合，持续滚动时的速度快慢，[0,1) 0代表立即停止。


        // 设置图例
        Legend l = mChart.getLegend();//图例
        l.setEnabled(false);
        l.setTextSize(10f);//设置文字大小
        l.setForm(Legend.LegendForm.CIRCLE);//正方形，圆形或线
        l.setFormSize(10f); // 设置Form的大小
        l.setWordWrapEnabled(true);//是否支持自动换行 目前只支持BelowChartLeft, BelowChartRight, BelowChartCenter
        l.setFormLineWidth(10f);//设置Form的宽度


        /**
         * Entry 坐标点对象  构造函数 第一个参数为x点坐标 第二个为y点
         */
        ArrayList<Entry> values1 = new ArrayList<>();

        values1.add(new Entry(0, 10));
        values1.add(new Entry(6, 15));
        values1.add(new Entry(9, 20));
        values1.add(new Entry(12, 5));
        values1.add(new Entry(15, 30));


        //LineDataSet每一个对象就是一条连接线
        LineDataSet set1;

        //判断图表中原来是否有数据
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            //获取数据1
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values1);
            //刷新数据
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            //设置数据1  参数1：数据源 参数2：图例名称
            set1 = new LineDataSet(values1, "");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);//设置线宽
            set1.setCircleRadius(3f);//设置焦点圆心的大小
            set1.enableDashedHighlightLine(10f, 5f, 0f);//点击后的高亮线的显示样式
            set1.setHighlightLineWidth(1f);//设置点击交点后显示高亮线宽
            set1.setHighlightEnabled(true);//是否禁用点击高亮线
            set1.setHighLightColor(Color.RED);//设置点击交点后显示交高亮线的颜色
            set1.setValueTextSize(9f);//设置显示值的文字大小
            set1.setDrawFilled(false);//设置禁用范围背景填充


            //格式化显示数据
            final DecimalFormat mFormat = new DecimalFormat("###,###,##0");
            set1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return mFormat.format(value);
                }
            });
            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(this, R.mipmap.logo);
                set1.setFillDrawable(drawable);//设置范围背景填充
            } else {
                set1.setFillColor(Color.BLACK);
            }

            //保存LineDataSet集合
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the datasets
            //创建LineData对象 属于LineChart折线图的数据集合
            LineData data = new LineData(dataSets);
            // 添加到图表中
            mChart.setData(data);
            //绘制图表
            mChart.invalidate();

            mChart.animateX(2000);//水平轴动画 在指定时间内 从左到右
//        animateY(int durationMillis) : 垂直轴动画 从下到上
//        animateXY(int xDuration, int yDuration) : 两个轴动画，从左到右，从下到上

        }
    }

}
