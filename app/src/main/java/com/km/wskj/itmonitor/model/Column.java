package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * Created by wuhan on 2017/3/20.
 */

public class Column extends BaseBean {
//    "pOrder": 1,
//            "createTime": "2017-03-01 18:19:16",
//            "pid": null,
//            "remark": "操作系统",
//            "id": "eb60219bd6c345578db80beb475dda48",
//            "isDel": 0,
//            "columnName": "操作系统"
    @JSONField(name = "pOrder")
    public int pOrder;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "pid")
    public String pid;
    @JSONField(name = "remark")
    public String remark;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "columnName")
    public String columnName;
}
