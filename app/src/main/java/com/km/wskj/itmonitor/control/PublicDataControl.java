package com.km.wskj.itmonitor.control;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.http.ZxlCallBack;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.UserInfo;
import com.km.wskj.itmonitor.pager.wq.forum.model.CommentParams;
import com.km.wskj.itmonitor.pager.wq.forum.model.ForumParams;
import com.km.wskj.itmonitor.pager.wq.note.model.AddNoteParms;
import com.km.wskj.itmonitor.pager.wq.note.model.NoteDetail;
import com.km.wskj.itmonitor.pager.wq.note.model.NoteImgs;
import com.yalantis.ucrop.entity.LocalMedia;
import com.zxl.zxlapplibrary.control.DataControl;
import com.zxl.zxlapplibrary.control.LoginControl;
import com.zxl.zxlapplibrary.control.UrlControl;
import com.zxl.zxlapplibrary.global.Constant;
import com.zxl.zxlapplibrary.http.OkHttpUtils;
import com.zxl.zxlapplibrary.http.callback.StringCallback;
import com.zxl.zxlapplibrary.model.ImageData;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.finalteam.toolsfinal.StringUtils;
import okhttp3.Call;
import okhttp3.Response;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */
//接口数据操作
public class PublicDataControl extends DataControl {
    public UserInfo mCurrentUser;//当前用户
    private Map<String, String> paramsValue;

    private Context mContext;
    private String baseUrl;
    private LApplication mLApplication;

    public PublicDataControl(Context context, LApplication app) {
        super(context);
        this.mContext = context;
        this.mLApplication = app;
        this.initSet();
    }

    /**
     * 初始化默认设置
     */
    public void initSet() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean haveinit = sp.getBoolean("initset", false);
        if (haveinit == false) {
            SharedPreferences.Editor et = sp.edit();
            et.putBoolean("initset", true);
            et.putBoolean(Constant.SET_AUTO_UPDATE, true);
            et.putBoolean(Constant.SET_AUTO_LOGIN, true);
            et.putBoolean(Constant.SET_PWD_REMEMBER, true);
            et.putString(Constant.SET_HTML_FONTSIZE, "3");
            et.commit();
        }
    }

    /**
     * 初始化配置的数据
     */
    public void initConfig() {

    }

    //登陆的回调，保存当前登陆用户
    public abstract class LoginCallback extends ZxlCallBack {
        @Override
        public Object parseNetworkResponse(Response response, int id) throws Exception {
            mCurrentUser = JSON.parseObject(validateData, UserInfo.class);
            return null;
        }

        @Override
        public void onAfter(int id) {
            if (mCurrentUser != null) {

            }
        }

    }

    /**
     * 根据设置尝试自动登录
     *
     * @return
     */
    public void autoLogin(Object tag, final LoginCallback callback) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(appContext);
        boolean isremember = sp.getBoolean(Constant.SET_PWD_REMEMBER, true);
        boolean isauto = sp.getBoolean(Constant.SET_AUTO_LOGIN, true);

        LogUtils.d("isrem=" + isremember + " isauto=" + isauto);
//        if (isremember && isauto) {
        String loginName = LoginControl.readLoginName(appContext);
        if (!StringUtils.isEmpty(loginName)) {
            String loginPwd = LoginControl.readPasswordForName(appContext, loginName);
            if (!StringUtils.isEmpty(loginPwd)) {
                LogUtils.d("尝试自动登录 name:" + loginName + " pwd:" + loginPwd);
            } else {
                callback.onResponse(null, 0);
            }
        } else {
            callback.onResponse(null, 0);
        }
    }

    public void logout() {
        this.mCurrentUser = null;
    }

    public void login(Object tag, String loginName, String loginPwd, LoginCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("username", loginName);
        paramsValue.put("password", loginPwd);
        OkHttpUtils
                .post()
                .url(UrlControl.url("user/login"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //注册方法

    /**
     * username true string 用户名
     * password true string 密码
     * user_cn_name false string 姓名
     */
    //老的注册方法  已经废弃
    public void reg(Object tag, String phonenum, String password, String nickname, ZxlCallBack callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("username", phonenum);
        paramsValue.put("password", password);
        paramsValue.put("user_cn_name", nickname);
        OkHttpUtils.post()
                .url(UrlControl.url("user/reg"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //注册根据手机号获取验证码
    public void getCode(Object tag, String telNo, StringCallback callback) {
        OkHttpUtils
                .get()
                .url(UrlControl.url("user/vcode"))
                .tag(tag)
                .addParams("telNo", telNo)
                .build()
                .execute(callback);
    }

    //根据验证码和手机号注册用户
    public void regUser(Object tag, String telNo, String vcode, ZxlGenericsCallback callback) {
        OkHttpUtils
                .post()
                .url(UrlControl.url("user/reg"))
                .tag(tag)
                .addParams("telNo", telNo)
                .addParams("vcode", vcode)
                .build()
                .execute(callback);
    }

    //注册成功以后更新用户信息
    public void updateInfo(Object tag, String userID, String nickname, String password, String imgId, LoginCallback callback) {
        paramsValue = new HashMap<>();
        if (userID != null)
            paramsValue.put("userId", userID);
        if (nickname != null)
            paramsValue.put("nickname", nickname);
        if (password != null)
            paramsValue.put("password", password);
        if (imgId != null)
            paramsValue.put("imgId", imgId);

        OkHttpUtils.post()
                .url(UrlControl.url("user/update"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //------------------------------------监控页面下的接口写在这里-----------------------------------

    //服务器类型接口
    public void serverTypeList(Object tag, ZxlGenericsCallback callback) {
        LogUtils.d("服务器类型接口请求了" + tag);
        OkHttpUtils.post()
                .url(mLApplication.getBaseUrl() + "server/typeList")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //服务器列表
    public void serverList(Object tag, int page, int rows, String serverType, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("page", page + "");
        paramsValue.put("rows", rows + "");
        if (serverType != null)
            paramsValue.put("serverType", serverType);
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/list")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //服务器详情
    public void serverDetail(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);

        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/detail")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //--------------------服务器历史性能写在这里---------------------

    /**
     * id 5065fb844e084acba65a3d5929040903
     * begTime
     * 2017-03-12 00:00:00
     * endTime
     * 2017-03-12 01:00:00
     */
    //CPU历史性能
    public void historyCpu(Object tag, String id, String begTime, String endTime, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        paramsValue.put("begTime", begTime);
        paramsValue.put("endTime", endTime);

        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/historyCpu")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //内存历史性能
    public void historyMemory(Object tag, String id, String begTime, String endTime, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        paramsValue.put("begTime", begTime);
        paramsValue.put("endTime", endTime);

        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/historyMemory")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //网络历史性能
    public void historyInternet(Object tag, String id, String begTime, String endTime, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        paramsValue.put("begTime", begTime);
        paramsValue.put("endTime", endTime);

        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/historyNet")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //--------------------服务器统计接口--------------------
    //类型统计
    public void serverTypeStatis(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/countByType")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警类型
    public void serverAlarmTypeStatis(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/countByAlarmType")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警状态
    public void serverAlarmStatus(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/countByAlarmStatus")
                .tag(tag)
                .build()
                .execute(callback);
    }


    //cpu
    public void cpu(Object tag, String ip, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("ip", ip);

        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "server/cupInfo")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //内存
    public void memory(Object tag, String ip, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("ip", ip);
        OkHttpUtils.post()
//                .url(UrlControl.url("server/memoryInfo"))
                .url(mLApplication.getBaseUrl() + "server/memoryInfo")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //硬盘
    public void serverHdMemory(Object tag, String ip, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("ip", ip);

        OkHttpUtils.get()
//                .url(UrlControl.url("server/hdInfo"))
                .url(mLApplication.getBaseUrl() + "server/hdInfo")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //数据库类型
    public void databaseType(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("database/typeList"))
                .url(mLApplication.getBaseUrl() + "database/typeList")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //数据库列表
    public void databaseList(Object tag, int page, int rows, String databaseType, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        int i = page + 1;
        paramsValue.put("page", i + "");
        paramsValue.put("rows", rows + "");

        if (databaseType != null)
            paramsValue.put("databaseType", databaseType);

        OkHttpUtils.get()
//                .url(UrlControl.url("database/list"))
                .url(mLApplication.getBaseUrl() + "database/list")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);

    }

    //数据库详情
    public void databaseDetail(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils.get()
//                .url(UrlControl.url("database/info"))
                .url(mLApplication.getBaseUrl() + "database/info")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //数据库配置
    public void databaseConfig(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "database/configInfo")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //数据库启动时间查询
    public void db_startup(Object tag, String id, StringCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "database/runTime")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //-------------数据库统计图表写在这里---------------------------
    //类型统计
    public void dbTypeStatis(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "database/countByType")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警类型
    public void dbAlarmTypeStatis(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "database/countByAlarmType")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警状态
    public void dbAlarmStatus(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "database/countByAlarmStatus")
                .tag(tag)
                .build()
                .execute(callback);
    }


    //mysql统计图表
    public void MySQLkeyBuffer(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils.get()
//                .url(UrlControl.url("database/keyBuffer"))
                .url(mLApplication.getBaseUrl() + "database/keyBuffer")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //mysql表大小
    public void mysqlSize(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils.get()
//                .url(UrlControl.url("database/tableSize"))
                .url(mLApplication.getBaseUrl() + "database/tableSize")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //sqlserver开始

    //高速缓冲区命中率的值
    public void b(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("database/bufferCacheHitRatio"))
                .url(mLApplication.getBaseUrl() + "database/bufferCacheHitRatio")
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }

    //sqlserver文件大小
    public void sqlserversize(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("database/dataSize"))
                .url(mLApplication.getBaseUrl() + "database/dataSize")
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }

    //sqlserver io
    public void sqlserverIO(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("database/io"))
                .url(mLApplication.getBaseUrl() + "database/io")
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }

    //oracle表空间
    public void oracleSize(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("database/datatablesSize"))
                .url(mLApplication.getBaseUrl() + "database/datatablesSize")
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }

    //oracle缓冲命中率
    public void oracleCacheHit(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("database/cacheHit"))
                .url(mLApplication.getBaseUrl() + "database/cacheHit")
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }


    //服务器历史性能
    public void serverHistory(Object tag, String id, String begTime,
                              String endTime, ZxlGenericsCallback callback) {

        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        if (begTime != null)
            paramsValue.put("begTime", begTime);
        if (endTime != null)
            paramsValue.put("endTime", endTime);

        OkHttpUtils.get()
//                .url(UrlControl.url("server/history"))
                .url(mLApplication.getBaseUrl() + "server/history")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //报警列表

    /**
     * page false string 1 页数
     * rows false string 20 每页条数
     * deviceId false string 设备ID
     * status false number -1 0 未处理 1 已处理 -1全部
     * level1 false string 一级分类ID
     * level2 false string 二级分类ID
     * level3 false string 三级分类ID
     *
     * @param tag
     */
    public void alarmList(Object tag, String deviceId, int page, int rows, int status
            , String level1
            , String level2
            , String level3
            , ZxlGenericsCallback callback) {

        paramsValue = new HashMap<>();

        paramsValue.put("page", page + "");
        paramsValue.put("rows", rows + "");
        paramsValue.put("status", status + "");

        if (deviceId != null)
            paramsValue.put("deviceId", deviceId);

        if (level1 != null)
            paramsValue.put("level1", level1);

        if (level2 != null)
            paramsValue.put("level1", level2);

        if (level3 != null)
            paramsValue.put("level1", level3);

        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "alarm/list")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //报警一级分类
    public void alramType(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
//                .url(UrlControl.url("alarm/typeList"))
                .url(mLApplication.getBaseUrl() + "alarm/typeList")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //处理报警

    /**
     * id false string 主键ID
     * handleMessage false string 报警处理记录
     *
     * @param tag
     */
    public void handleAlarm(Object tag, String id, String handleMessage, ZxlGenericsCallback callback) {
        OkHttpUtils
                .post()
//                .url(UrlControl.url("alarm/handle"))
                .url(mLApplication.getBaseUrl() + "alarm/handle")
                .tag(tag)
                .addParams("id", id)
                .addParams("handleMessage", handleMessage)
                .build()
                .execute(callback);
    }


    //报警根据类型进行统计
    public void typeCount(Object tag, StringCallback callback) {
        OkHttpUtils
                .get()
//                .url(UrlControl.url("alarm/typeCount"))
                .url(mLApplication.getBaseUrl() + "alarm/typeCount")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警根据状态进行统计
    public void statusCount(Object tag, StringCallback callback) {
        OkHttpUtils
                .get()
//                .url(UrlControl.url("alarm/statusCount"))
                .url(mLApplication.getBaseUrl() + "alarm/statusCount")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警详情
    public void alarmDetail(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
                .url(mLApplication.getBaseUrl() + "alarm/detail")
                .addParams("id", id)
                .tag(tag)
                .build()
                .execute(callback);
    }


    //app/list
    //应用列表
    public void appList(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
                .url(mLApplication.getBaseUrl() + "app/list")
                .tag(tag)
                .build()
                .connTimeOut(200000)
                .writeTimeOut(200000)
                .readTimeOut(200000)
                .execute(callback);
    }


    //网络列表
    public void internetList(Object tag, int page, int rows, String typeId, ZxlGenericsCallback callback) {
       /* page false string 1 页码
        rows false string 20 每页条数*/
        paramsValue = new HashMap<>();
        paramsValue.put("page", page + "");
        paramsValue.put("rows", rows + "");
        if (!typeId.equals(""))
            paramsValue.put("typeId", typeId);
        OkHttpUtils
                .get()
//                .url(UrlControl.url("net/list"))
                .url(mLApplication.getBaseUrl() + "net/list")
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //网络类型
    public void internetType(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
//                .url(UrlControl.url("net/typeList"))
                .url(mLApplication.getBaseUrl() + "net/typeList")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //------------------网络统计------------------
    //类型统计
    public void netTypeStatis(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "net/countByType")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警类型
    public void netAlarmTypeStatis(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "net/countByAlarmType")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //报警状态
    public void netAlarmStatus(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils.get()
                .url(mLApplication.getBaseUrl() + "net/countByAlarmStatus")
                .tag(tag)
                .build()
                .execute(callback);
    }

    //net/portList  端口列表
    public void portList(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
//                .url(UrlControl.url("net/portList"))
                .url(mLApplication.getBaseUrl() + "net/portList")
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }


    //---------------------微栖相关接口写在这里---------------------------

    //轮播图接口
    public void bannerList(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
                .url(UrlControl.url("article/slider"))
                .tag(tag)
                .build()
                .execute(callback);
    }

    /**
     * pid false string 上级ID，空为取顶级栏目
     * 知识库栏目接口
     *
     * @param tag
     */
    public void knowledgeLib(Object tag, String pid, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        if (pid != null)
            paramsValue.put("pid", pid);
        OkHttpUtils
                .get()
                .url(UrlControl.url("knowledge/columnList"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    /**
     * pageNumber false string 1 当前页
     * pageSize false string 10 每页数量
     * columnId false string 栏目ID
     * 知识列表接口
     */

    public void knowledgeList(Object tag, int pageNumber, int pageSize, String columnId, String keyword
            , ZxlGenericsCallback callback) {

        paramsValue = new HashMap<>();
        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");
        if (columnId != null)
            paramsValue.put("columnId", columnId);
        if (keyword != null)
            paramsValue.put("keyword", keyword);

        OkHttpUtils
                .get()
                .url(UrlControl.url("knowledge/list"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    /**
     * id false string 主键ID
     * 知识详情
     */
    public void knowledgeDetail(Object tag, String id, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
                .url(UrlControl.url("knowledge/info"))
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }

    //新闻栏目
    public void newsColumn(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils
                .get()
                .url(UrlControl.url("article/columnList"))
                .tag(tag)
                .build()
                .execute(callback);
    }

    //新闻列表

    /**
     * pageNumber false string 1 当前页
     * pageSize false string 10 每页数量
     * columnId false string 栏目ID
     *
     * @param tag
     * @param callback
     */
    public void newsList(Object tag, int pageNumber, int pageSize, String columnId, String keyword, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");
        if (columnId != null)
            paramsValue.put("columnId", columnId);
        if (keyword != null)
            paramsValue.put("keyword", keyword);
        OkHttpUtils
                .get()
                .url(UrlControl.url("article/list"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //新闻详情接口
    public void newsDetail(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils
                .get()
                .url(UrlControl.url("article/info"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //----------------微数学院接口写在这里----------------------
    //培训列表

    /**
     * columnId
     * pageNumber
     * pageSize
     */
    public void collegeList(Object tag, String columnId, int pageNumber, int pageSize, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();

        if (columnId != null)
            paramsValue.put("columnId", columnId);

        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");
        OkHttpUtils
                .get()
                .url(UrlControl.url("train/list"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //培训详情
    public void collegeDetail(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils
                .get()
                .url(UrlControl.url("train/info"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //备件专区商品列表
    public void goodsList(Object tag, String columnId, int pageNumber, int pageSize, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        if (columnId != null)
            paramsValue.put("columnId", columnId);
        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");

        OkHttpUtils
                .get()
                .url(UrlControl.url("goods/list"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //配件详情列表
    public void goodsDetail(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils
                .get()
                .url(UrlControl.url("goods/info"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //商品详情HTML
    public void goodsHtml(Object tag, String id, StringCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils
                .get()
                .url(UrlControl.url("goods/html"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //配件分类
    public void partsColumn(Object tag, String pid, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        if (pid != null)
            paramsValue.put("pid", pid);
        OkHttpUtils
                .get()
                .url(UrlControl.url("goods/columnList"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //---------------------------------运维云笔记写在这里--------------------------
    //添加笔记本

    /**
     * id false string 主键ID 编辑时需要
     * userId true string 用户ID
     * typeName true string 类型名称
     * 演示
     */
    public void addNotebook(Object tag, String id, String typeName, ZxlCallBack callback) {
        paramsValue = new HashMap<>();
        if (id != null)
            paramsValue.put("id", id);
        paramsValue.put("userId", mCurrentUser.userId);
        paramsValue.put("typeName", typeName);
        OkHttpUtils
                .post()
                .url(UrlControl.url("note/saveType"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //笔记本列表
    public void noteBookList(Object tag, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        if (mCurrentUser.userId != null)
            paramsValue.put("userId", mCurrentUser.userId);
        OkHttpUtils
                .post()
                .url(UrlControl.url("note/typeList"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    //删除笔记本
    public void delNoteBook(Object tag, String id, ZxlCallBack callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", id);
        OkHttpUtils
                .post()
                .url(UrlControl.url("note/delType"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    public abstract class NoteDetailCallback extends ZxlCallBack {
        private NoteDetail ni;

        public NoteDetailCallback(NoteDetail ni) {
            this.ni = ni;
        }

        @Override
        public Object parseNetworkResponse(Response response, int flag) throws Exception {
            NoteDetail nd = JSON.parseObject(validateData, NoteDetail.class);

            JSONObject json_data = JSON.parseObject(validateData);
            JSONArray json_array = json_data.getJSONArray("imgs");

            List<NoteImgs> imgs = nd.imgs;

            if (imgs != null && imgs.size() > 0) {
                for (int i = 0; i < json_array.size(); i++) {
                    ImageData img = new ImageData();
                    img.imgId = imgs.get(i).attachId;

                    img.imgUrl = imgs.get(i).host
                            + imgs.get(i).filePath
                            + imgs.get(i).newFileName;

                    nd.imgList.add(img);
                }
            }
            ni.copyFrom(nd);
            return null;
        }
    }

    //笔记详情
    public void noteDetail(Object tag, String noteId, NoteDetailCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", noteId);
        OkHttpUtils
                .get()
                .url(UrlControl.url("note/info"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    public void noteDetail_(Object tag, String noteId, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("id", noteId);
        OkHttpUtils
                .get()
                .url(UrlControl.url("note/info"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //根据ID获得栏目分类
    public void getColumnsByID(Object tag, String id, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("pid", id);
        OkHttpUtils
                .post()
                .url(UrlControl.url("knowledge/columnList"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    public void getColumns(Object tag, ZxlGenericsCallback callback) {
        OkHttpUtils
                .post()
                .url(UrlControl.url("knowledge/columnList"))
                .tag(tag)
                .build()
                .execute(callback);
    }


    /*pageNumber false string 1 当前页
    pageSize false string 10 每页数量
    columnId false string 栏目ID
    userId true string 用户ID*/
    public void noteList(Object tag, int pageNumber, int pageSize, String columnId, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");
        if (columnId != null)
            paramsValue.put("columnId", columnId);
        paramsValue.put("userId", mCurrentUser.userId);
        OkHttpUtils
                .get()
                .url(UrlControl.url("note/list"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }


    /**
     * 上传图片接口
     */
    public void uploadPic(Object tag, ImageData img, StringCallback callback) {
        LogUtils.d("upload pic:" + img.fileCompressPath);
        File picfile = new File(img.fileCompressPath == null ? img.fileOrgPath : img.fileCompressPath);

        OkHttpUtils
                .post()
                .url("http://www.microdatac.com/iOPS/uploadFile")
                .tag(tag)
                .addFile("pic", "note.jpg", picfile)
                .build()
                .execute(callback);
    }

    /**
     * 添加和修改笔记
     *
     * @param tag
     * @param addNoteParms
     * @param callback     id false string 主键ID，编辑的时候才需要
     *                     userId true string e73139460d654221b9d1d607cc9530f9 用户ID
     *                     columnId false string 栏目ID
     *                     title false string 测试标题 标题
     *                     content false string 测试内容 内容
     *                     fileIds false string 附件ID列表，逗号分隔
     *                     imgIds false string 图片ID列表，逗号分隔
     */
    public void addNote(Object tag, AddNoteParms addNoteParms, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();

        if (addNoteParms.id != null)
            paramsValue.put("id", addNoteParms.id);
        if (mCurrentUser.userId != null)
            paramsValue.put("userId", mCurrentUser.userId);
        if (addNoteParms.columnId != null)
            paramsValue.put("columnId", addNoteParms.columnId);
        if (addNoteParms.title != null)
            paramsValue.put("title", addNoteParms.title);
        if (addNoteParms.content != null)
            paramsValue.put("content", addNoteParms.content);
        if (addNoteParms.imgIds != null)
            paramsValue.put("imgIds", addNoteParms.imgIds);
        if (addNoteParms.fileIds != null)
            paramsValue.put("fileIds", addNoteParms.fileIds);

        OkHttpUtils
                .post()
                .url(UrlControl.url("note/add"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //删除笔记接口
    public void delNote(Object tag, String id, StringCallback callback) {
        OkHttpUtils
                .get()
                .url(UrlControl.url("note/del"))
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }


    //-----------------论坛-------------------------
    //帖子列表

    /**
     * columnId false string 所属栏目名称
     * pageNumber false string 1 页数
     * pageSize false string 10 每页数量
     * keyword false string 关键词
     */
    public void forumList(Object tag, String columnId, int pageNumber
            , int pageSize, String keyword, ZxlGenericsCallback callback) {

        paramsValue = new HashMap<>();
        if (columnId != null)
            paramsValue.put("columnId", columnId);
        if (keyword != null)
            paramsValue.put("keyword", keyword);

        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");

        OkHttpUtils
                .get()
                .url(UrlControl.url("forum/list"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    //根据主键id查询帖子详情
    public void forumDetail(Object tag, String id, ZxlGenericsCallback callback) {

        OkHttpUtils
                .get()
                .url(UrlControl.url("forum/info"))
                .tag(tag)
                .addParams("id", id)
                .build()
                .execute(callback);
    }


    //上传图片接口
    public void uploadPic(Object tag, LocalMedia img, StringCallback callback) {
        LogUtils.d("upload pic:" + img.getPath());
        File picfile = new File(img.getPath());

        OkHttpUtils
                .post()
                .url("http://www.microdatac.com/iOPS/uploadFile")
                .tag(tag)
                .addFile("pic", "note.jpg", picfile)
                .build()
                .execute(callback);
    }

    /**
     * id false string 帖子ID 编辑时才需要
     * userId true string 发帖人ID
     * columnId true string 所属栏目ID
     * title true string 帖子标题
     * content true string 帖子内容
     * imgIds false string 图片ID 逗号分隔
     *
     * @param tag
     * @param params
     * @param callback
     */
    //发表主题帖子
    public void sendForum(Object tag, ForumParams params, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        if (params.id != null)
            paramsValue.put("id", params.id);

        if (params.userId != null)
            paramsValue.put("userId", params.userId);

        if (params.columnId != null)
            paramsValue.put("columnId", params.columnId);

        if (params.title != null)
            paramsValue.put("title", params.title);

        if (params.content != null)
            paramsValue.put("content", params.content);

        if (params.imgIds != null)
            paramsValue.put("imgIds", params.imgIds);


        OkHttpUtils
                .post()
                .url(UrlControl.url("forum/add"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    /**
     * @param tag
     * @param params
     * @param callback id false string 评论ID 编辑的时候用
     *                 userId false string 评论人ID
     *                 forumId false string 帖子ID
     *                 content false string 评论内容
     *                 imgIds false string 图片ID列表，逗号分隔
     */
    //添加评论
    public void addComment(Object tag, CommentParams params, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        if (params.id != null)
            paramsValue.put("id", params.id);

        if (params.userId != null)
            paramsValue.put("userId", params.userId);

        if (params.forumId != null)
            paramsValue.put("forumId", params.forumId);

        if (params.content != null)
            paramsValue.put("content", params.content);

        if (params.imgIds != null)
            paramsValue.put("imgIds", params.imgIds);

        OkHttpUtils
                .post()
                .url(UrlControl.url("forum/addComment"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

    /**
     * pageNumber
     * 1
     * pageSize
     * 10
     * forumId
     *
     * @param tag
     */
    public void commentList(Object tag, int pageNumber, int pageSize, String forumId, ZxlGenericsCallback callback) {
        paramsValue = new HashMap<>();
        paramsValue.put("pageNumber", pageNumber + "");
        paramsValue.put("pageSize", pageSize + "");
        paramsValue.put("forumId", forumId);

        OkHttpUtils
                .post()
                .url(UrlControl.url("forum/commentList"))
                .tag(tag)
                .params(paramsValue)
                .build()
                .execute(callback);
    }

}
