package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/2/28.
 */

public class AlarmListType extends BaseBean {
    /*id true string 类型ID
    val true string 类型名称*/

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "val")
    public String val;

    public String toString() {
        return val;
    }
}
