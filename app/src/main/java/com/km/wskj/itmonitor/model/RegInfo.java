package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/3/22.
 */
//注册返回的信息
public class RegInfo extends BaseBean {

    /**
     * user_role_id : dd3b26f2714c4376a3fb097afd1cb91f
     * user_id : d9600aef565e457c8e3c827f1940f1ed
     * tel_no : 18388139726
     * username : 18388139726
     */

    @JSONField(name = "user_role_id")
    public String userRoleId;
    @JSONField(name = "user_id")
    public String userId;
    @JSONField(name = "tel_no")
    public String telNo;
    @JSONField(name = "username")
    public String username;
}
