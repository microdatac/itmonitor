package com.km.wskj.itmonitor.pager.monitor.dh;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.monitor.dh.domain.WaterTestItem;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zrc.widget.SimpleFooter;
import zrc.widget.SimpleHeader;
import zrc.widget.ZrcListView;

/**
 * @author by Xianling.Zhou on 2017/4/24.
 */
//新风机页面
public class FreshAirMachineActivity extends LActivity {

    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.zrcListView)
    ZrcListView mZrcListView;

    MyAdapter mAdapter;
    List<WaterTestItem> mDatas;
    WaterTestItem wItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_test);
        ButterKnife.bind(this);

        toolBarBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText("新风机");
        mDatas = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            wItem = new WaterTestItem();
            wItem.area = i + "#新风机";
            wItem.state = "0";  //暂定0正常 1异常
            mDatas.add(wItem);
        }
        initPullToRefresh();
    }

    private void initPullToRefresh() {
        //添加头布局
        View view = getLayoutInflater().inflate(R.layout.fresh_air_machine_header, null);

        if (mZrcListView.getHeaderViewsCount() == 0)
            mZrcListView.addHeaderView(view, null, true);//注意：第三个参数必须为true，否则无效

        //配置listview
        ColorDrawable line = new ColorDrawable(getResources().getColor(R.color.white50));
        mZrcListView.setDivider(line);
        mZrcListView.setDividerHeight(1);

        //设置下拉刷新的样式（可选，但如果没有Header则无法下拉刷新）
        SimpleHeader header = new SimpleHeader(context);
        header.setTextColor(ContextCompat.getColor(context, R.color.text_gray));
        header.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setHeadable(header);

        //设置加载更多的样式（可选）
        SimpleFooter footer = new SimpleFooter(context);
        footer.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setFootable(footer);

        // 设置列表项出现动画（可选）
        //zListView.setItemAnimForTopIn(R.anim.topitem_in);
        mZrcListView.setItemAnimForBottomIn(R.anim.bottomitem_in);

        // 下拉刷新事件回调（可选）
        mZrcListView.setOnRefreshStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onRefresh();
            }
        });

        // 加载更多事件回调（可选）
        mZrcListView.setOnLoadMoreStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onLoadMore();
            }
        });

        // 项目选中
        mZrcListView.setOnItemClickListener(new ZrcListView.OnItemClickListener() {
            @Override
            public void onItemClick(ZrcListView parent, View view, int position, long id) {

            }
        });

        mAdapter = new MyAdapter();
        mZrcListView.setAdapter(mAdapter);
    }

    private void onRefresh() {
        mAdapter.notifyDataSetChanged();
        mZrcListView.setRefreshSuccess();
    }

    private void onLoadMore() {
        mAdapter.notifyDataSetChanged();
        mZrcListView.setLoadMoreSuccess();
    }

    @OnClick(R.id.tool_bar_back)
    public void onClick() {
        finish();
    }

    /**
     * 适配器
     */
    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (mDatas == null) {
                return 0;
            }
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            WaterTestItem item = mDatas.get(position);
            return item;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final BaseAdapterHelper helper = BaseAdapterHelper.get(context
                    , convertView, parent, R.layout.item_fresh_air_machine);
            WaterTestItem item = mDatas.get(position);
            helper.setText(R.id.tv_area_name, item.area);

            return helper.getView();
        }
    }
}
