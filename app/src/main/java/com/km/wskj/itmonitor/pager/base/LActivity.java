package com.km.wskj.itmonitor.pager.base;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.km.wskj.itmonitor.LApplication;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.control.PublicDataControl;
import com.km.wskj.itmonitor.pager.MainActivity;
import com.km.wskj.itmonitor.utils.ActivityCollector;
import com.km.wskj.itmonitor.utils.StatusBarCompat;
import com.zxl.zxlapplibrary.activity.BaseActivity;
import com.zxl.zxlapplibrary.control.VersionControl;
import com.zxl.zxlapplibrary.util.LogUtils;

import cn.pedant.SweetAlert.ProgressHelper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */

public class LActivity extends BaseActivity {

    public PublicDataControl pdc = LApplication.app.pdc;
    public VersionControl vc = LApplication.app.vc;
    private SweetAlertDialog dialog;
    public LActivity mActivity;

    //--控制重启----------------------------------------------------------------

    public boolean checkLoadingFlag() {
        return true;
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //沉浸式
        StatusBarCompat.compat(context, getResources().getColor(R.color.colorWs_blue));
        ActivityCollector.addActivity(this);
        mActivity = this;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (checkLoadingFlag()) {
            if (pdc.loadingFlag != 1) {
                LogUtils.d("----- app restorestate,now need to goto loading -----");
                restart();
            }
        }
    }

    public void restart() {
        Intent t = new Intent(this, MainActivity.class);
        t.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        t.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(t);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityCollector.removeActivity(this);
    }

    public void showLoading() {
        dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        ProgressHelper helper = dialog.getProgressHelper();
        helper.setBarColor(Color.parseColor("#A5DC86"));
        dialog.setTitleText("Loading");
        dialog.setCancelable(false);
        try {
            dialog.show();
        } catch (Exception e) {
            LogUtils.e("error", e);
        }
    }

    public void myDisMiss() {
        dialog.dismiss();
    }

    //各种toast
    public void successToasty(String str, boolean isicon) {
        Toasty.success(context, str, Toast.LENGTH_SHORT, isicon).show();
    }

    public void errorToasty(String str, boolean isicon) {
        Toasty.error(context, str, Toast.LENGTH_SHORT, isicon).show();
    }

    public void normalToasty(String str) {
        Toasty.normal(context, str, Toast.LENGTH_SHORT).show();
    }


}
