package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.pager.wq.note.model.NoteDetail;

import java.util.List;

public class NoteAllDetail extends BaseBean {

    @JSONField(name = "imgs")
    public List<NoteImage> noteImages;
    @JSONField(name = "files")
    public List<FileDetails> noteFiles;
    @JSONField(name = "info")
    public NoteDetail noteInfo;
}
