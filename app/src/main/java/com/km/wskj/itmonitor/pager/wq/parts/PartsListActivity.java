package com.km.wskj.itmonitor.pager.wq.parts;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.fingdo.statelayout.StateLayout;
import com.km.wskj.itmonitor.R;
import com.km.wskj.itmonitor.http.ZxlGenericsCallback;
import com.km.wskj.itmonitor.model.CollegeItem;
import com.km.wskj.itmonitor.model.Imgs;
import com.km.wskj.itmonitor.model.PartsColumn;
import com.km.wskj.itmonitor.model.PartsItem;
import com.km.wskj.itmonitor.pager.base.LActivity;
import com.km.wskj.itmonitor.pager.wq.wscollege.CollegeDetailActivity;
import com.zxl.zxlapplibrary.adapter.BaseAdapterHelper;
import com.zxl.zxlapplibrary.global.Constant;
import com.zxl.zxlapplibrary.util.LogUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import zrc.widget.SimpleFooter;
import zrc.widget.SimpleHeader;
import zrc.widget.ZrcListView;


/**
 * @author by Xianling.Zhou on 2017/2/9.
 */

public class PartsListActivity extends LActivity implements StateLayout.OnViewRefreshListener {
    private static final String TAG = "PartsListActivity";
    @BindView(R.id.tool_bar_back)
    ImageView toolBarBack;
    @BindView(R.id.tool_bar_title)
    TextView toolBarTitle;
    @BindView(R.id.tool_bar_right)
    ImageView toolBarRight;
    @BindView(R.id.zrcListView)
    ZrcListView mZrcListView;
    @BindView(R.id.state_layout)
    StateLayout mStateLayout;

    PartsColumn mPartsColumn;
    List<PartsItem> piList;
    int lastUpdateNum;
    int pageIndex;
    MyAdapter mAdapter;

    //ui
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parts_list);
        if (getIntent().hasExtra("data"))
            mPartsColumn = (PartsColumn) getIntent().getSerializableExtra("data");
        ButterKnife.bind(this);

        toolBarTitle.setText(mPartsColumn.columnName);
        toolBarBack.setVisibility(View.VISIBLE);
        toolBarRight.setVisibility(View.VISIBLE);
        toolBarRight.setImageResource(R.mipmap.search_white);

        initData();

    }

    private void initData() {
        mStateLayout.showLoadingView();
        if (mPartsColumn == null)
            return;

        pdc.goodsList(HTTP_TASK_TAG, mPartsColumn.id, 1, Constant.PageListSize, new ZxlGenericsCallback<List<PartsItem>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                mZrcListView.setRefreshFail("数据获取失败");
                mStateLayout.showErrorView();
            }

            @Override
            public void onResponse(List<PartsItem> pg, int i) {
                if (pg.size() == 0)
                    mStateLayout.showEmptyView();

                piList = pg;
                lastUpdateNum = pg.size();
                pageIndex = 1;
                //control
                mAdapter.notifyDataSetChanged();
                mZrcListView.setRefreshSuccess();

                if (haveMore())
                    mZrcListView.startLoadMore();
                else
                    mZrcListView.stopLoadMore();
            }
        });

        initPullToRefresh();
    }

    private void initPullToRefresh() {
        //配置listview
        ColorDrawable line = new ColorDrawable(Color.parseColor("#eeeeee"));
        mZrcListView.setDivider(line);
        mZrcListView.setDividerHeight(1);

        //设置下拉刷新的样式（可选，但如果没有Header则无法下拉刷新）
        SimpleHeader header = new SimpleHeader(context);
        header.setTextColor(ContextCompat.getColor(context, R.color.text_gray));
        header.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setHeadable(header);

        //设置加载更多的样式（可选）
        SimpleFooter footer = new SimpleFooter(context);
        footer.setCircleColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mZrcListView.setFootable(footer);

        // 设置列表项出现动画（可选）
        //zListView.setItemAnimForTopIn(R.anim.topitem_in);
        mZrcListView.setItemAnimForBottomIn(R.anim.bottomitem_in);

        // 下拉刷新事件回调（可选）
        mZrcListView.setOnRefreshStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onRefresh();
            }
        });

        // 加载更多事件回调（可选）
        mZrcListView.setOnLoadMoreStartListener(new ZrcListView.OnStartListener() {
            @Override
            public void onStart() {
                onLoadMore();
            }
        });

        // 项目选中
        mZrcListView.setOnItemClickListener(new ZrcListView.OnItemClickListener() {
            @Override
            public void onItemClick(ZrcListView parent, View view, int position, long id) {
                PartsItem item = (PartsItem) parent.getItemAtPosition(position);
                Intent t = new Intent(context, PartsDetailActivity.class);
                t.putExtra("id", item.id);
                startActivity(t);
            }
        });

        mAdapter = new MyAdapter();
        mZrcListView.setAdapter(mAdapter);
        mStateLayout.showContentView();
    }

    //下拉刷新
    private void onRefresh() {
        pdc.goodsList(HTTP_TASK_TAG, mPartsColumn.id, 1, Constant.PageListSize, new ZxlGenericsCallback<List<PartsItem>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                mZrcListView.setRefreshFail("数据获取失败");
                mStateLayout.showErrorView();
            }

            @Override
            public void onResponse(List<PartsItem> pg, int i) {
                if (pg.size() == 0)
                    mStateLayout.showEmptyView();
                piList = pg;
                lastUpdateNum = pg.size();
                pageIndex = 1;
                //control
                mAdapter.notifyDataSetChanged();
                mZrcListView.setRefreshSuccess();

                if (haveMore())
                    mZrcListView.startLoadMore();
                else
                    mZrcListView.stopLoadMore();
            }
        });
    }

    //加载更多
    private void onLoadMore() {
        pdc.goodsList(HTTP_TASK_TAG, mPartsColumn.id, pageIndex + 1, Constant.PageListSize, new ZxlGenericsCallback<List<PartsItem>>() {
            @Override
            public void onError(Call call, Exception e, int i) {
                LogUtils.e(TAG, e);
                mZrcListView.setRefreshFail("数据获取失败");
            }

            @Override
            public void onResponse(List<PartsItem> pg, int i) {

                lastUpdateNum = pg.size();
                if (pg.size() > 0) {
                    piList.addAll(pg);
                    pageIndex++;
                }
                //control
                mAdapter.notifyDataSetChanged();
                mZrcListView.setLoadMoreSuccess();
                if (haveMore())
                    mZrcListView.startLoadMore();
                else
                    mZrcListView.stopLoadMore();

            }
        });
    }

    private boolean haveMore() {
        return lastUpdateNum > 0;
    }


    //点击重试
    @Override
    public void refreshClick() {
        initData();
    }

    @Override
    public void loginClick() {

    }

    @OnClick({R.id.tool_bar_back, R.id.tool_bar_right})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tool_bar_back:
                finish();
                break;
            case R.id.tool_bar_right:
                normalToasty("search");
                break;
        }
    }

    /**
     * 适配器
     */
    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (piList == null) {
                return 0;
            }
            return piList.size();
        }

        @Override
        public Object getItem(int position) {
            return piList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final BaseAdapterHelper helper = BaseAdapterHelper.get(context, convertView, parent, R.layout.item_parts_list);
            PartsItem item = piList.get(position);
            helper.setText(R.id.tv_title, item.title + "");
            helper.setText(R.id.tv_des, item.applicableModels + "");
            helper.setText(R.id.tv_price, "￥" + item.price + "");

            //图片url
            if (item.imgs.size() > 0) {
                Imgs imgs = item.imgs.get(0);
                String url = imgs.host + imgs.file_path + imgs.new_file_name;
                Glide.with(mActivity)
                        .load(url)
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                helper.setImageBitmap(R.id.iv_preview, resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                LogUtils.e("LLpp:加载失败");
                            }
                        }); //方法中设置asBitmap可以设置回调类型
            }
            return helper.getView();
        }
    }

}
