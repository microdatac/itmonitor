package com.km.wskj.itmonitor.model;

/**
 * Created by wuhan on 2017/3/17.
 */

public class NoteImageJson {

    /**
     * path : /upload/2017/03/17/
     * attach_id : e2b5dfcc3b43422a893450e38ff4a582
     * original_name : 1.jpg
     * name : 201703171024304071f33.jpg
     * status : y
     */

    private String path;
    private String attach_id;
    private String original_name;
    private String name;
    private String status;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAttach_id() {
        return attach_id;
    }

    public void setAttach_id(String attach_id) {
        this.attach_id = attach_id;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
