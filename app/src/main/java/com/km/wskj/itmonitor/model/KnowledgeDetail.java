package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/14.
 */

public class KnowledgeDetail extends BaseBean {
/* "imgs": [ ],
         "files": [ ],
         "info": {
        "path": "51e994eaa9c84520b398e0d36a4e44aa",
                "createTime": "2017-03-09 15:32:13",
                "columnId": "51e994eaa9c84520b398e0d36a4e44aa",
                "author": null,
                "id": "7a049ddae43546a69a7220c0237a729c",
                "source": null,
                "title": "浏览器兼容性",
                "isDel": 0,
                "content": "<p><span style=\"font-size: 12px; font-family: 宋体;\">1、启动IE浏览器，在选项菜单下面选择兼容性视图设置。</span></p><p><img alt=\"clip_image001.jpg\" src=\"http://wsiops.iok.la:8028/iOPS/ueditor/jsp/upload/image/20161212/1481525929143012559.jpg\" title=\"1481525929143012559.jpg\"/></p><p><span style=\"font-size: 12px; font-family: 宋体;\">2、填入你所需要设置兼容模式的网址，点击添加，然后在把下面“在兼容性视图中显示Intranet站点（I）”前面的勾选，最后点击关闭即可。</span></p><p style=\"margin-top:auto;margin-bottom:auto;margin-left:48px;line-height:28px\"><span style=\"font-family: 宋体; color: rgb(51, 51, 51); font-size: 12px;\"><img alt=\"clip_image002.png\" src=\"http://wsiops.iok.la:8028/iOPS/ueditor/jsp/upload/image/20161212/1481525942338009851.png\" title=\"1481525942338009851.png\"/> </span></p><p><br/></p><p><br/></p>",
                "desc": "添加需要设置兼容模式的网址，并在“兼容性视图中显示Intranet站点（I）”前面打钩。"
    }*/

    @JSONField(name = "imgs")
    public List<Imgs> knowledgeImgs;
    @JSONField(name = "files")
    public List<Files> knowledgeFiles;
    @JSONField(name = "info")
    public Info knowledgeInfo;
}
