package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * @author by Xianling.Zhou on 2017/3/27.
 */

public class GoodsDetail extends BaseBean {
    
    @JSONField(name = "imgs")
    public List<Imgs> imgses;

    @JSONField(name = "info")
    public GoodsInfo goodsInfo;
}
