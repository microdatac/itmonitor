package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author by Xianling.Zhou on 2017/4/7.
 */

public class AlarmType extends BaseBean {

    /**
     * level2Name : Windows
     * num : 5
     */

    @JSONField(name = "level2Name")
    public String level2Name;
    @JSONField(name = "num")
    public int num;
}
