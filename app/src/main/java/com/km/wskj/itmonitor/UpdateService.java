package com.km.wskj.itmonitor;

import com.zxl.zxlapplibrary.service.ApkDownloadService;

/**
 * @author by Xianling.Zhou on 2017/2/7.
 */

public class UpdateService extends ApkDownloadService {
    @Override
    public int iconResId() {
        return R.mipmap.logo;
    }

    @Override
    public String titleText() {
        return "版本更新";
    }
}
