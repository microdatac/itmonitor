package com.km.wskj.itmonitor.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.km.wskj.itmonitor.pager.wq.forum.model.ForumImg;

import java.util.List;

/**
 * Created by wuhan on 2017/3/15.
 */

public class CommentList extends BaseBean {
//"path": null,
//        "imgs": [ ],
//        "quote": null,
//        "createTime": "2017-03-23 17:48:07",
//        "pId": null,
//        "id": "dc0e6671f92748d5a32679a518b7b66a",
//        "userId": "e73139460d654221b9d1d607cc9530f9",
//        "isDel": 0,
//        "forumId": "5170a8e655e94790a9ff39ed45ce27ad",
//        "content": "测试评论"

    @JSONField(name = "path")
    public String path;
    @JSONField(name = "imgs")
    public List<ForumImg> noteImages;
    @JSONField(name = "quote")
    public String quote;
    @JSONField(name = "createTime")
    public String createTime;
    @JSONField(name = "pId")
    public String pId;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "userId")
    public String userId;
    @JSONField(name = "isDel")
    public int isDel;
    @JSONField(name = "forumId")
    public String forumId;
    @JSONField(name = "content")
    public String content;


}
